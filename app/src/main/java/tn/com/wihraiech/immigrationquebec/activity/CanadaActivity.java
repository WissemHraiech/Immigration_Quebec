package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import tn.com.wihraiech.immigrationquebec.adapter.CanadaPagerAdapter;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by lenovo on 25/12/2016.
 */
public class CanadaActivity extends AppCompatActivity {

    private int currentPos;
    private ViewPager viewPager;
    private CanadaPagerAdapter adapter;
    String[] city = new String[]{"Chateau de Frontenac", "Ottawa", "Rochers Hopewell Baie Fundy", "Québec", "Aurore Boreale", "Bluenose Coast", "Chateau Lake Louise", "Montréal", "Les chutes du Niagara", "Vancouver", "Edmonton", "Golf Île-du-Prince-Édouard", "Saint John's", "Ontario", "Saskatoon"};
    int[] photos = new int[]{R.drawable.frontenacquebec, R.drawable.ottawa, R.drawable.nouveau_brunswick, R.drawable.quebec, R.drawable.yukon, R.drawable.nouvelle_ecosse, R.drawable.lake_louise_alberta, R.drawable.montreal, R.drawable.niagara, R.drawable.vancouver, R.drawable.edmonton, R.drawable.golf, R.drawable.terre_neuve_labrador, R.drawable.manitoba, R.drawable.saskatoon};
    final String EXTRA_POSITION = "position";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canada);

        Intent intent = getIntent();
        if (intent != null) {
            currentPos = intent.getIntExtra(EXTRA_POSITION, 0);
        }


        viewPager = (ViewPager) findViewById(R.id.vp_canada);
        adapter  = new CanadaPagerAdapter(CanadaActivity.this, photos, city);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(currentPos);


    }
}
