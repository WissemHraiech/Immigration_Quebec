package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import tn.com.wihraiech.immigrationquebec.extras.UtilTCM;
import tn.com.wihraiech.immigrationquebec.R;

public class NewPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private String msg;
    private TextView txtWelcome, tv_new_password;
    private EditText input_new_password;
    private Button btnChangePass,btnLogout;
    private RelativeLayout activity_dashboard;

    //private CallbackManager callbackManager;

    private FirebaseUser user;
    private FirebaseAuth auth;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_newPass);
        mToolbar.setTitle("Changer de mot de passe");
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_notification);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        //View
        txtWelcome = (TextView)findViewById(R.id.dashboard_welcome);
        tv_new_password = (TextView)findViewById(R.id.tv_new_password);
        input_new_password = (EditText)findViewById(R.id.dashboard_new_password);
        btnChangePass = (Button)findViewById(R.id.dashboard_btn_change_pass);
        btnLogout = (Button)findViewById(R.id.dashboard_btn_logout);
        activity_dashboard = (RelativeLayout)findViewById(R.id.activity_dash_board);

        progressBar = (ProgressBar) findViewById(R.id.progressBarNP);
        //callbackManager = CallbackManager.Factory.create();

        btnChangePass.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        tv_new_password.setOnClickListener(this);

        //Init Firebase
        /*auth = FirebaseAuth.getInstance();

        //Session check
        if(auth.getCurrentUser() != null)
            txtWelcome.setText("Welcome , "+auth.getCurrentUser().getEmail());*/

        user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            if( user.getDisplayName() != null ){
                msg = user.getDisplayName();
            }else if( user.getEmail() != null ){
                msg = user.getEmail();
               /* String email = user.getEmail();
                Uri photoUrl = user.getPhotoUrl();
                String uid = user.getUid();*/
            }else{
                msg = "Cher(e) Abonné(e)";
            }
            Log.i("logName", msg);
            txtWelcome.setText("Bienvenue , " + msg);

        } else
        {
            goLoginScreen();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            finish();
        }

        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.dashboard_btn_change_pass :
                String password = input_new_password.getText().toString();
                if(!password.isEmpty()) {
                    if (UtilTCM.verifyConnection(getApplicationContext())){
                        /// codes
                        changePassword(password);
                        progressBar.setVisibility(View.VISIBLE);
                    }else{
                        Snackbar snackbar = Snackbar
                                .make(view, "Pas de connexion internet. SVP vérifiez votre wifi ou 3/4G.", Snackbar.LENGTH_LONG)
                                .setAction("Ok", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent it = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                        startActivity(it);
                                    }
                                });
                        // Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                } else {
                    Snackbar.make(activity_dashboard, "Entrez votre nouveau mot de passe !", Snackbar.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }
                break;

            case R.id.dashboard_btn_logout :
                finish();
                break;

            case R.id.tv_new_password:
                startActivity(new Intent(NewPasswordActivity.this, ForgotPassword.class));
                break;
        }


    }

    private void logoutUser() {
        auth.signOut();
        if(auth.getCurrentUser() == null)
        {
            startActivity(new Intent(NewPasswordActivity.this,LoginActivity.class));
            //finish();
        }
    }

    private void goLoginScreen() {
        //Intent intent = new Intent(this, FacebookLoginActivity.class);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void logout() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        goLoginScreen();
    }

    private void changePassword(String newPassword) {
        //FirebaseUser user = auth.getCurrentUser();
        user.updatePassword(newPassword).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(!task.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    Log.i("Logy", task.getException().getMessage());
                    if (task.getException().getMessage().equals("The given password is invalid. [ Password should be at least 6 characters ]")) {
                        Snackbar.make(activity_dashboard, "Le mot de passe doit comporter au moins 6 caractères !", Snackbar.LENGTH_LONG).show();
                    } else if (task.getException().getMessage().equals("This operation is sensitive and requires recent authentication. Log in again before retrying this request.")) {
                        Snackbar.make(activity_dashboard, "Cette opération est sensible et nécessite une authentification récente. Connectez-vous à nouveau avant de réessayer cette demande.", Snackbar.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Snackbar.make(activity_dashboard,"Mot de passe changé avec succès !",Snackbar.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }


            }
        });
    }

    // [START onBackPressed]
    @Override
    public void onBackPressed() {
        finish();
    }


}