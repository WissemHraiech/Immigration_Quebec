package tn.com.wihraiech.immigrationquebec.model;

import android.os.AsyncTask;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.lang.ref.WeakReference;

import tn.com.wihraiech.immigrationquebec.presenter.Presenter;

/**
 * Created by Wissem Hraiech on 28/03/2017.
 */

public class VersionRequester extends AsyncTask<Void, Void, String> {
    private WeakReference<Presenter> presenter;

    public VersionRequester( Presenter p ){
        presenter = new WeakReference<>( p );
    }

    @Override
    protected String doInBackground(Void... voids) {
        String version = null;
        try{
            version = Jsoup
                    .connect("https://play.google.com/store/apps/details?id=tn.com.wihraiech.immigrationquebec")
                    .get()
                    .select("div[itemprop=\"softwareVersion\"]")
                    .text()
                    .trim();/* REMOVING THE BLANK SPACES OF THE LIMITATIONS OF STRING */
        }
        catch (IOException e){}

        return version;
    }

    @Override
    protected void onPostExecute(String version) {
        super.onPostExecute(version);

        if( presenter.get() != null ){
            //presenter.get().showUpdateAppDialog( version );
        }
    }
}
