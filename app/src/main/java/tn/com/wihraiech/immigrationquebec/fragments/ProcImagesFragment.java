package tn.com.wihraiech.immigrationquebec.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.activity.AccueilActivity;
import tn.com.wihraiech.immigrationquebec.adapter.TabsAdapter;
import tn.com.wihraiech.immigrationquebec.domain.ProcImage;
import tn.com.wihraiech.immigrationquebec.extras.SlidingTabLayout;


public class ProcImagesFragment extends Fragment {

    private List<ProcImage> mList;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            mList = ((AccueilActivity) getActivity()).getSetImageList(23);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_proc_images, container, false);

        // TABS
        mViewPager = (ViewPager) view.findViewById(R.id.vp_tabs_proced);
        mViewPager.setAdapter(new TabsAdapter(getChildFragmentManager(), getContext()));

        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.stl_proced);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorAccent));
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                //navigationDrawerLeft.setSelection(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mSlidingTabLayout.setViewPager(mViewPager);

        return view;
    }

}
