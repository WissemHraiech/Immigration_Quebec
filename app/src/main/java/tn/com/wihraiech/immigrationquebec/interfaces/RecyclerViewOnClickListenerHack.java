package tn.com.wihraiech.immigrationquebec.interfaces;

import android.view.View;

/**
 * Created by lenovo on 11/12/2016.
 */
public interface RecyclerViewOnClickListenerHack {
    void onClickListener(View view, int position);
    void onLongPressClickListener(View view, int position);
}
