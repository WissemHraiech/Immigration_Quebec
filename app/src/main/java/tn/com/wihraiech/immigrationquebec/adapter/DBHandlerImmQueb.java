package tn.com.wihraiech.immigrationquebec.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import tn.com.wihraiech.immigrationquebec.domain.Score;

public class DBHandlerImmQueb extends SQLiteOpenHelper{

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;
	// Database Name
	private static final String DATABASE_NAME = "ImmQuebDB";
	// vehicule table name
	public static final String TABLE_SCORE = "score";
	// vehicule Table Columns names
	public static final String KEY_ID = "_id";
	public static final String NOM = "nom";
	public static final String SCORE = "score";
	public static final String CREATED_AT = "created_at";
			
			
	public DBHandlerImmQueb(Context context) {
		super(context,DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_SCORE_TABLE = "CREATE TABLE " + TABLE_SCORE + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + NOM + " TEXT," + SCORE + " TEXT,"
				+ CREATED_AT + " DATETIME DEFAULT CURRENT_DATE" + ")";
		db.execSQL(CREATE_SCORE_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db,int oldVersion, int newVersion) {
		// Upgrading older table if existed
		Log.w(DATABASE_NAME, "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
		// Create tables again
		onCreate(db);

	}

	// Adding new Score
		public void addScore(Score score) {

			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(NOM, score.get_nom());
			values.put(SCORE, score.get_score());
			// Inserting Row
			db.insert(TABLE_SCORE, null, values);
			db.close(); // Closing database connection
		}

		//Update single Vehicule
		/*public int updateVehicule(Enseignant enseignant) {

			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(, enseignant.get_);
			return db.update(TABLE_VEHICULE, values, KEY_ID + " = ?",
					new String[] { String.valueOf(vehicule.get_id()) });
		}*/

		// Deleting single Score
		public int deleteScore(Score score) {

			SQLiteDatabase db = this.getWritableDatabase();

			int i = db.delete(TABLE_SCORE, KEY_ID + " = ?",
			new String[] { String.valueOf(score.get_id()) });
			db.close();
			return i;
		}
			
			
}
