package tn.com.wihraiech.immigrationquebec.domain;

/**
 * Created by lenovo on 11/12/2016.
 */
public class ProcImage {

    private int id;
    private String title;
    private int photo;

    public ProcImage(){}
    public ProcImage(int p){
        this.photo = p;
    }
    public ProcImage(int p, String t){
        this.photo = p;
        this.title = t;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}