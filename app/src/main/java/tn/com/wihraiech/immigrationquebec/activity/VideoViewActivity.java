package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by lenovo on 13/12/2016.
 */
public class VideoViewActivity extends YouTubeBaseActivity {

    private YouTubePlayerView youTubePlayerView;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    private String idVideo;
    private final String EXTRA_IDVIDEO = "idvideo";

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference apiKey = database.getReference("link_notif");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        Intent intent = getIntent();
        if (intent != null) {
            idVideo = intent.getStringExtra(EXTRA_IDVIDEO);
        }

        // Read from the database
        apiKey.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String API_KEY = dataSnapshot.getValue(String.class);
                youTubePlayerView.initialize(API_KEY, onInitializedListener);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("API_KEY_TAG", "Failed to read API_KEY");
            }
        });

        youTubePlayerView = findViewById(R.id.youtube_player_view);
        onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(idVideo);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

    }

}
