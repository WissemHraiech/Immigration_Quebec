package tn.com.wihraiech.immigrationquebec.activity;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mikepenz.materialdrawer.Drawer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import tn.com.wihraiech.immigrationquebec.adapter.DBHandlerImmQueb;
import tn.com.wihraiech.immigrationquebec.domain.Score;
import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.extras.PatternEditableBuilder;
import tn.com.wihraiech.immigrationquebec.R;

import static tn.com.wihraiech.immigrationquebec.GlobalFunctions.loadAdBanner;
import static tn.com.wihraiech.immigrationquebec.globalAds.INTERSTITIAL_EPOUX_AD_UNIT_ID;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        com.rey.material.widget.Spinner.OnItemSelectedListener {

    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final String TWELVE_PRO_SECONDARY_LEVEL_POINT = "12";

    final DBHandlerImmQueb db = new DBHandlerImmQueb(this);
    final String EXTRA_SCORE = "score", EXTRA_SCOREMP = "scoremp", EXTRA_EMAIL = "email";
    private final String googlePlayURL = Config.GOOGLEPLAY_URL;
    private Toolbar mToolbar;
    private Drawer.Result navigationDrawerLeft;
    private com.rey.material.widget.Spinner spinner_Scolarité, spinner_univ, spinner_colleg, spinner_seconPro, spinner_Exp, spinner_Age,
            spinner_fco, spinner_fpo, spinner_fce, spinner_fpe, spinner_aco, spinner_ace, spinner_apo, spinner_ape, spinner_sejour,
            spinner_famille, spinner_emploi, spinner_emploi_outside_cmm, spinner_enf12, spinner_enf1318;
    private EditText et_scorScol, et_form, et_exp, et_age, et_fco, et_fce, et_fpo, et_fpe, et_aco, et_ace, et_apo, et_ape, et_sejour,
            et_famille, et_emploi, et_scorenf, et_scorEnf12, et_scorEnf1318, et_CapFin;
    private TextView tv_monScrore, tv_scoreRequi, tv_epoux, tv_felicit, tv_msgEmploy;
    private RadioGroup rg_form;
    private CheckBox chkSejour, chkFam, chkEmploi, chkCapFin;
    private Button bt_next, bt_save;
    private Config gestion = new Config();
    private int val, scoreTot, scoreEmp;
    //PDF File Parameters
    private PdfPCell cell;
    private Image bgImage;
    private String path, msgPDF;
    private File dir;
    private File file;
    //use to set background color
    private BaseColor myColor = WebColors.getRGBColor("#01238f");
    private BaseColor myColor2 = WebColors.getRGBColor("#909090");
    private BaseColor myColor1 = WebColors.getRGBColor("#FBD2B2");
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdView = findViewById(R.id.adView);
        loadAdBanner(mAdView, getApplicationContext());

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_main1);
        //mToolbar.setTitle("Immigration Québec");
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_launcher);
        setSupportActionBar(mToolbar);

        //creating new file path
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/";
        dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        tv_scoreRequi = (TextView) findViewById(R.id.tv_scoreRequi);
        tv_epoux = (TextView) findViewById(R.id.tv_epoux);
        tv_felicit = (TextView) findViewById(R.id.tv_felicit);
        tv_msgEmploy = (TextView) findViewById(R.id.tv_msgEmploy);

        bt_next = (Button) findViewById(R.id.bt_links);
        bt_save = (Button) findViewById(R.id.bt_saveCelib);
        setSaveBtOnClickListener();


        Intent intent = new Intent();
        if (intent != null) {
            val = getIntent().getIntExtra("val", 0);
        }

        setupActionBar();

        /*  Spinners  */
        spinner_Scolarité = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_scolarite);
        spinner_univ = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_univ);
        spinner_colleg = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_collegial);
        spinner_seconPro = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_seconPro);
        spinner_Exp = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_exp);
        spinner_Age = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_age);
        spinner_fco = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_cpOral);
        spinner_fce = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_cpEcrite);
        spinner_fpo = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_prOrale);
        spinner_fpe = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_prEcrite);
        spinner_aco = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_cpOrAng);
        spinner_ace = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_cpEcAng);
        spinner_apo = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_prOrAng);
        spinner_ape = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_prEcAng);
        spinner_sejour = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_sejour);
        spinner_famille = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_fam);
        spinner_emploi = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_emploi);
        spinner_emploi_outside_cmm = findViewById(R.id.work_outside_cmm_spinner);
        spinner_enf12 = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_enf12);
        spinner_enf1318 = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_enfplus12);


        /*  EditText  */
        et_scorScol = (EditText) findViewById(R.id.et_scoreNivScol);
        et_form = (EditText) findViewById(R.id.et_scoreDomForm);
        et_exp = (EditText) findViewById(R.id.et_scoreExp);
        et_age = (EditText) findViewById(R.id.et_scoreAge);
        et_fco = (EditText) findViewById(R.id.et_scoreCpOral);
        et_fce = (EditText) findViewById(R.id.et_scoreCpEcrite);
        et_fpo = (EditText) findViewById(R.id.et_scorePrOrale);
        et_fpe = (EditText) findViewById(R.id.et_scorePrEcrite);
        et_aco = (EditText) findViewById(R.id.et_scoreCpOrAng);
        et_ace = (EditText) findViewById(R.id.et_scoreCpEcAng);
        et_apo = (EditText) findViewById(R.id.et_scorePrOrAng);
        et_ape = (EditText) findViewById(R.id.et_scorePrEcAng);
        et_sejour = (EditText) findViewById(R.id.et_scoreSejour);
        et_famille = (EditText) findViewById(R.id.et_scoreFam);
        et_emploi = (EditText) findViewById(R.id.et_scoreEmploi);
        et_scorenf = (EditText) findViewById(R.id.et_scoreenf);
        et_scorEnf12 = (EditText) findViewById(R.id.et_scoreenf12);
        et_scorEnf1318 = (EditText) findViewById(R.id.et_scor_enfplus12);
        et_CapFin = (EditText) findViewById(R.id.et_scoreCapFin);

        tv_monScrore = (TextView) findViewById(R.id.tv_monScore);

        et_scorScol.setClickable(false);

        addItemsOnSpinner_Scolarité();
        addItemsOnSpinnerUniversite();
        addItemsOnSpinner_Colleg();
        addItemsOnSpinner_Experience();
        addItemsOnSpinner_SecondPro();
        addItemsOnSpinner_Age();
        addItemsOnSpinner_langues();
        addItemsOnSpinner_Sejours();
        addItemsOnSpinner_Famille();
        addItemsOnSpinner_Emploi();
        addItemsOnSpinner_Emploi_outside_cmm();
        addItemsOnSpinner_nbr12();
        addItemsOnSpinner_nbr1318();

        addListenerOnChkSejour();
        addListenerOnChkFamille();
        addListenerOnChkEmploi();
        addListenerOnChkCapFin();

        gestionNiveauUniversite();
        gestionNiveauCollegiale();
        gestionSecondPro();
        gestionNbr12();
        gestionNbr1318();

        //Drawer finish()
        navigationDrawerLeft = new Drawer()
                .withActivity(this)
                .withActionBarDrawerToggle(true)
                .withCloseOnClick(true)
                .withActionBarDrawerToggleAnimated(true)
                .withActionBarDrawerToggle(new ActionBarDrawerToggle(this, new DrawerLayout(this), R.string.drawer_open, R.string.drawer_close) {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        super.onDrawerSlide(drawerView, slideOffset);
                        navigationDrawerLeft.closeDrawer();
                        finish();
                    }
                })
                .build();

        loadInterstitialAd();

        bt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeLangDialog();
            }
        });

        tv_epoux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(getParent());
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                    toSecond();
                }

            }
        });


    }

    public void loadInterstitialAd() {
        mInterstitialAd.load(getApplicationContext(),
                INTERSTITIAL_EPOUX_AD_UNIT_ID,
                new AdRequest.Builder().build(),
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitial) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                toSecond();
                            }
                        };
                        mInterstitialAd = interstitial;
                        Log.i("TAG_InterstitialAd", "onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
                        Log.i("TAG_InterstitialAd", loadAdError.getMessage());
                        mInterstitialAd = null;
                    }
                });
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);
        TextView textView = findViewById(R.id.idtoolbar);

        // COLORED LINKS
        new PatternEditableBuilder().
                addPattern(Pattern.compile("Cliquer ici"), getResources().getColor(R.color.colorDivider)).
                into(tv_epoux);

        if (val == 1) {
            textView.setText(getResources().getString(R.string.title_action_celib));
            tv_epoux.setVisibility(View.GONE);
            bt_save.setVisibility(View.VISIBLE);
            scoreTot = 49;
            scoreEmp = 42;
        } else {
            textView.setText(getResources().getString(R.string.title_action_couple));
            tv_scoreRequi.setText("/59");
            tv_epoux.setVisibility(View.VISIBLE);
            bt_save.setVisibility(View.GONE);
            scoreTot = 58;
            scoreEmp = 51;
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        /*getSupportActionBar().setLogo(R.drawable.icquebec);
        getSupportActionBar().setDisplayUseLogoEnabled(true);*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setSaveBtOnClickListener() {
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO CREATE PDF
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        try {
                            createPDF();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                        // Code for above or equal 23 API Oriented Device
                        // Your Permission granted already .Do next code
                    } else {
                        requestPermission(); // Code for permission

                    }
                } else {
                    try {
                        createPDF();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }
                    // Code for Below 23 API Oriented Device
                    // Do next code
                }
                // Todo: Contact an Expert
                //startActivity(new Intent(MainActivity.this, ContactActivity.class));

            }
        });
    }

    /**
     * TODO: Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_rate) {
            Intent it = new Intent(Intent.ACTION_VIEW);
            it.setData(Uri.parse(googlePlayURL));
            startActivity(it);
        }
        //Reset
        if (id == R.id.action_reset) {
            resetAll();
        }

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    //Link To SecondActivity
    public void toSecond() {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("et_scorScol", et_scorScol.getText().toString());
        intent.putExtra("et_form", et_form.getText().toString());
        intent.putExtra("et_exp", et_exp.getText().toString());
        intent.putExtra("et_age", et_age.getText().toString());
        intent.putExtra("et_fco", et_fco.getText().toString());
        intent.putExtra("et_fpo", et_fpo.getText().toString());
        intent.putExtra("et_fce", et_fce.getText().toString());
        intent.putExtra("et_fpe", et_fpe.getText().toString());
        intent.putExtra("et_aco", et_aco.getText().toString());
        intent.putExtra("et_apo", et_apo.getText().toString());
        intent.putExtra("et_ace", et_ace.getText().toString());
        intent.putExtra("et_ape", et_ape.getText().toString());
        intent.putExtra("et_sejour", et_sejour.getText().toString());
        intent.putExtra("et_famille", et_famille.getText().toString());
        intent.putExtra("et_emploi", et_emploi.getText().toString());
        intent.putExtra("scoreEnfants", et_scorenf.getText().toString());
        intent.putExtra("et_CapFin", et_CapFin.getText().toString());
        intent.putExtra(EXTRA_SCORE, tv_monScrore.getText().toString());
        intent.putExtra(EXTRA_SCOREMP, scoreEmploy());
        startActivity(intent);
    }

    //Link To ImmigrationActivity
    public void toImmigration(View view) {
        Intent intent = new Intent(this, ImmigrationActivity.class);
        startActivity(intent);
    }

    // TODO: ALERT DIALOG SAVE SCORE
    public void showChangeLangDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_score, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.et_usernameScore);
        final TextView txt = (TextView) dialogView.findViewById(R.id.tv_dialogscore);
        txt.setText(tv_monScrore.getText().toString());

        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle(getResources().getString(R.string.save_score));

        dialogBuilder.setPositiveButton(R.string.save_score, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Add New Score
                Log.d("Insert: ", "Inserting..");
                db.addScore(new Score(edt.getText().toString(), tv_monScrore.getText().toString()));
                // TODO Auto-generated method stub

                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        try {
                            createPDF();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                        // Code for above or equal 23 API Oriented Device
                        // Your Permission granted already .Do next code
                    } else {
                        requestPermission(); // Code for
                        try {
                            createPDF();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    try {
                        createPDF();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }
                    // Code for Below 23 API Oriented Device
                    // Do next code
                }


                //Toast.makeText(MainActivity.this, "Score enregistré !",Toast.LENGTH_SHORT).show();
                // To LinksActivity
                Intent intent = new Intent(MainActivity.this, LinkActivity.class);
                startActivity(intent);
            }
        });
        dialogBuilder.setNegativeButton(R.string.cancel_score, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // To LinksActivity
                Intent intent = new Intent(MainActivity.this, LinkActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    // TODO: Score Enfants
    public int scoreEnfants() {
        int score = Integer.decode(et_scorEnf12.getText().toString()) + Integer.decode(et_scorEnf1318.getText().toString());
        if (score >= 8)
            return 8;

        return score;
    }

    // TODO: Calcul Score Réqurent Principal
    public int calculScore() {
        int score = Integer.decode(et_scorScol.getText().toString()) + Integer.decode(et_form.getText().toString()) + Integer.decode(et_exp.getText().toString())
                + Integer.decode(et_ace.getText().toString()) + Integer.decode(et_ape.getText().toString()) + Integer.decode(et_apo.getText().toString())
                + Integer.decode(et_aco.getText().toString()) + Integer.decode(et_fce.getText().toString()) + Integer.decode(et_fpe.getText().toString())
                + Integer.decode(et_fco.getText().toString()) + Integer.decode(et_fpo.getText().toString()) + Integer.decode(et_age.getText().toString())
                + Integer.decode(et_sejour.getText().toString()) + Integer.decode(et_famille.getText().toString()) + Integer.decode(et_emploi.getText().toString())
                + Integer.decode(et_CapFin.getText().toString()) + scoreEnfants();

        et_scorenf.setText("" + scoreEnfants());

        //Animation text view monscore
        YoYo.with(Techniques.ZoomInUp)
                .duration(700)
                .playOn(findViewById(R.id.tv_monScore));

        if (score > scoreTot) {
            if (scoreEmploy() > scoreEmp) {
                tv_monScrore.setTextColor(getResources().getColor(R.color.green2));
                tv_felicit.setVisibility(View.VISIBLE);
                bt_next.setVisibility(View.VISIBLE);
                tv_msgEmploy.setVisibility(View.GONE);
                msgPDF = tv_felicit.getText().toString();


                YoYo.with(Techniques.Tada)
                        .duration(700)
                        .playOn(findViewById(R.id.tv_felicit));
            } else {
                tv_monScrore.setTextColor(getResources().getColor(R.color.red));
                tv_felicit.setVisibility(View.GONE);
                bt_next.setVisibility(View.GONE);
                tv_msgEmploy.setVisibility(View.VISIBLE);
                msgPDF = getResources().getString(R.string.employ_thres_message);

                //Animation
                YoYo.with(Techniques.Tada)
                        .duration(700)
                        .playOn(findViewById(R.id.tv_msgEmploy));
            }
        } else {
            tv_monScrore.setTextColor(getResources().getColor(R.color.red));
            tv_felicit.setVisibility(View.GONE);
            bt_next.setVisibility(View.GONE);
            tv_msgEmploy.setVisibility(View.GONE);
            msgPDF = getResources().getString(R.string.selection_crit_message);
        }

        return score;
    }

    //Méthode de calcul score Employabilité
    public int scoreEmploy() {

        int score = Integer.decode(et_scorScol.getText().toString()) + Integer.decode(et_form.getText().toString()) + Integer.decode(et_exp.getText().toString())
                + Integer.decode(et_ace.getText().toString()) + Integer.decode(et_ape.getText().toString()) + Integer.decode(et_apo.getText().toString())
                + Integer.decode(et_aco.getText().toString()) + Integer.decode(et_fce.getText().toString()) + Integer.decode(et_fpe.getText().toString())
                + Integer.decode(et_fco.getText().toString()) + Integer.decode(et_fpo.getText().toString()) + Integer.decode(et_age.getText().toString())
                + Integer.decode(et_sejour.getText().toString()) + Integer.decode(et_famille.getText().toString()) + Integer.decode(et_emploi.getText().toString());

        return score;
    }

    @Override
    public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {

        gestionNiveauUniversite();
        gestionNiveauCollegiale();
        gestionSecondPro();

        gestionScolarité();
        gestionFormation();
        gestionExperience();
        gestionAge();
        gestionLangues();
        gestionSejours();
        gestionFamille();
        gestionEmploi();
        gestionJobOfferOutsideCMM();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    // add items into spinner Scolarité
    public void addItemsOnSpinner_Scolarité() {

        // Spinner click listener
        spinner_Scolarité.setOnItemSelectedListener(this);

        List<String> categories = gestion.scolarité(getApplicationContext());

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_Scolarité.setAdapter(dataAdapter);
    }

    public void gestionScolarité() {
        //  switch (String.valueOf(spinner_Scolarité.getSelectedItem())) {
        switch (spinner_Scolarité.getSelectedItemPosition()) {
            case 0:
                et_scorScol.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 1:
                et_scorScol.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 2:
                et_scorScol.setText("4");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 3:
                et_scorScol.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            /*case "Secondaire professionnel 1 an ou + ou postsecondaire technique 1 an ou 2 ans ET domaine de formation à 12 points ou à 16 points":
                et_scorScol.setText("10");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;*/
            case 4:
                et_scorScol.setText("8");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            /*case "Postsecondaire technique 3 ans ET domaine de formation à 12 points ou à 16 points":
                et_scorScol.setText("10");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;*/
            case 5:
                et_scorScol.setText("4");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 6:
                et_scorScol.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 7:
                et_scorScol.setText("10");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 8:
                et_scorScol.setText("12");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 9:
                et_scorScol.setText("14");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_scorScol.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }
    }

    public void gestionFormation() {

        /* Initialize Radio Group and attach click handler */
        rg_form = (RadioGroup) findViewById(R.id.rg_formation);
        //rg_form.clearCheck();
        /* Attach CheckedChangeListener to radio group */
        /*rg_form.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {*/

        /* Attach CheckedChangeListener to radio group */
        rg_form.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {

                    if (rb.getText().equals(getResources().getString(R.string.string_university_level))) {
                        spinner_univ.setVisibility(View.VISIBLE);
                        spinner_colleg.setVisibility(View.INVISIBLE);
                        spinner_seconPro.setVisibility(View.INVISIBLE);
                        spinner_univ.setSelection(0);
                        spinner_seconPro.setSelection(0);
                        spinner_colleg.setSelection(0);
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.zero_point_domain_message), Toast.LENGTH_LONG).show();

                    } else if (rb.getText().equals(getResources().getString(R.string.string_technical_college_level))) {
                        spinner_colleg.setVisibility(View.VISIBLE);
                        spinner_univ.setVisibility(View.INVISIBLE);
                        spinner_seconPro.setVisibility(View.INVISIBLE);
                        spinner_univ.setSelection(0);
                        spinner_seconPro.setSelection(0);
                        spinner_colleg.setSelection(0);
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.zero_point_domain_message), Toast.LENGTH_LONG).show();

                    } else if (rb.getText().equals(getResources().getString(R.string.string_secondary_professional_level))) {
                        spinner_seconPro.setVisibility(View.VISIBLE);
                        spinner_univ.setVisibility(View.INVISIBLE);
                        spinner_colleg.setVisibility(View.INVISIBLE);
                        spinner_univ.setSelection(0);
                        spinner_seconPro.setSelection(0);
                        spinner_colleg.setSelection(0);
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.zero_point_domain_message), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        //}
        //});
    }

    public void addItemsOnSpinnerUniversite() {

        // Spinner click listener
        spinner_univ.setOnItemSelectedListener(this);


        List<String> univ = gestion.univ();

        // Creating adapter for spinner
        ArrayAdapter<String> univAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, univ);

        // Drop down layout style - list view with radio button
        univAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_univ.setAdapter(univAdapter);
    }

    public void gestionNiveauUniversite() {

        spinner_univ.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {
                switch (String.valueOf(spinner_univ.getSelectedItem())) {
                    case "Autres professions de la santé (DOC)":
                    case "Chiropratique (DOC)":
                    case "Génie informatique et de la construction des ordinateurs(BAC)":
                    case "Information de gestion (BAC)":
                    case "Psychoéducation (MAI)":
                    case "Recherche opérationnelle (MAI)":
                        et_form.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    case "Actuariat (BAC)":
                    case "Architecture (MAI)":
                    case "Architecture paysagiste (BAC)":
                    case "Architecture urbaine et aménagement (MAI)":
                    case "Coopération (MAI)":
                    case "Criminologie (BAC)":
                    case "Ergothérapie (MAI)":
                    case "Ethnologie et ethnographie (MAI)":
                    case "Économie rurale et agricole (BAC)":
                    case "Énergie (MAI)":
                    case "Formation des enseignants au préscolaire et au primaire(BAC)":
                    case "Gestion du personnel (BAC)":
                    case "Génie agricole et génie rural (BAC)":
                    case "Génie agroforestier (MAI)":
                    case "Génie aérospatial, aéronautique et astronautique (BAC)":
                    case "Génie industriel et administratif (BAC)":
                    case "Génie mécanique (BAC)":
                    case "Génie nucléaire (MAI)":
                    case "Génie physique (BAC)":
                    case "Génie électrique, électronique et des communications (BAC)":
                    case "Marketing et achats (BAC)":
                    case "Mathématiques (BAC)":
                    case "Mathématiques appliquées (BAC)":
                    case "Optométrie (DOC)":
                    case "Opérations bancaires et finance (BAC)":
                    case "Orthophonie et audiologie (BAC)":
                    case "Orthophonie et audiologie (MAI)":
                    case "Physiothérapie (MAI)":
                    case "Probabilités et statistiques (BAC)":
                    case "Psychoéducation (BAC)":
                    case "Relations industrielles (BAC)":
                    case "Santé communautaire et épidémiologie (MAI)":
                    case "Sciences de l'informatique (BAC)":
                    case "Sciences domestiques (BAC)":
                    case "Sciences infirmières et nursing (BAC)":
                    case "Sciences physiques (BAC)":
                    case "Service social (BAC)":
                    case "Sexologie (BAC)":
                        et_form.setText("9");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    case "Administration des affaires (BAC)":
                    case "Affaires sur le plan international (BAC)":
                    case "Agriculture (BAC)":
                    case "Animation sociale ou communautaire (BAC)":
                    case "Anthropologie (BAC)":
                    case "Architecture (BAC)":
                    case "Arts graphiques (communications graphiques) (BAC)":
                    case "Arts plastiques (peinture, dessin, sculpture) (BAC)":
                    case "Bibliothéconomie et archivistique (MAI)":
                    case "Chimie (BAC)":
                    case "Communications et journalisme (BAC)":
                    case "Comptabilité et sciences comptables (BAC)":
                    case "Didactique (art d'enseigner) (MAI)":
                    case "Démographie (MAI)":
                    case "Environnement (qualité du milieu et pollution) (BAC)":
                    case "Ergothérapie (BAC)":
                    case "Études géopolitiques (BAC)":
                    case "Formation des enseignants au préscolaire (BAC)":
                    case "Formation des enseignants au secondaire (BAC)":
                    case "Formation des enseignants spécialistes au primaire et au secondaire (BAC)":
                    case "Formation des enseignants spécialistes en adaptation scolaire (orthopédagogie) (BAC)":
                    case "Français, en général et langue maternelle (BAC)":
                    case "Gestion des services de santé (MAI)":
                    case "Gestion et administration des entreprises (BAC)":
                    case "Génie alimentaire (BAC)":
                    case "Génie biologique et biomédical (BAC)":
                    case "Génie civil, de la construction et du transport (BAC)":
                    case "Génie des pâtes et papiers (MAI)":
                    case "Génie géologique (BAC)":
                    case "Génie minier (BAC)":
                    case "Génie métallurgique et des matériaux (BAC)":
                    case "Génétique (MAI)":
                    case "Géologie (minéralogie, etc.) (BAC)":
                    case "Hydrologie et sciences de l'eau (MAI)":
                    case "Ingénierie (BAC)":
                    case "Langues et littératures françaises ou anglaises (BAC)":
                    case "Médecine et chirurgie expérimentales (MAI)":
                    case "Océanographie (MAI)":
                    case "Philosophie (BAC)":
                    case "Photographie (BAC)":
                    case "Physiothérapie (BAC)":
                    case "Psychologie (BAC)":
                    case "Psychologie (DOC)":
                    case "Psychologie (MAI)":
                    case "Pédagogie universitaire (MAI)":
                    case "Ressources naturelles (BAC)":
                    case "Récréologie (BAC)":
                    case "Sciences de l'activité physique (BAC)":
                    case "Sciences fondamentales et sciences appliquées de la santé (BAC)":
                    case "Sciences politiques (BAC)":
                    case "Sociologie (BAC)":
                    case "Traduction (BAC)":
                    case "Urbanisme (BAC)":
                        et_form.setText("6");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    default:
                        et_form.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                }
            }
        });

    }

    public void addItemsOnSpinner_Colleg() {

        // Spinner click listener
        spinner_colleg.setOnItemSelectedListener(this);


        List<String> colleg = gestion.colleg();

        // Creating adapter for spinner
        ArrayAdapter<String> collegAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, colleg);

        // Drop down layout style - list view with radio button
        collegAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_colleg.setAdapter(collegAdapter);
    }

    public void gestionNiveauCollegiale() {

        spinner_colleg.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {
                switch (String.valueOf(spinner_colleg.getSelectedItem())) {
                    case "Techniques d'orthèses visuelles (DEC)":
                    case "Techniques d'éducation à l'enfance (AEC)":
                    case "Techniques d'électrophysiologie médicale (DEC)":
                    case "Techniques de denturologie (DEC)":
                    case "Techniques de l'informatique (DEC)":
                    case "Techniques de santé animale (DEC)":
                        et_form.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    case "Acupuncture (DEC)":
                    case "Assainissement de l'eau (DEC)":
                    case "Environnement, hygiène et sécurité au travail (DEC) ":
                    case "Gestion d'un établissement de restauration (AEC)":
                    case "Gestion d'un établissement de restauration (DEC)":
                    case "Gestion et technologies d'entreprise agricole (AEC)":
                    case "Gestion et technologies d'entreprise agricole (DEC)":
                    case "Soins infirmiers (DEC)":
                    case "Techniques d'avionique (AEC) ":
                    case "Techniques d'avionique (DEC) ":
                    case "Techniques d'hygiène dentaire (DEC)":
                    case "Techniques d'intégration multimédia (DEC)":
                    case "Techniques d'orthèses et de prothèses orthopédiques (DEC)":
                    case "Techniques d'éducation spécialisée (DEC)":
                    case "Techniques d'éducation à l'enfance (AEC) ":
                    case "Techniques de gestion hôtelière (DEC)":
                    case "Techniques de génie aérospatial (DEC) ":
                    case "Techniques de génie mécanique (DEC)":
                    case "Techniques de génie mécanique de marine (DEC)":
                    case "Techniques de la plasturgie (DEC)":
                    case "Techniques de pilotage d'aéronefs (DEC)":
                    case "Techniques du meuble et d'ébénisterie (DEC)":
                    case "Technologie de la mécanique du bâtiment (DEC)":
                    case "Technologie de médecine nucléaire (DEC)":
                    case "Technologie de radio-oncologie (DEC)":
                    case "Technologie de radiodiagnostic (DEC)":
                    case "Technologie de systèmes ordinés (DEC)":
                    case "Technologie du génie métallurgique (DEC)":
                        et_form.setText("9");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    case "Assainissement de l'eau (AEC) ":
                    case "Audioprothèse (DEC) ":
                    case "Conseil en assurances et en services financiers (DEC)":
                    case "Environnement, hygiène et sécurité au travail (AEC)":
                    case "Gestion de projet en communications graphiques (DEC) ":
                    case "Navigation (DEC)":
                    case "Paysage et commercialisation en horticulture ornementale (DEC)":
                    case "Techniques d'inhalothérapie (DEC) ":
                    case "Techniques d'intervention en délinquance (AEC)":
                    case "Techniques d'intervention en délinquance (DEC)":
                    case "Techniques d'intervention en loisir (DEC) ":
                    case "Techniques d'intégration multimédia (AEC) ":
                    case "Techniques d'éducation spécialisée (AEC)":
                    case "Techniques de bureautique (AEC)":
                    case "Techniques de bureautique (DEC)":
                    case "Techniques de diététique (DEC)":
                    case "Techniques de gestion hôtelière (AEC) ":
                    case "Techniques de génie aérospatial (AEC)":
                    case "Techniques de génie chimique (DEC) ":
                    case "Techniques de génie mécanique (AEC)":
                    case "Techniques de l'informatique (AEC)":
                    case "Techniques de maintenance d'aéronefs (AEC) ":
                    case "Techniques de maintenance d'aéronefs (DEC) ":
                    case "Techniques de procédés chimiques (DEC) ":
                    case "Techniques de recherche sociale (DEC)":
                    case "Techniques de thanatologie (AEC) ":
                    case "Techniques de thanatologie (DEC)":
                    case "Techniques de tourisme (DEC) ":
                    case "Techniques de transformation des matériaux composites (AEC)":
                    case "Techniques de transformation des matériaux composites (DEC) ":
                    case "Techniques du meuble et d'ébénisterie (AEC)":
                    case "Techniques juridiques (DEC) ":
                    case "Techniques équines (DEC) ":
                    case "Technologie de l'architecture (AEC)":
                    case "Technologie de l'architecture (DEC) ":
                    case "Technologie de l'électronique (AEC) ":
                    case "Technologie de l'électronique (DEC) ":
                    case "Technologie de l'électronique industrielle (DEC) ":
                    case "Technologie de la géomatique (AEC) ":
                    case "Technologie de la géomatique (DEC) ":
                    case "Technologie de la production pharmaceutique (DEC) ":
                    case "Technologie de maintenance industrielle (DEC)":
                    case "Technologie des procédés et de la qualité des aliments (AEC) ":
                    case "Technologie des procédés et de la qualité des aliments (DEC) ":
                    case "Technologie des productions animales (DEC) ":
                    case "Technologie du génie agromécanique (DEC)":
                    case "Technologie du génie civil (DEC)":
                    case "Technologie du génie industriel (AEC) ":
                    case "Technologie du génie industriel (DEC)":
                    case "Technologie du génie métallurgique (AEC)":
                    case "Technologie du génie physique (DEC)":
                    case "Technologie minérale (DEC)":
                        et_form.setText("6");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    default:
                        et_form.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                }
            }
        });

    }

    public void addItemsOnSpinner_SecondPro() {

        // Spinner click listener
        spinner_seconPro.setOnItemSelectedListener(this);


        List<String> colleg = gestion.secondaire();

        // Creating adapter for spinner
        ArrayAdapter<String> secondAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, colleg);

        // Drop down layout style - list view with radio button
        secondAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_seconPro.setAdapter(secondAdapter);
    }

    public void gestionSecondPro() {

        spinner_seconPro.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {
                switch (String.valueOf(spinner_seconPro.getSelectedItem())) {

                    case "Boucherie de détail (DEP)":
                    case "Grandes cultures (DEP) ":
                    case "Mécanique agricole (DEP)":
                    case "Mécanique d'ascenseur (DEP)":
                    case "Mécanique d'engins de chantier (DEP)":
                    case "Production acéricole (DEP) ":
                    case "Production animale (DEP)":
                        et_form.setText(TWELVE_PRO_SECONDARY_LEVEL_POINT);
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    case "Assistance technique en pharmacie (DEP)":
                    case "Assistance à la personne à domicile (DEP) ":
                    case "Chaudronnerie (DEP)":
                    case "Conduite de procédés de traitement de l'eau (DEP)":
                    case "Cuisine (DEP)":
                    case "Électromécanique de systèmes automatisés (DEP)":
                    case "Forage et dynamitage (DEP)":
                    case "Installation et fabrication de produits verriers (DEP)":
                    case "Installation et réparation d'équipement de télécommunication (DEP)":
                    case "Mécanique de machines fixes (DEP)":
                    case "Mécanique de véhicules lourds routiers (DEP)":
                    case "Régulation de vol (DEP)":
                    case "Techniques d'usinage (DEP) ":
                        et_form.setText("9");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case "Calorifugeage (DEP)":
                    case "Carrosserie (DEP)":
                    case "Charpenterie-menuiserie (DEP)":
                    case "Coiffure (DEP)":
                    case "Conduite et réglage de machines à mouler (DEP)":
                    case "Conseil et vente de pièces d'équipement motorisé (DEP)":
                    case "Conseil et vente de voyages (DEP)":
                    case "Conseil technique en entretien et en réparation de véhicules (DEP)":
                    case "Dessin de bâtiment (DEP) ":
                    case "Dessin industriel (DEP)":
                    case "Esthétique (DEP)":
                    case "Fabrication de moules (ASP) ":
                    case "Ferblanterie-tôlerie (DEP)":
                    case "Fonderie (DEP) ":
                    case "Installation de revêtements souples (DEP)":
                    case "Installation et entretien de systèmes de sécurité (DEP) ":
                    case "Matriçage (ASP) ":
                    case "Mise en oeuvre de matériaux composites (DEP)":
                    case "Modelage (DEP) ":
                    case "Montage de câbles et de circuits (DEP)":
                    case "Montage de structures en aérospatiale (DEP) ":
                    case "Montage mécanique en aérospatiale (DEP) ":
                    case "Mécanique automobile (DEP) ":
                    case "Mécanique de protection contre les incendies (DEP) ":
                    case "Mécanique de véhicules légers (DEP)":
                    case "Mécanique industrielle de construction et d'entretien (DEP)":
                    case "Opération d'équipements de production (DEP)":
                    case "Outillage (ASP) ":
                    case "Pâtisserie (DEP)":
                    case "Peinture en bâtiment (DEP)":
                    case "Plomberie et chauffage (DEP) ":
                    case "Préparation et finition de béton (DEP) ":
                    case "Réfrigération (DEP)":
                    case "Santé, assistance et soins infirmiers (DEP)":
                    case "Service de la restauration (DEP) ":
                    case "Service de la restauration (DEP)":
                    case "Soudage-montage (DEP) ":
                    case "Soutien informatique (DEP) ":
                    case "Vente-conseil (DEP) ":
                    case "Ébénisterie (DEP) ":
                        et_form.setText("6");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    default:
                        et_form.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                }
            }
        });

    }

    public void addItemsOnSpinner_Experience() {
        // Spinner click listener
        spinner_Exp.setOnItemSelectedListener(this);

        List<String> exp = gestion.exp(getApplicationContext());

        // Creating adapter for spinner
        ArrayAdapter<String> expAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, exp);

        // Drop down layout style - list view with radio button
        expAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_Exp.setAdapter(expAdapter);
    }

    public void gestionExperience() {
        switch (spinner_Exp.getSelectedItemPosition()) {
            case 0:
                et_exp.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 1:
                et_exp.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 2:
                et_exp.setText("4");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 3:
                et_exp.setText("4");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 4:
                et_exp.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 5:
                et_exp.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 6:
                et_exp.setText("8");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_exp.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

    }

    // add Items On Spinner Age
    public void addItemsOnSpinner_Age() {
        // Spinner click listener
        spinner_Age.setOnItemSelectedListener(this);

        List<String> age = gestion.age(getApplicationContext());

        // Creating adapter for spinner
        ArrayAdapter<String> ageAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, age);

        // Drop down layout style - list view with radio button
        ageAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_Age.setAdapter(ageAdapter);
    }

    public void gestionAge() {
        switch (spinner_Age.getSelectedItemPosition()) {
            case 1:
                et_age.setText("16");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 2:
                et_age.setText("14");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 3:
                et_age.setText("12");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 4:
                et_age.setText("10");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 5:
                et_age.setText("8");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 6:
                et_age.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 7:
                et_age.setText("4");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 8:
                et_age.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 9:
                et_age.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_age.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

    }


    public void addItemsOnSpinner_langues() {
        /**  Spinner  Français   **/

        /*  Spinner Compréhension Orale   */
        // Spinner click listener
        spinner_fco.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> francais = new ArrayList<String>();
        francais.add(" ");
        francais.add("B1");
        francais.add("B2 16/25 +");
        francais.add("C1 16/25 +");
        francais.add("C2 32/50 +");

        // Creating adapter for spinner
        ArrayAdapter<String> fcoAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, francais);

        // Drop down layout style - list view with radio button
        fcoAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_fco.setAdapter(fcoAdapter);

        //TODO*  Spinner Compréhension Ecrite   */
        // Spinner click listener
        spinner_fce.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> fceAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, francais);

        // Drop down layout style - list view with radio button
        fceAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_fce.setAdapter(fceAdapter);

        /*  Spinner Production Orale   */

        // Spinner click listener
        spinner_fpo.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> fpoAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, francais);

        // Drop down layout style - list view with radio button
        fpoAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_fpo.setAdapter(fpoAdapter);

        /*  Spinner Production Ecrite   */
        // Spinner click listener
        spinner_fpe.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> fpeAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, francais);

        // Drop down layout style - list view with radio button
        fpeAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_fpe.setAdapter(fpeAdapter);

        /**  Spinner  Anglais   **/

        // Spinner Drop down elements
        List<String> comparalanglais = new ArrayList<String>();
        comparalanglais.add(" ");
        comparalanglais.add("1.0 - 4.5");
        comparalanglais.add("5.0 - 7.5");
        comparalanglais.add("8.0 - 9.0");

        List<String> compEcritAnglais = new ArrayList<String>();
        compEcritAnglais.add(" ");
        compEcritAnglais.add("1.0 - 3.5");
        compEcritAnglais.add("5.0 - 6.5");
        compEcritAnglais.add("7.0 - 9.0");

        List<String> prodAnglais = new ArrayList<String>();
        prodAnglais.add(" ");
        prodAnglais.add("1.0 - 4.5");
        prodAnglais.add("5.0 - 6.5");
        prodAnglais.add("7.0 - 9.0");

        /*  Spinner Compréhension Orale   */
        // Spinner click listener
        spinner_aco.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> acoAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, comparalanglais);

        // Drop down layout style - list view with radio button
        acoAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_aco.setAdapter(acoAdapter);

        /*  Spinner Compréhension Ecrite   */
        // Spinner click listener
        spinner_ace.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> aceAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, compEcritAnglais);

        // Drop down layout style - list view with radio button
        aceAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_ace.setAdapter(aceAdapter);

        /*  Spinner Compréhension Ecrite   */
        // Spinner click listener
        spinner_apo.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> apoAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, prodAnglais);

        // Drop down layout style - list view with radio button
        apoAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_apo.setAdapter(apoAdapter);

        /*  Spinner Compréhension Ecrite   */
        // Spinner click listener
        spinner_ape.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> apeAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, prodAnglais);

        // Drop down layout style - list view with radio button
        apeAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_ape.setAdapter(apeAdapter);
    }

    public void gestionLangues() {

        switch (String.valueOf(spinner_fco.getSelectedItem())) {
            case "B1":
                et_fco.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "B2 16/25 +":
                et_fco.setText("5");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C1 16/25 +":
                et_fco.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C2 32/50 +":
                et_fco.setText("7");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_fco.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

        switch (String.valueOf(spinner_fpo.getSelectedItem())) {
            case "B1":
                et_fpo.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "B2 16/25 +":
                et_fpo.setText("5");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C1 16/25 +":
                et_fpo.setText("6");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C2 32/50 +":
                et_fpo.setText("7");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_fpo.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

        if ((String.valueOf(spinner_fce.getSelectedItem()).equals("B1")) || (String.valueOf(spinner_fce.getSelectedItem()).equals(" "))) {
            et_fce.setText("0");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else {
            et_fce.setText("1");
            tv_monScrore.setText(String.valueOf(calculScore()));
        }

        if ((String.valueOf(spinner_fpe.getSelectedItem()).equals("B1")) || (String.valueOf(spinner_fpe.getSelectedItem()).equals(" "))) {
            et_fpe.setText("0");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else {
            et_fpe.setText("1");
            tv_monScrore.setText(String.valueOf(calculScore()));
        }

        switch (String.valueOf(spinner_aco.getSelectedItem())) {
            case "1.0 - 4.5":
                et_aco.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "5.0 - 7.5":
                et_aco.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "8.0 - 9.0":
                et_aco.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_aco.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

        switch (String.valueOf(spinner_apo.getSelectedItem())) {
            case "1.0 - 4.5":
                et_apo.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "5.0 - 6.5":
                et_apo.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "7.0 - 9.0":
                et_apo.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_apo.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

        if ((String.valueOf(spinner_ace.getSelectedItem()).equals("1.0 - 3.5")) || (String.valueOf(spinner_ace.getSelectedItem()).equals(" "))) {
            et_ace.setText("0");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else {
            et_ace.setText("1");
            tv_monScrore.setText(String.valueOf(calculScore()));
        }

        if (String.valueOf(spinner_ape.getSelectedItem()).equals("1.0 - 4.5") || (String.valueOf(spinner_ape.getSelectedItem()).equals(" "))) {
            et_ape.setText("0");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else {
            et_ape.setText("1");
            tv_monScrore.setText(String.valueOf(calculScore()));
        }
    }

    public void addItemsOnSpinner_Sejours() {
        // Spinner click listener
        spinner_sejour.setOnItemSelectedListener(this);

        List<String> sejour = gestion.sejour(getApplicationContext());

        // Creating adapter for spinner
        ArrayAdapter<String> sejourAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, sejour);

        // Drop down layout style - list view with radio button
        sejourAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_sejour.setAdapter(sejourAdapter);
    }

    public void gestionSejours() {
        String v = String.valueOf(spinner_sejour.getSelectedItem());
        if (v.equals(getResources().getString(R.string.other_stays_spinner))) {
            et_sejour.setText("2");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else if (v.equals(getResources().getString(R.string.less_3month_stays_spinner))) {
            et_sejour.setText("1");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else if (v.equals(" ")) {
            et_sejour.setText("0");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else {
            et_sejour.setText("5");
            tv_monScrore.setText(String.valueOf(calculScore()));
        }
    }

    public void addItemsOnSpinner_Famille() {
        // Spinner click listener
        spinner_famille.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> famille = new ArrayList<String>();
        famille.add(" ");
        famille.add(getResources().getString(R.string.family_in_quebec));

        // Creating adapter for spinner
        ArrayAdapter<String> famAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, famille);

        // Drop down layout style - list view with radio button
        famAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_famille.setAdapter(famAdapter);
    }

    public void gestionFamille() {

        String v = String.valueOf(spinner_famille.getSelectedItem());
        if (v.equals(getResources().getString(R.string.family_in_quebec))) {
            et_famille.setText("3");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else {
            et_famille.setText("0");
            tv_monScrore.setText(String.valueOf(calculScore()));
        }
    }

    public void addItemsOnSpinner_Emploi() {

        // Spinner click listener
        spinner_emploi.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> emploi = new ArrayList<String>();
        emploi.add(" ");
        emploi.add(getResources().getString(R.string.job_offer_inside_cmm));
        emploi.add(getResources().getString(R.string.job_offer_outside_cmm));

        // Creating adapter for spinner
        ArrayAdapter<String> empAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, emploi);

        // Drop down layout style - list view with radio button
        empAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_emploi.setAdapter(empAdapter);
    }

    public void addItemsOnSpinner_Emploi_outside_cmm() {

        // Spinner click listener
        spinner_emploi_outside_cmm.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> emploi = new ArrayList<String>();
        emploi.add(getResources().getString(R.string.abitibi_out_cmm));
        emploi.add(getResources().getString(R.string.bas_st_laurent_out_cmm));
        emploi.add(getResources().getString(R.string.capitale_nat_out_cmm));
        emploi.add(getResources().getString(R.string.quebec_center_out_cmm));
        emploi.add(getResources().getString(R.string.chaudiere_out_cmm));
        emploi.add(getResources().getString(R.string.cote_nord_out_cmm));
        emploi.add(getResources().getString(R.string.estrie_out_cmm));
        emploi.add(getResources().getString(R.string.gaspesie_out_cmm));
        emploi.add(getResources().getString(R.string.lanaudiere_out_cmm));
        emploi.add(getResources().getString(R.string.laurentides_out_cmm));
        emploi.add(getResources().getString(R.string.mauricie_out_cmm));
        emploi.add(getResources().getString(R.string.monteregie_out_cmm));
        emploi.add(getResources().getString(R.string.quebec_nord_out_cmm));
        emploi.add(getResources().getString(R.string.outaouais_out_cmm));
        emploi.add(getResources().getString(R.string.saguenay_out_cmm));

        // Creating adapter for spinner
        ArrayAdapter<String> empAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, emploi);

        // Drop down layout style - list view with radio button
        empAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_emploi_outside_cmm.setAdapter(empAdapter);
    }

    public void gestionEmploi() {

        if (String.valueOf(spinner_emploi.getSelectedItem()).equals(getResources().getString(R.string.job_offer_inside_cmm))) {
            spinner_emploi_outside_cmm.setVisibility(View.GONE);
            et_emploi.setText("8");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else if (String.valueOf(spinner_emploi.getSelectedItem()).equals(getResources().getString(R.string.job_offer_outside_cmm))) {
            spinner_emploi_outside_cmm.setVisibility(View.VISIBLE);
            et_emploi.setText("13");
            tv_monScrore.setText(String.valueOf(calculScore()));
        } else {
            et_emploi.setText("0");
            tv_monScrore.setText(String.valueOf(calculScore()));
        }
    }

    public void gestionJobOfferOutsideCMM() {

        spinner_emploi_outside_cmm.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {

                switch (spinner_emploi_outside_cmm.getSelectedItemPosition()) {
                    case 0:
                        et_emploi.setText("13");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 1:
                        et_emploi.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 2:
                        et_emploi.setText("14");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 3:
                        et_emploi.setText("13");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 4:
                        et_emploi.setText("14");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 5:
                        et_emploi.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 6:
                        et_emploi.setText("13");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 7:
                        et_emploi.setText("10");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 8:
                        et_emploi.setText("13");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 9:
                        et_emploi.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 10:
                        et_emploi.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 11:
                        et_emploi.setText("14");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 12:
                        et_emploi.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 13:
                        et_emploi.setText("13");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case 14:
                        et_emploi.setText("12");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    default:
                        et_emploi.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                }
            }
        });
    }

    public void addListenerOnChkSejour() {

        chkSejour = (CheckBox) findViewById(R.id.checkbox_sejour);

        chkSejour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkSejour checked?
                if (((CheckBox) v).isChecked()) {
                    spinner_sejour.setVisibility(View.VISIBLE);
                    et_sejour.setText("0");
                    spinner_sejour.setSelection(0);
                } else {
                    spinner_sejour.setVisibility(View.INVISIBLE);
                    et_sejour.setText("0");
                    spinner_sejour.setSelection(0);
                }

            }
        });
    }

    public void addListenerOnChkFamille() {

        chkFam = (CheckBox) findViewById(R.id.checkbox_fam);

        chkFam.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkSejour checked?
                if (((CheckBox) v).isChecked()) {
                    spinner_famille.setVisibility(View.VISIBLE);
                    et_famille.setText("0");
                    spinner_famille.setSelection(0);
                } else {
                    spinner_famille.setVisibility(View.INVISIBLE);
                    et_famille.setText("0");
                    spinner_famille.setSelection(0);
                }
            }
        });
    }

    public void addListenerOnChkEmploi() {

        chkEmploi = (CheckBox) findViewById(R.id.checkbox_emploi);

        chkEmploi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkSejour checked?
                if (((CheckBox) v).isChecked()) {
                    spinner_emploi.setVisibility(View.VISIBLE);
                    et_emploi.setText("0");
                    spinner_emploi.setSelection(0);
                } else {
                    spinner_emploi.setVisibility(View.INVISIBLE);
                    spinner_emploi_outside_cmm.setVisibility(View.GONE);
                    et_emploi.setText("0");
                    spinner_emploi.setSelection(0);
                    spinner_emploi_outside_cmm.setSelection(0);
                }
            }
        });
    }

    // add Items On Spinner nbr enfants -12
    public void addItemsOnSpinner_nbr12() {
        // Spinner click listener
        spinner_enf12.setOnItemSelectedListener(this);

        List<String> age = gestion.enf12();


        // Creating adapter for spinner
        ArrayAdapter<String> enf12Adapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, age);

        // Drop down layout style - list view with radio button
        enf12Adapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_enf12.setAdapter(enf12Adapter);
    }

    public void gestionNbr12() {

        spinner_enf12.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {

                switch (String.valueOf(spinner_enf12.getSelectedItem())) {
                    case "0":
                        et_scorEnf12.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case "1":
                        et_scorEnf12.setText("4");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case "2+":
                        et_scorEnf12.setText("8");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    default:
                        et_scorEnf12.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                }
            }
        });
    }

    // add Items On Spinner nbr enfants 13-18
    public void addItemsOnSpinner_nbr1318() {
        // Spinner click listener
        spinner_enf1318.setOnItemSelectedListener(this);

        List<String> age = gestion.enf1318();

        // Creating adapter for spinner
        ArrayAdapter<String> enf12Adapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, age);

        // Drop down layout style - list view with radio button
        enf12Adapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_enf1318.setAdapter(enf12Adapter);
    }

    public void gestionNbr1318() {

        spinner_enf1318.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {

                switch (String.valueOf(spinner_enf1318.getSelectedItem())) {
                    case "0":
                        et_scorEnf1318.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case "1":
                        et_scorEnf1318.setText("2");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case "2":
                        et_scorEnf1318.setText("4");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case "3":
                        et_scorEnf1318.setText("6");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                    case "4":
                        et_scorEnf1318.setText("8");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;

                    default:
                        et_scorEnf12.setText("0");
                        tv_monScrore.setText(String.valueOf(calculScore()));
                        break;
                }
            }
        });
    }

    public void addListenerOnChkCapFin() {

        chkCapFin = (CheckBox) findViewById(R.id.checkbox_capacité);

        chkCapFin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkSejour checked?
                if (((CheckBox) v).isChecked()) {
                    et_CapFin.setText("1");
                    tv_monScrore.setText(String.valueOf(calculScore()));
                } else {
                    et_CapFin.setText("0");
                    tv_monScrore.setText(String.valueOf(calculScore()));
                }
            }
        });
    }

    public void resetAll() {
        et_CapFin.setText("0");
        et_form.setText("0");
        et_scorScol.setText("0");
        et_age.setText("0");
        et_sejour.setText("0");
        et_scorScol.setText("0");
        et_ace.setText("0");
        et_aco.setText("0");
        et_apo.setText("0");
        et_ape.setText("0");
        et_emploi.setText("0");
        et_exp.setText("0");
        et_famille.setText("0");
        et_fpo.setText("0");
        et_fce.setText("0");
        et_fco.setText("0");
        et_fpe.setText("0");
        tv_monScrore.setText("0");

        spinner_Scolarité.setSelection(0);
        rg_form.clearCheck();
        spinner_univ.setVisibility(View.INVISIBLE);
        spinner_colleg.setVisibility(View.INVISIBLE);
        spinner_seconPro.setVisibility(View.INVISIBLE);
        spinner_Exp.setSelection(0);
        spinner_Age.setSelection(0);
        spinner_aco.setSelection(0);
        spinner_ace.setSelection(0);
        spinner_ape.setSelection(0);
        spinner_apo.setSelection(0);
        spinner_fco.setSelection(0);
        spinner_fce.setSelection(0);
        spinner_fpe.setSelection(0);
        spinner_fpo.setSelection(0);
        spinner_sejour.setSelection(0);
        spinner_sejour.setVisibility(View.INVISIBLE);
        spinner_famille.setSelection(0);
        spinner_famille.setVisibility(View.INVISIBLE);
        spinner_emploi.setSelection(0);
        spinner_emploi.setVisibility(View.INVISIBLE);
        spinner_emploi_outside_cmm.setSelection(0);
        spinner_emploi_outside_cmm.setVisibility(View.GONE);

        chkFam.setChecked(false);
        chkEmploi.setChecked(false);
        chkCapFin.setChecked(false);
        chkSejour.setChecked(false);
    }

    //TODO: CREATE PDF
    public void createPDF() throws FileNotFoundException, DocumentException {

        //create document file
        Document doc = new Document();
        try {

            Log.e("PDFCreator", "PDF Path: " + path);
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            file = new File(dir, getResources().getString(R.string.rating_sheet_pdf_name) + ".pdf");
            //file = new File(dir, getResources().getString(R.string.file_pfd_save) + ".pdf");
            //file = new File(dir, "Scores PDF" + sdf.format(Calendar.getInstance().getTime()) + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfWriter writer = PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();
            //create table
            PdfPTable pt = new PdfPTable(2);
            pt.setWidthPercentage(100);
            float[] fl = new float[]{20, 80};
            pt.setWidths(fl);
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);

            //set drawable in cell
            Drawable myImage = MainActivity.this.getResources().getDrawable(R.drawable.ic_logo);
            Bitmap bitmap = ((BitmapDrawable) myImage).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bitmapdata = stream.toByteArray();
            try {
                bgImage = Image.getInstance(bitmapdata);
                bgImage.setAbsolutePosition(330f, 642f);
                cell.addElement(bgImage);
                pt.addCell(cell);
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                Paragraph p1 = new Paragraph(
                        String.format(getResources().getString(R.string.app_name)),
                        new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.WHITE));
                cell.addElement(p1);

                Paragraph p2 = new Paragraph(
                        String.format(getResources().getString(R.string.body_pdf_message)),
                        new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.WHITE));
                cell.addElement(p2);
                //cell.addElement(new Paragraph(""));
                pt.addCell(cell);
                /*cell = new PdfPCell(new Paragraph("aaaaa"));
                cell.setBorder(Rectangle.NO_BORDER);
                pt.addCell(cell);*/

                PdfPTable pTable = new PdfPTable(1);
                pTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.setColspan(1);
                cell.addElement(pt);
                pTable.addCell(cell);
                PdfPTable table = new PdfPTable(3);

                float[] columnWidth = new float[]{30, 70, 30};
                table.setWidths(columnWidth);


                cell = new PdfPCell();


                cell.setBackgroundColor(myColor);
                cell.setColspan(6);
                cell.addElement(pTable);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(" "));
                cell.setColspan(6);
                table.addCell(cell);
                cell = new PdfPCell();
                cell.setColspan(6);

                cell.setBackgroundColor(myColor2);

                cell = new PdfPCell(new Phrase(getResources().getString(R.string.message_tresh_pdf)));
                cell.setBackgroundColor(myColor2);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(getResources().getString(R.string.criteria_pdf)));
                cell.setBackgroundColor(myColor2);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(getResources().getString(R.string.points_pdf)));
                cell.setBackgroundColor(myColor2);
                table.addCell(cell);

                //table.setHeaderRows(3);
                cell = new PdfPCell();
                cell.setColspan(6);

                table.addCell(" 2 points");
                table.addCell(getResources().getString(R.string.string_educational_level));
                table.addCell(getCell(" " + et_scorScol.getText().toString() + " / 14"));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.string_training_area));
                table.addCell(getCell(" " + et_form.getText().toString() + " / 12"));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.string_professionnel_exp2));
                table.addCell(getCell(" " + et_exp.getText().toString() + " / 8 "));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.string_age));
                table.addCell(getCell(" " + et_age.getText().toString() + " / 16 "));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.fr_oral_comprehension_pdf));
                table.addCell(getCell(" " + et_fco.getText().toString() + " / 7 "));
                table.addCell(" ");
                table.addCell("                     " + getResources().getString(R.string.string_oral_production));
                table.addCell(getCell(" " + et_fpo.getText().toString() + " / 7 "));
                table.addCell(" ");
                table.addCell("                     " + getResources().getString(R.string.string_write_comprehension));
                table.addCell(getCell(" " + et_fce.getText().toString() + " / 1 "));
                table.addCell(" ");
                table.addCell("                     " + getResources().getString(R.string.string_write_production));
                table.addCell(getCell(" " + et_fpe.getText().toString() + " / 1 "));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.en_oral_comprehension_pdf));
                table.addCell(getCell(" " + et_aco.getText().toString() + " / 2 "));
                table.addCell(" ");
                table.addCell("                   " + getResources().getString(R.string.string_oral_production));
                table.addCell(getCell(" " + et_apo.getText().toString() + " / 2 "));
                table.addCell(" ");
                table.addCell("                   " + getResources().getString(R.string.string_write_comprehension));
                table.addCell(getCell(" " + et_ace.getText().toString() + " / 1 "));
                table.addCell(" ");
                table.addCell("                   " + getResources().getString(R.string.string_write_production));
                table.addCell(getCell(" " + et_ape.getText().toString() + " / 1 "));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.string_quebec_visit));
                table.addCell(getCell(" " + et_sejour.getText().toString() + " / 5 "));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.family_in_quebec));
                table.addCell(getCell(" " + et_famille.getText().toString() + " / 3 "));
                table.addCell(" ");
                table.addCell(getResources().getString(R.string.string_validated_job_offer_pdf));
                table.addCell(getCell(" " + et_emploi.getText().toString() + " / 14 "));
                table.addCell(getCellColor(" " + (scoreEmp + 1) + " " + getResources().getString(R.string.points_pdf)));
                table.addCell(getCellColor(getResources().getString(R.string.string_tresh_work_pdf)));
                table.addCell(getCellColorCenter(" " + (scoreEmploy()) + " / " + (scoreEmp + 1) + " "));
                table.addCell(" ");
                table.addCell(" " + getResources().getString(R.string.string_children_pdf));
                table.addCell(getCell(" " + et_scorenf.getText().toString() + " / 8 "));
                table.addCell("  ");
                table.addCell(getResources().getString(R.string.string_financial_autonomy_pdf) + " ");
                table.addCell(getCell(" " + et_CapFin.getText().toString() + " / 1 "));


                PdfPTable ftable = new PdfPTable(2);
                ftable.setWidthPercentage(100);
                float[] columnWidthaa = new float[]{77, 23};
                ftable.setWidths(columnWidthaa);
                cell = new PdfPCell();
                cell.setColspan(6);
                cell.setBackgroundColor(myColor1);
                cell = new PdfPCell(new Phrase(getResources().getString(R.string.tresh_prem_pdf)));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase("             " + tv_monScrore.getText().toString() + " / " + (scoreTot + 1)));
                //cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                ftable.addCell(getCell2(msgPDF));
                cell = new PdfPCell();
                cell.setColspan(6);
                cell.addElement(ftable);
                table.addCell(cell);
                doc.add(table);

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.save_pdf_message), Toast.LENGTH_LONG).show();

            } catch (DocumentException de) {
                Log.e("PDFCreator", "DocumentException:" + de);
            } catch (IOException e) {
                Log.e("PDFCreator", "ioException:" + e);
            } finally {
                doc.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PdfPCell getCell(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(1);
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        p.setAlignment(Element.ALIGN_CENTER);
        cell.addElement(p);
        return cell;
    }

    private PdfPCell getCellColor(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(1);
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        cell.setBackgroundColor(myColor1);
        cell.addElement(p);
        return cell;
    }

    private PdfPCell getCellColorCenter(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(3);
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        p.setAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(myColor1);
        cell.addElement(p);
        return cell;
    }

    private PdfPCell getCell2(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(3);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        p.setAlignment(Element.ALIGN_CENTER);
        cell.addElement(p);
        return cell;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.write_permission_message), Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    // [START onBackPressed]
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, AccueilActivity.class));
    }


}

