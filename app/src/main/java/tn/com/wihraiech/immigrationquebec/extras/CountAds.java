package tn.com.wihraiech.immigrationquebec.extras;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Wissem Hraiech on 30/03/2017.
 */

public class CountAds {
    public static void incrementCounter(Context context){
        SharedPreferences sp = context.getSharedPreferences("sp_db", Context.MODE_PRIVATE);
        int count = sp.getInt("count", 0);
        count++;
        sp.edit().putInt("count", count).apply();
    }

    public static boolean isUpToShowAd( Context context ){
        SharedPreferences sp = context.getSharedPreferences("sp_db", Context.MODE_PRIVATE);
        int count = sp.getInt("count", 0);
        return count % 3 == 0;
    }
}
