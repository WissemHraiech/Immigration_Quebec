package tn.com.wihraiech.immigrationquebec.adapter;




import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseImmQuebAdapter {
	
	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;
	// Database Name
	private static final String DATABASE_NAME = "ImmQuebDB";
	// Vehicule Table Name
	public static final String TABLE_SCORE = "score";
	// vehicule Table Columns names
	public static final String KEY_ID = "_id";
	public static final String NOM = "nom";
	public static final String SCORE = "score";
	public static final String CREATED_AT = "created_at";

		// Context of application who uses us.
		private final Context context;
		
		private DataBaseHandler myDBHelper;
		private SQLiteDatabase db;
		
		public DataBaseImmQuebAdapter(Context ctx) {
			this.context = ctx;
			myDBHelper = new DataBaseHandler(context);
		}
		
		// Close the database connection.
		public void close() {
			myDBHelper.close();
		}

	
	// Open the database connection.
		public DataBaseImmQuebAdapter open() {
			db = myDBHelper.getWritableDatabase();
			return this;
		}
		
		public Cursor selectQuery(String query) {
			  Cursor c1 = null;
			  try {
			 
			   if (db.isOpen()) {
			    db.close();
			 
			   }
			   db = myDBHelper.getWritableDatabase();
			   c1 = db.rawQuery(query, null);
			 
			  } catch (Exception e) {
			 
			   System.out.println("DATABASE ERROR " + e);
			 
			  }
			  return c1;
			 
			 }
		
		// Deleting single Score
		public void deleteScore(long id_pres) {
			db.delete(TABLE_SCORE, KEY_ID + " = ?",
			new String[] { String.valueOf(id_pres) });
			//db.close();
		}
		
		
		
		public class DataBaseHandler extends SQLiteOpenHelper {
			
			public DataBaseHandler(Context context) {
				super(context,DATABASE_NAME, null, DATABASE_VERSION);
				// TODO Auto-generated constructor stub
			}

			@Override
			public void onCreate(SQLiteDatabase db) {
				String CREATE_SCORE_TABLE = "CREATE TABLE " + TABLE_SCORE + "("
						+ KEY_ID + " INTEGER PRIMARY KEY," + NOM + " TEXT," + SCORE + " TEXT,"
						+ CREATED_AT + " DATETIME DEFAULT CURRENT_DATE" + ")";
						db.execSQL(CREATE_SCORE_TABLE);

			}

			@Override
			public void onUpgrade(SQLiteDatabase db,int oldVersion, int newVersion) {
				// Upgrading older table if existed
				Log.w(DATABASE_NAME, "Upgrading database from version "
				        + oldVersion + " to " + newVersion
				        + ", which will destroy all old data");
				// Drop older table if existed
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
				// Create tables again
				onCreate(db);

			}
			
		}

}
