package tn.com.wihraiech.immigrationquebec.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;

import java.util.regex.Pattern;

import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.extras.PatternEditableBuilder;

import static tn.com.wihraiech.immigrationquebec.GlobalFunctions.loadAdBanner;

/**
 * Created by lenovo on 11/12/2016.
 */
public class ImmigrationFragment extends Fragment {

    private TextView tv_google;
    private final String googlePlayURL = Config.GOOGLEPLAY_URL;
    private com.google.android.gms.ads.AdView mAdView;

    private com.facebook.ads.AdView adView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_immigration, container, false);

        //ADMOB
        mAdView = view.findViewById(R.id.adView_imm);
        loadAdBanner(mAdView, getContext());

        //Animation
        YoYo.with(Techniques.ZoomInUp)
                .duration(700)
                .playOn(view.findViewById(R.id.tv_nouvel));

        //Animation
        YoYo.with(Techniques.ZoomInUp)
                .duration(700)
                .playOn(view.findViewById(R.id.tv_priorit));

        tv_google = (TextView) view.findViewById(R.id.tv_google);
        // GooglePlay HyperLink
        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.green),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Uri uriUrl = Uri.parse(googlePlayURL);
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                startActivity(launchBrowser);
                            }
                        }).into(tv_google);


        // TODO: Instantiate an AdView view
        // Find the Ad Container
        /*RelativeLayout adViewContainer = (RelativeLayout) view.findViewById(R.id.immAdViewContainer);
        adView = new com.facebook.ads.AdView(getContext(), getResources().getString(R.string.facebook_banner_imm), AdSize.BANNER_320_50);
        adViewContainer.addView(adView);
        adView.loadAd();

        // START FACEBOOK AdListener for AdView
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                //Toast.makeText(SecondActivity.this, "Error: " + adError.getErrorMessage(),Toast.LENGTH_LONG).show();
                Log.i("adError:", adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });

        // Request an ad
        adView.loadAd();
*/
        return view;
    }

    // Add to each long-lived activity
    @Override
    public void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(getContext());
    }

    // for Android, you should also log app deactivation
    @Override
    public void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(getContext());
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

}
