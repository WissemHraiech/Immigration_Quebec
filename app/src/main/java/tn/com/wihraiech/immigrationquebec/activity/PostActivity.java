package tn.com.wihraiech.immigrationquebec.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by Wissem Hraiech on 13/11/2017.
 */

public class PostActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private EditText et_title, et_desc, et_date, et_link;
    private ImageButton ib_postImage;
    private Button bt_post;

    private Uri mImageUri = null;
    private ProgressDialog mProgress;

    private StorageReference mStorage;
    private DatabaseReference mDatabase;

    public static final int GALLERY_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_post);
        mToolbar.setTitle(" Add Post");
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_notification);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");

        mProgress = new ProgressDialog(this);

        et_title = (EditText) findViewById(R.id.et_title);
        et_desc = (EditText) findViewById(R.id.et_description);
        et_date = (EditText) findViewById(R.id.et_date);
        et_link = (EditText) findViewById(R.id.et_link);

        ib_postImage = (ImageButton) findViewById(R.id.ib_post);
        ib_postImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);
            }
        });


        bt_post = (Button) findViewById(R.id.bt_post);
        bt_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPosting();
            }
        });

    }

    private void startPosting(){

        mProgress.setMessage("Posting ...");


        final String title = et_title.getText().toString().trim();
        final String date = et_date.getText().toString().trim();
        final String desc = et_desc.getText().toString().trim();
        final String link = et_link.getText().toString().trim();

        if(!TextUtils.isEmpty(title) && (!TextUtils.isEmpty(desc) && mImageUri != null)){
            mProgress.show();

            StorageReference filePath = mStorage.child("Blog_Images").child(mImageUri.getLastPathSegment());

            filePath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri downloadUrl = taskSnapshot.getUploadSessionUri();

                    DatabaseReference newPost = mDatabase.push();

                    newPost.child("image").setValue(mImageUri.toString());
                    newPost.child("title").setValue(title);
                    newPost.child("date").setValue(date);
                    newPost.child("description").setValue(desc);
                    newPost.child("link").setValue(link);

                    mProgress.dismiss();

                    startActivity( new Intent(PostActivity.this, AccueilActivity.class) );
                }
            });

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_donate) {
            ShortcutBadger.applyCount(AccueilActivity.this, 0);
            Intent i = new Intent(AccueilActivity.this, DonateActivity.class);
            startActivity( i );
        }*/

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            startActivity( new Intent(this, AccueilActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){

            mImageUri = data.getData();
            ib_postImage.setImageURI(mImageUri);
        }

    }

}
