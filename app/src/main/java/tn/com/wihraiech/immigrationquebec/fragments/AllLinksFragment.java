package tn.com.wihraiech.immigrationquebec.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.regex.Pattern;

import tn.com.wihraiech.immigrationquebec.activity.ImagePagerActivity;
import tn.com.wihraiech.immigrationquebec.activity.VideoViewActivity;
import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.extras.PatternEditableBuilder;
import tn.com.wihraiech.immigrationquebec.R;

public class AllLinksFragment extends Fragment {
    private TextView tv_google, tv_cout, tv_tcf, tv_ielts, tv_googleAll, tv_photo;
    private ImageView iv_inscrip, iv_form, iv_paie, iv_paieAll, iv_clegc;
    private final String EXTRA_POSITION = "position", EXTRA_IDVIDEO = "idvideo";
    private final String googlePlayURL = Config.GOOGLEPLAY_URL;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_all_links, container, false);

        iv_inscrip = (ImageView) view.findViewById(R.id.iv_VdinscripAll);
        iv_form = (ImageView) view.findViewById(R.id.iv_VdformulaireAll);
        iv_paieAll = (ImageView) view.findViewById(R.id.iv_VdpaiementAll);


        iv_inscrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "_GYijw2qSC8");
                startActivity(i);
            }
        });

        iv_form.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "c3tayqec8AY");
                startActivity(i);
            }
        });

        iv_paieAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "FPw-NaMHAjc");
                startActivity(i);
            }
        });


        //LINK COÛT
        tv_cout = (TextView) view.findViewById(R.id.tv_coutAll);
        new PatternEditableBuilder().
                addPattern(Pattern.compile("Cliquer ici"), getResources().getColor(R.color.colorAccent),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Intent i = new Intent(getContext(), ImagePagerActivity.class);
                                i.putExtra(EXTRA_POSITION, 0);
                                startActivity(i);
                            }
                        }).into(tv_cout);

        //LINK TCF
        tv_tcf = (TextView) view.findViewById(R.id.tv_tcfAll);
        new PatternEditableBuilder().
                addPattern(Pattern.compile("TCF"), getResources().getColor(R.color.colorAccent),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Intent i = new Intent(getContext(), ImagePagerActivity.class);
                                i.putExtra(EXTRA_POSITION, 9);
                                startActivity(i);
                            }
                        }).into(tv_tcf);

        //LINK IELTS
        tv_ielts = (TextView) view.findViewById(R.id.tv_ieltsAll);
        new PatternEditableBuilder().
                addPattern(Pattern.compile("IELTS"), getResources().getColor(R.color.colorAccent),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Intent i = new Intent(getContext(), ImagePagerActivity.class);
                                i.putExtra(EXTRA_POSITION, 11);
                                startActivity(i);
                            }
                        }).into(tv_ielts);

        /*tv_google = (TextView) view.findViewById(R.id.tv_google);
        // GooglePlay HyperLink
        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.green),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Uri uriUrl = Uri.parse(googlePlayURL);
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                startActivity(launchBrowser);
                            }
                        }).into(tv_google);*/

        iv_paie = (ImageView) view.findViewById(R.id.iv_Vdpaie);
        iv_paie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "FPw-NaMHAjc");
                startActivity(i);
            }
        });

        iv_clegc = (ImageView) view.findViewById(R.id.iv_VdClegc);
        iv_clegc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "UM5JzpuGhWU");
                startActivity(i);
            }
        });

        //LINK COÛT
        tv_photo = (TextView) view.findViewById(R.id.tv_photo);
        // COLORED LINKS
        new PatternEditableBuilder().
                addPattern(Pattern.compile("Cliquer ici"), getResources().getColor(R.color.colorAccent),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Intent i = new Intent(getContext(), ImagePagerActivity.class);
                                i.putExtra(EXTRA_POSITION, 17);
                                startActivity(i);
                            }
                        }).into(tv_photo);

        tv_googleAll = (TextView) view.findViewById(R.id.tv_googleAll);
        // GooglePlay HyperLink
        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.green),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Uri uriUrl = Uri.parse(googlePlayURL);
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                startActivity(launchBrowser);
                            }
                        }).into(tv_googleAll);

        return view;
    }

}