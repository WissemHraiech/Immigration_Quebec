package tn.com.wihraiech.immigrationquebec.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Wissem Hraiech on 28/03/2017.
 */

public class SPLocalBase {
    private static final String PREF = "PREFERENCES";
    private static final String TIME_KEY = "time";
    private static final long DELAY = 24*60*60*1000; /* 24 HORAS */

    private static void saveTime(Context context){
        SharedPreferences sp = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        sp.edit().putLong(TIME_KEY, System.currentTimeMillis() + DELAY).apply();
    }

    public static boolean is24hrsDelayed(Context context){
        SharedPreferences sp = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        Long time = sp.getLong(TIME_KEY, 0);
        Long timeCompare = System.currentTimeMillis();

        if( time < timeCompare ){
            saveTime(context);
            return true;
        }
        return false;
    }
}
