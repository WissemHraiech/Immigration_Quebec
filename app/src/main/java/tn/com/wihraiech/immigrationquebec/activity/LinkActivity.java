package tn.com.wihraiech.immigrationquebec.activity;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.adapter.TabsLinksAdapter;
import tn.com.wihraiech.immigrationquebec.extras.SlidingTabLayout;

/**
 * Created by lenovo on 25/10/2016.
 */
public class LinkActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_link);
        //mToolbar.setTitle("Immigration Québec");
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_launcher);
        setSupportActionBar(mToolbar);

        ////////////////////TODO: ActionBar///////////////
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        TextView textView = (TextView) findViewById(R.id.idtoolbar);
        textView.setText(getResources().getString(R.string.title_action_link));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        // TODO: TABS
        mViewPager = (ViewPager) findViewById(R.id.vp_tabs_links);
        mViewPager.setAdapter(new TabsLinksAdapter(getSupportFragmentManager(), getApplicationContext()));

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_links);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorAccent));
        mSlidingTabLayout.setViewPager(mViewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return true;
    }
}
