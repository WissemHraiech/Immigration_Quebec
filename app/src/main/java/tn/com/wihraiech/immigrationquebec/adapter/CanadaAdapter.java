package tn.com.wihraiech.immigrationquebec.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;

import java.util.List;

import tn.com.wihraiech.immigrationquebec.domain.Canada;
import tn.com.wihraiech.immigrationquebec.extras.ImageHelper;
import tn.com.wihraiech.immigrationquebec.interfaces.RecyclerViewOnClickListenerHack;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by lenovo on 11/12/2016.
 */
public class CanadaAdapter extends RecyclerView.Adapter<CanadaAdapter.MyViewHolder> {
    private Context mContext;
    private List<Canada> mList;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private float scale;
    private int width;
    private int height;

    private boolean withAnimation;
    private boolean withCardLayout;

    public CanadaAdapter(Context c, List<Canada> l) {
        this(c, l, true, true);
    }

    public CanadaAdapter(Context c, List<Canada> l, boolean wa, boolean wcl) {
        mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        withAnimation = wa;
        withCardLayout = wcl;

        scale = mContext.getResources().getDisplayMetrics().density;
        width = mContext.getResources().getDisplayMetrics().widthPixels - (int) (14 * scale + 0.5f);
        height = (width / 16) * 9;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;

        if (withCardLayout) {
            v = mLayoutInflater.inflate(R.layout.item_canada_card, viewGroup, false);
        } else {
            v = mLayoutInflater.inflate(R.layout.item_image, viewGroup, false);
        }

        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {

        myViewHolder.tvProvince.setText(mList.get(position).getProvince());
        myViewHolder.tvCity.setText(mList.get(position).getCity());

       /* try{
            Picasso.with(mContext)
                    .load(mList.get(position).getPhoto())
                    //.placeholder(R.drawable.ic_loading)
                    //.error(R.drawable.ic_loading)
                    .into(myViewHolder.ivCiy);
        }
        catch (Exception e){}*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //myViewHolder.ivCiy.setImageResource(mList.get(position).getPhoto());
            try {
                Picasso.with(mContext)
                        .load(mList.get(position).getPhoto())
                        .into(myViewHolder.ivCiy);

            } catch (Exception e) {
                Log.e("Picasso_TAG", e.getMessage());
            }

        } else {
            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), mList.get(position).getPhoto());
            bitmap = bitmap.createScaledBitmap(bitmap, width, height, false);

            bitmap = ImageHelper.getRoundedCornerBitmap(mContext, bitmap, 10, width, height, false, false, true, true);
            myViewHolder.ivCiy.setImageBitmap(bitmap);
        }

        if (withAnimation) {
            try {
                YoYo.with(Techniques.FadeIn)
                        .duration(700)
                        .playOn(myViewHolder.itemView);
            } catch (Exception e) {
                Log.e("YoYo_ANIMATION_TAG", e.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public int getImage(int pos) {
        mList.get(pos).getPhoto();
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView ivCiy;
        //public SimpleDraweeView ivCiy;
        public TextView tvProvince;
        public TextView tvCity;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivCiy = (ImageView) itemView.findViewById(R.id.iv_city);
            tvProvince = (TextView) itemView.findViewById(R.id.tv_province);
            tvCity = (TextView) itemView.findViewById(R.id.tv_city);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mRecyclerViewOnClickListenerHack != null) {
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }


    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r) {
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(Canada c, int position) {
        mList.add(c);
        notifyItemInserted(position);
    }

    public void removeListItem(int position) {
        mList.remove(position);
        notifyItemRemoved(position);
    }

}
