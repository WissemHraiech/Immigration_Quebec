package tn.com.wihraiech.immigrationquebec.model;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;

import cz.msebera.android.httpclient.Header;
import tn.com.wihraiech.immigrationquebec.presenter.Presenter;

/**
 * Created by Wissem Hraiech on 29/03/2017.
 */

public class JsonHttpRequest extends JsonHttpResponseHandler {
    public static final String URI = "http://seu_host/jaguar-app/ctrl/CtrlAdmin.php";
    public static final String METODO_KEY = "metodo";
    public static final String METODO_JAGUARS = "get-jaguars";
    private Presenter presenter;

    public JsonHttpRequest( Presenter presenter ){
        this.presenter = presenter;
    }

    @Override
    public void onStart() {
        presenter.showProgressBar( true );
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

        new VersionRequester(presenter).execute();
    }

    @Override
    public void onFinish() {
        presenter.showProgressBar( false );
    }
}
