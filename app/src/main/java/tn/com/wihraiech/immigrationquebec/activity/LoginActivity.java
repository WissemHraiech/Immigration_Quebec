package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import tn.com.wihraiech.immigrationquebec.extras.UtilTCM;
import tn.com.wihraiech.immigrationquebec.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button bt_Login, bt_facebook, bt_google;
    private EditText et_email, et_password;
    private TextView tv_Signup, tv_ForgotPass;
    private RelativeLayout activity_login;
    private ProgressBar progressBar;

    private static final int RC_SIGN_IN = 1;
    private GoogleApiClient mGoogleApiClient;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        printKeyHash();

        //View
        bt_Login = (Button)findViewById(R.id.login_btn_login);
        bt_facebook = (Button) findViewById(R.id.bt_facebookLg);
        bt_google = (Button) findViewById(R.id.bt_googleLg);

        et_email = (EditText)findViewById(R.id.login_email);
        et_password = (EditText)findViewById(R.id.login_password);

        tv_Signup = (TextView)findViewById(R.id.login_btn_signup);
        tv_ForgotPass = (TextView)findViewById(R.id.login_btn_forgot_password);

        activity_login = (RelativeLayout)findViewById(R.id.intent_activity);

        progressBar = (ProgressBar) findViewById(R.id.progressBarLI);

        tv_Signup.setOnClickListener(this);
        tv_ForgotPass.setOnClickListener(this);

        bt_Login.setOnClickListener(this);
        bt_facebook.setOnClickListener(this);
        bt_google.setOnClickListener(this);


        //TODO: Check already session , if ok-> DashBoard
        auth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    Intent i = new Intent(LoginActivity.this, AccueilActivity.class);
                    startActivity(i);
                }
            }
        };

        // TODO: [ Configure Google Sign In ]
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getApplicationContext(), "Erreur d'authentification !", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        //FirebaseUser currentUser = auth.getCurrentUser();
        //updateUI(currentUser);

        auth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.login_btn_login:
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();

                if(!email.isEmpty() && !password.isEmpty()) {
                    if(email.contains("@")){
                        if (UtilTCM.verifyConnection(getApplicationContext())){
                            /// codes
                            loginUser(email, password);
                            progressBar.setVisibility(View.VISIBLE);
                        }else{
                            Snackbar snackbar = Snackbar
                                    .make(view, "Pas de connexion internet. SVP vérifiez votre wifi ou 3/4G.", Snackbar.LENGTH_LONG)
                                    .setAction("Ok", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent it = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                            startActivity(it);
                                        }
                                    });
                            // Changing action button text color
                            View sbView = snackbar.getView();
                            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();
                        }

                    } else {
                        Snackbar.make(activity_login, "Adresse e-mail incorrecte !", Snackbar.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }

                } else {

                    Snackbar.make(activity_login, "Les champs sont vides !", Snackbar.LENGTH_LONG).show();
                }
                break;

            case R.id.bt_facebookLg:
                startActivity(new Intent(LoginActivity.this, FacebookLoginActivity.class));
                //finish();
                break;

            case R.id.bt_googleLg:
                progressBar.setVisibility(View.VISIBLE);
                googleSignIn();
                break;

            case R.id.login_btn_forgot_password:
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
                //finish();
                break;

            case R.id.login_btn_signup:
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                //finish();
                break;

        }

    }

    private void loginUser(String email, final String password) {
        auth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(!task.isSuccessful())
                        {
                            progressBar.setVisibility(View.GONE);
                            Log.i("Logy", task.getException().getMessage());
                            if(task.getException().getMessage().equals("There is no user record corresponding to this identifier. The user may have been deleted."))
                            {
                                Snackbar.make(activity_login, "Aucun compte ne correspond à cette adresse e-mail ! Veuillez la vérifier et réessayer.", Snackbar.LENGTH_LONG).show();
                            }
                            else if(task.getException().getMessage().equals("The email address is badly formatted."))
                            {
                                Snackbar.make(activity_login, "Il semble que vous avez saisie une adresse e-mail mal orthographiée.", Snackbar.LENGTH_LONG).show();
                            }
                            else if(task.getException().getMessage().equals("Error description received from server: INVALID_PASSWORD"))
                            {
                                Snackbar.make(activity_login, "Mot de passe invalide", Snackbar.LENGTH_LONG).show();
                            }
                            else if(task.getException().getMessage().equals("The password is invalid or the user does not have a password."))
                            {
                                Snackbar.make(activity_login, "Le mot de passe est invalide ou l'utilisateur n'a pas de mot de passe. !", Snackbar.LENGTH_LONG).show();
                            }
                        }
                        else{
                            startActivity(new Intent(LoginActivity.this, AccueilActivity.class));
                        }
                    }
                });
    }

    //TODO [Start: Google SignIN]
    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            progressBar.setVisibility(View.GONE);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
                Log.i("logerr", "success");
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
                Log.i("logerr", "failed");
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("Log", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Log", "signInWithCredential:success");
                            FirebaseUser user = auth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Log", "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Echec d'authentication.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("tn.com.wihraiech.immigrationquebec", PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }

}
