package tn.com.wihraiech.immigrationquebec.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tn.com.wihraiech.immigrationquebec.activity.AccueilActivity;
import tn.com.wihraiech.immigrationquebec.activity.ImagePagerActivity;
import tn.com.wihraiech.immigrationquebec.adapter.ProcImageAdapter;
import tn.com.wihraiech.immigrationquebec.domain.ProcImage;
import tn.com.wihraiech.immigrationquebec.interfaces.RecyclerViewOnClickListenerHack;
import tn.com.wihraiech.immigrationquebec.R;

public class AllImagesFragment extends Fragment implements RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private List<ProcImage> mList;
    private final String EXTRA_POSITION = "position";
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mList = ((AccueilActivity) getActivity()).getSetImageList(22);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_all_images, container, false);



        //RECYCLERVIEW
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_all_images);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnItemTouchListener(new AllImagesFragment.RecycleViewTouchListener(getActivity(), mRecyclerView, this));

        // GridLayout
        GridLayoutManager llm = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);

        ProcImageAdapter adapter = new ProcImageAdapter(getActivity(), mList);
        mRecyclerView.setAdapter( adapter );

        // SWIPE REFRESH LAYOUT
        /*mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srl_image);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (UtilTCM.verifyConnection( getActivity() )){

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SystemClock.sleep(2000);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mSwipeRefreshLayout.setRefreshing(false);
                                }
                            });
                        }
                    }).start();
                }
                else {
                    mSwipeRefreshLayout.setRefreshing(false);

                    android.support.design.widget.Snackbar.make(view, "SVP vérifiez votre connexion Internet.", android.support.design.widget.Snackbar.LENGTH_LONG)
                            .setAction("Ok", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent it = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                    startActivity(it);
                                }
                            })
                            .setActionTextColor(getActivity().getResources().getColor(R.color.coloLink))
                            .show();
                }

            }
        });*/

        return view;
    }

    @Override
    public void onClickListener(View view, int position) {
        //Toast.makeText(getActivity(), "onClickListener:" + position, Toast.LENGTH_SHORT).show();
        /*CarAdapter adapter = (CarAdapter) mRecyclerView.getAdapter();
                adapter.removeListItem(position);*/

        position = position>3 ? position+1 : position;
        Intent i = new Intent(getActivity(), ImagePagerActivity.class);
        i.putExtra(EXTRA_POSITION, position);
        getActivity().startActivity(i);

    }

    @Override
    public void onLongPressClickListener(View view, int position) {
        //Toast.makeText(getActivity(), "onLongPressClickListener:" + position, Toast.LENGTH_SHORT).show();
        /*CarAdapter adapter = (CarAdapter) mRecyclerView.getAdapter();
        adapter.removeListItem(position);*/
    }

    private static class RecycleViewTouchListener implements RecyclerView.OnItemTouchListener {
        private Context mContext;
        private GestureDetector mGestureDetector;
        private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

        public RecycleViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerHack rvoclh){
            mContext = c;
            mRecyclerViewOnClickListenerHack = rvoclh;

            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if(cv != null && mRecyclerViewOnClickListenerHack != null){
                        mRecyclerViewOnClickListenerHack.onLongPressClickListener(cv,
                                rv.getChildPosition(cv) );
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if(cv != null && mRecyclerViewOnClickListenerHack != null){
                        mRecyclerViewOnClickListenerHack.onClickListener(cv,
                                rv.getChildPosition(cv));
                    }

                    return (true);
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            try {
                mGestureDetector.onTouchEvent(e);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
