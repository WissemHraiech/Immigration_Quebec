package tn.com.wihraiech.immigrationquebec.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by lenovo on 11/12/2016.
 */
public class ViewPagerAdapter extends PagerAdapter {

    private Activity activity;
    private int[] images;
    private String[] titres;
    private TextView title;
    private uk.co.senab.photoview.PhotoView image;
    private LayoutInflater inflater;

    public ViewPagerAdapter(Activity activity, int[] images, String[] titres) {
        this.activity = activity;
        this.images = images;
        this.titres = titres;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_view_pager, container, false);

        title = itemView.findViewById(R.id.tv_title);
        image = itemView.findViewById(R.id.pv_proced);

        title.setText(titres[position]);

        DisplayMetrics dis = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dis);
        int height = dis.heightPixels;
        int width = dis.widthPixels;
        image.setMinimumHeight(height);
        image.setMinimumWidth(width);

        try {

            Picasso.with(activity.getApplicationContext())
                    .load(images[position])
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_loading)
                    .into(image);
        } catch (Exception e) {
            Log.e("Picasso_TAG", e.getMessage());
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
