package tn.com.wihraiech.immigrationquebec.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.regex.Pattern;

import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.activity.ImagePagerActivity;
import tn.com.wihraiech.immigrationquebec.activity.VideoViewActivity;
import tn.com.wihraiech.immigrationquebec.extras.PatternEditableBuilder;

/**
 * Created by lenovo on 11/12/2016.
 */
public class HelpFragment extends Fragment {

    private TextView tv_google, tv_links, tv_couple, link_installapp, tv_grille;
    private ImageView iv_help;
    private final String EXTRA_POSITION = "position", EXTRA_IDVIDEO = "idvideo";
    private final String googlePlayURL = Config.GOOGLEPLAY_URL;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_help, container, false);

        iv_help = (ImageView) view.findViewById(R.id.iv_VdHelp);
        iv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "qGti_9IYsMk");
                startActivity(i);
            }
        });

        tv_google = (TextView) view.findViewById(R.id.tv_google);
        // GooglePlay HyperLink
        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.green),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Uri uriUrl = Uri.parse(googlePlayURL);
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                startActivity(launchBrowser);
                            }
                        }).into(tv_google);

        // COLORED LINKS
        tv_links = (TextView) view.findViewById(R.id.tv_linkss);
        new PatternEditableBuilder().
                addPattern( Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.colorAccent) ).
                into(tv_links);

        // COUPLE LINKS
        tv_couple = (TextView) view.findViewById(R.id.tv_linkcouple);
        new PatternEditableBuilder().
                addPattern( Pattern.compile("Ajout les points de mon épous(e)"), getResources().getColor(R.color.colorAccent) ).
                into(tv_couple);

        // GRILLE DE SELECTION LINKS
        tv_grille = (TextView) view.findViewById(R.id.tv_grille);
        new PatternEditableBuilder().
                addPattern(Pattern.compile("d'une grille de facteurs"), getResources().getColor(R.color.colorAccent),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Intent i = new Intent(getContext(), ImagePagerActivity.class);
                                i.putExtra(EXTRA_POSITION, 3);
                                startActivity(i);
                            }
                        }).into(tv_grille);

        return view;
    }

}
