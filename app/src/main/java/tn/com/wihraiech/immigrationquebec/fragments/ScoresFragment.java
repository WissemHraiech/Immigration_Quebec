package tn.com.wihraiech.immigrationquebec.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.snackbar.Snackbar;

import tn.com.wihraiech.immigrationquebec.adapter.DataBaseImmQuebAdapter;
import tn.com.wihraiech.immigrationquebec.extras.UtilTCM;
import tn.com.wihraiech.immigrationquebec.R;

import static tn.com.wihraiech.immigrationquebec.GlobalFunctions.loadAdBanner;


public class ScoresFragment extends Fragment {

    DataBaseImmQuebAdapter dbImmQuebAdapter;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private com.google.android.gms.ads.AdView mAdView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_scores, container, false);

        //ADMOB
        mAdView = view.findViewById(R.id.adView_score);
        loadAdBanner(mAdView, getContext());

        affichlisteVehicule(view);

        registerListClickCallback(view);

        // SWIPE REFRESH LAYOUT
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srl_score);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (UtilTCM.verifyConnection( getActivity() )){

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SystemClock.sleep(2000);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mSwipeRefreshLayout.setRefreshing(false);
                                }
                            });
                        }
                    }).start();
                }
                else {
                    mSwipeRefreshLayout.setRefreshing(false);

                    Snackbar.make(view, "SVP vérifiez votre connexion Internet.", Snackbar.LENGTH_LONG)
                            .setAction("Ok", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent it = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                    startActivity(it);
                                }
                            })
                            .setActionTextColor(getActivity().getResources().getColor(R.color.coloLink))
                            .show();

                }

            }
        });

        return view;
    }

    /**TODO: ADMOB **/
    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    //TODO: SQL DATABASE
    private void openDB() {
        dbImmQuebAdapter = new DataBaseImmQuebAdapter(getContext());
        dbImmQuebAdapter.open();
    }
    private void closeDB() {
        dbImmQuebAdapter.close();
    }

    //Afficher la liste des scores
    @SuppressWarnings("deprecation")
    private void affichlisteVehicule(View view) {

        openDB();

        String query = "SELECT " + DataBaseImmQuebAdapter.KEY_ID + ", " + DataBaseImmQuebAdapter.NOM + ", "
                + DataBaseImmQuebAdapter.SCORE + ", " + DataBaseImmQuebAdapter.CREATED_AT
                + " FROM " + DataBaseImmQuebAdapter.TABLE_SCORE + " " ;
        Cursor cursor = dbImmQuebAdapter.selectQuery(query);

        //Allow activity to manage lifetime of the cursor
        //Deprecated!
        getActivity().startManagingCursor(cursor);

        //Setup mapping from cursor to view fields:
        String[] fromFielsNames = new String[]
                {DataBaseImmQuebAdapter.NOM, DataBaseImmQuebAdapter.SCORE, DataBaseImmQuebAdapter.CREATED_AT};

        int[] toViewIDs = new int[]
                {R.id.tv_nom, 				R.id.tv_score, 			R.id.tv_date};


        //Create adapter to map columns of the DB into elements in the UI
        SimpleCursorAdapter myCursorAdapter =
                new SimpleCursorAdapter(getActivity(), R.layout.item_score, cursor, fromFielsNames, toViewIDs);

        //Set the adapter for the list view
        ListView listeScore = (ListView) view.findViewById(R.id.lv_scores);
        listeScore.setAdapter(myCursorAdapter);
    }

    private void registerListClickCallback(View view) {
        ListView listeVehicule = (ListView) view.findViewById(R.id.lv_scores);
        listeVehicule.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View viewClicked,
                                           int position, long idInDB) {

                displayToastForId(idInDB);
                return true;
            }

        });
    }


    private void displayToastForId(long idInDB) {

        onCreateDialog(idInDB).show();

    }

    // DELETE SCORE
    protected Dialog onCreateDialog(final long idInDB) {
        // Création d'une boite de dialogue
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setMessage("Voulez-vous supprimer ce score ?");
        builder.setCancelable(false);
        builder.setTitle("Confirmation");

        builder.setPositiveButton("OUI",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // effacement du médicament;
                        dbImmQuebAdapter.deleteScore(idInDB);
                        Toast.makeText(getContext(), "Score supprimé!", Toast.LENGTH_LONG).show();
                        //openDB();
                        affichlisteVehicule(getView());
                    }
                });

        builder.setNegativeButton("NON",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        dialog = builder.create();
        return dialog;
    }

}

