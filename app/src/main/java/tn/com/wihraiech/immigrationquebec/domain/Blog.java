package tn.com.wihraiech.immigrationquebec.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Wissem Hraiech on 12/11/2017.
 */

public class Blog implements Parcelable{

    String title, date, description, link, image;

    public Blog() {}

    public Blog(String title, String date, String description, String link, String image) {
        this.title = title;
        this.date = date;
        this.description = description;
        this.link = link;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    // PARCELABLE
    public Blog(Parcel parcel){
        setTitle(parcel.readString());
        setDate(parcel.readString());
        setDescription(parcel.readString());
        setLink(parcel.readString());
        setImage(parcel.readString());

    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString( getTitle() );
        dest.writeString( getDate() );
        dest.writeString( getDescription() );
        dest.writeString( getLink() );
        dest.writeString( getImage() );

    }
    public static final Parcelable.Creator<Blog> CREATOR = new Parcelable.Creator<Blog>(){
        @Override
        public Blog createFromParcel(Parcel source) {
            return new Blog(source);
        }
        @Override
        public Blog[] newArray(int size) {
            return new Blog[size];
        }
    };

}
