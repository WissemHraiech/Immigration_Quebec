package tn.com.wihraiech.immigrationquebec.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Wissem Hraiech on 26/04/2017.
 */

public class Video implements Parcelable {

    private long id;
    private String title;
    private int photo;

    public Video() {}

    public Video(String title, int photo) {
        this.title = title;
        this.photo = photo;
    }

    protected Video(Parcel in) {
        id = in.readLong();
        title = in.readString();
        photo = in.readInt();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }


    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeInt(photo);
    }
}

