package tn.com.wihraiech.immigrationquebec.extras;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by lenovo on 13/04/2016.
 */
public class Config {

    //TODO: [Start: GMAIL]
    public static final String EMAIL ="immigrationquebec.co@gmail.com"; // YOUR-GMAIL-USERNAME
    public static final String PASSWORD ="xxx"; // YOUR-GMAIL-PASSWORD

    //TODO: [Start: NETWORK URL]
    public static final String GOOGLEPLAY_URL = "https://play.google.com/store/apps/details?id=tn.com.wihraiech.immigrationquebec";
    public static final String FACEBOOK_URL = "https://www.facebook.com/Application.Immigration.Quebec/";
    public static final String CHANNEL_TOUTUBE_URL = "https://bit.ly/sub_WissemHraiechYT";
    public static final String LINKEDIN_URL = "https://www.linkedin.com/in/wissemhraiech";
    public static final String LOGO_INVITE_URL = "https://firebasestorage.googleapis.com/v0/b/immigration-quebec.appspot.com/o/logo_invite.jpg?alt=media&token=6c536ab0-370d-496f-bc6f-edb61376ff9c";
    public static final String APP_FACEBOOK_LINK_URL = "https://fb.me/1852538974995770";

    //TODO: [End: NETWORK URL]

    // TODO: [start: GESTION CALCUL]
    public List<String> age( Context context ){
        // Spinner Drop down elements
        List<String> age = new ArrayList <String>();
        age.add(" ");
        age.add(context.getResources().getString(R.string.age_18_35_years));
        age.add(context.getResources().getString(R.string.age_36_years));
        age.add(context.getResources().getString(R.string.age_37_years));
        age.add(context.getResources().getString(R.string.age_38_years));
        age.add(context.getResources().getString(R.string.age_39_years));
        age.add(context.getResources().getString(R.string.age_40_years));
        age.add(context.getResources().getString(R.string.age_41_years));
        age.add(context.getResources().getString(R.string.age_42_years));
        age.add(context.getResources().getString(R.string.age_43_years));

        return age;
    }

    public List<String> exp( Context context ){
        // Spinner Drop down elements
        List<String> age = new ArrayList <String>();
        age.add(" ");
        age.add(context.getResources().getString(R.string.less_6month_experience));
        age.add(context.getResources().getString(R.string.work_6_11month_experience));
        age.add(context.getResources().getString(R.string.work_12_23month_experience));
        age.add(context.getResources().getString(R.string.work_24_35month_experience));
        age.add(context.getResources().getString(R.string.work_36_47month_experience));
        age.add(context.getResources().getString(R.string.more_48month_experience));

        return age;
    }

    public List<String> sejour(Context context){
        // Spinner Drop down elements
        List<String> age = new ArrayList <String>();
        age.add(" ");
        age.add(context.getResources().getString(R.string.studies_less1800h_spinner));
        age.add(context.getResources().getString(R.string.studies_plus1800h_spinner));
        age.add(context.getResources().getString(R.string.work_permit_spinner));
        age.add(context.getResources().getString(R.string.other_stays_spinner));
        age.add(context.getResources().getString(R.string.less_3month_stays_spinner));

        return age;
    }

    public List<String> scolarité( Context context){
        // Spinner Drop down elements
        List<String> age = new ArrayList <String>();

        age.add(context.getResources().getString(R.string.second_general_schooling));
        age.add(context.getResources().getString(R.string.second_prof_schooling));
        age.add(context.getResources().getString(R.string.post_second_general2_schooling));
        age.add(context.getResources().getString(R.string.post_second_tech12_schooling));
        //age.add("Secondaire professionnel 1 an ou + ou postsecondaire technique 1 an ou 2 ans ET domaine de formation à 12 points ou à 16 points");
        age.add(context.getResources().getString(R.string.post_second_tech3_schooling));
        //age.add("Postsecondaire technique 3 ans ET domaine de formation à 12 points ou à 16 points");
        age.add(context.getResources().getString(R.string.university_1cycle1_schooling));
        age.add(context.getResources().getString(R.string.university_1cycle2y_schooling));
        age.add(context.getResources().getString(R.string.university_1cycle3y_schooling));
        age.add(context.getResources().getString(R.string.university_2cycle1y_schooling));
        age.add(context.getResources().getString(R.string.university_3cycle_schooling));

        return age;
    }

    /*public List<String> scolarité_conjoint(){
        // Spinner Drop down elements
        List<String> age = new ArrayList <String>();
        age.add(" ");
        age.add("Secondaire général");
        age.add("Secondaire professionnel");
        age.add("Postsecondaire général 2 ans");
        age.add("Postsecondaire technique 1 an ou 2 ans");
        age.add("Postsecondaire technique 3 ans");
        age.add("Universitaire 1er cycle 1 an");
        age.add("Universitaire 1er cycle 2 ans");
        age.add("Universitaire 1er cycle 3 ans ou +");
        age.add("Universitaire 2ième cycle 1 an ou +");
        age.add("Universitaire 3ième cycle");

        return age;
    }*/

    //** Cette liste prend effet le 1er novembre 2019 **//

    // Spinner Drop down Niveau universitaire elements
    public List<String> univ(){
        List<String> univ = new ArrayList <String>();
        univ.add(" ");
        univ.add("Autres professions de la santé (DOC)");
        univ.add("Chiropratique (DOC)");
        univ.add("Génie informatique et de la construction des ordinateurs(BAC)");
        univ.add("Information de gestion (BAC)");
        univ.add("Psychoéducation (MAI)");
        univ.add("Recherche opérationnelle (MAI)");
        univ.add("----------------------------------------");
        univ.add("Actuariat (BAC)");
        univ.add("Architecture (MAI)");
        univ.add("Architecture paysagiste (BAC)");
        univ.add("Architecture urbaine et aménagement (MAI) ");
        univ.add("Coopération (MAI)");
        univ.add("Criminologie (BAC)");
        univ.add("Ergothérapie (MAI)");
        univ.add("Ethnologie et ethnographie (MAI)");
        univ.add("Économie rurale et agricole (BAC)");
        univ.add("Énergie (MAI)");
        univ.add("Formation des enseignants au préscolaire et au primaire(BAC)");
        univ.add("Gestion du personnel (BAC)");
        univ.add("Génie agricole et génie rural (BAC)");
        univ.add("Génie agroforestier (MAI)");
        univ.add("Génie aérospatial, aéronautique et astronautique (BAC)");
        univ.add("Génie industriel et administratif (BAC)");
        univ.add("Génie mécanique (BAC)");
        univ.add("Génie nucléaire (MAI)");
        univ.add("Génie physique (BAC)");
        univ.add("Génie électrique, électronique et des communications (BAC)");
        univ.add("Marketing et achats (BAC)");
        univ.add("Mathématiques (BAC)");
        univ.add("Mathématiques appliquées (BAC)");
        univ.add("Optométrie (DOC)");
        univ.add("Opérations bancaires et finance (BAC)");
        univ.add("Orthophonie et audiologie (BAC)");
        univ.add("Orthophonie et audiologie (MAI)");
        univ.add("Physiothérapie (MAI)");
        univ.add("Probabilités et statistiques (BAC)");
        univ.add("Psychoéducation (BAC)");
        univ.add("Relations industrielles (BAC)");
        univ.add("Santé communautaire et épidémiologie (MAI)");
        univ.add("Sciences de l'informatique (BAC) ");
        univ.add("Sciences domestiques (BAC)");
        univ.add("Sciences infirmières et nursing (BAC)");
        univ.add("Sciences physiques (BAC)");
        univ.add("Service social (BAC)");
        univ.add("Sexologie (BAC)");
        univ.add("----------------------------------------");
        univ.add("Administration des affaires (BAC)");
        univ.add("Affaires sur le plan international (BAC)");
        univ.add("Agriculture (BAC)");
        univ.add("Animation sociale ou communautaire (BAC)");
        univ.add("Anthropologie (BAC)");
        univ.add("Architecture (BAC)");
        univ.add("Arts graphiques (communications graphiques) (BAC)");
        univ.add("Arts plastiques (peinture, dessin, sculpture) (BAC)");
        univ.add("Bibliothéconomie et archivistique (MAI)");
        univ.add("Chimie (BAC)");
        univ.add("Communications et journalisme (BAC)");
        univ.add("Comptabilité et sciences comptables (BAC)");
        univ.add("Didactique (art d'enseigner) (MAI)");
        univ.add("Démographie (MAI)");
        univ.add("Environnement (qualité du milieu et pollution) (BAC)");
        univ.add("Ergothérapie (BAC)");
        univ.add("Études géopolitiques (BAC)");
        univ.add("Formation des enseignants au préscolaire (BAC)");
        univ.add("Formation des enseignants au secondaire (BAC)");
        univ.add("Formation des enseignants spécialistes au primaire et au secondaire (BAC)");
        univ.add("Formation des enseignants spécialistes en adaptation scolaire (orthopédagogie) (BAC)");
        univ.add("Français, en général et langue maternelle (BAC)");
        univ.add("Gestion des services de santé (MAI)");
        univ.add("Gestion et administration des entreprises (BAC)");
        univ.add("Génie alimentaire (BAC)");
        univ.add("Génie biologique et biomédical (BAC)");
        univ.add("Génie civil, de la construction et du transport (BAC)");
        univ.add("Génie des pâtes et papiers (MAI)");
        univ.add("Génie géologique (BAC)");
        univ.add("Génie minier (BAC)");
        univ.add("Génie métallurgique et des matériaux (BAC)");
        univ.add("Génétique (MAI)");
        univ.add("Géologie (minéralogie, etc.) (BAC)");
        univ.add("Hydrologie et sciences de l'eau (MAI)");
        univ.add("Ingénierie (BAC)");
        univ.add("Langues et littératures françaises ou anglaises (BAC)");
        univ.add("Médecine et chirurgie expérimentales (MAI)");
        univ.add("Océanographie (MAI)");
        univ.add("Philosophie (BAC)");
        univ.add("Photographie (BAC)");
        univ.add("Physiothérapie (BAC)");
        univ.add("Psychologie (BAC)");
        univ.add("Psychologie (DOC)");
        univ.add("Psychologie (MAI)");
        univ.add("Pédagogie universitaire (MAI)");
        univ.add("Ressources naturelles (BAC)");
        univ.add("Récréologie (BAC)");
        univ.add("Sciences de l'activité physique (BAC)");
        univ.add("Sciences fondamentales et sciences appliquées de la santé (BAC)");
        univ.add("Sciences politiques (BAC)");
        univ.add("Sociologie (BAC)");
        univ.add("Traduction (BAC)");
        univ.add("Urbanisme (BAC)");

        return univ;
    }

    public List<String> colleg(){
        // Spinner Drop down elements
        List<String> colleg = new ArrayList <String>();
        colleg.add(" ");
        colleg.add("Techniques d'orthèses visuelles (DEC)");
        colleg.add("Techniques d'éducation à l'enfance (AEC)");
        colleg.add("Techniques d'électrophysiologie médicale (DEC)");
        colleg.add("Techniques de denturologie (DEC)");
        colleg.add("Techniques de l'informatique (DEC)");
        colleg.add("Techniques de santé animale (DEC)");
        colleg.add("----------------------------------------");
        colleg.add("Acupuncture (DEC)");
        colleg.add("Assainissement de l'eau (DEC)");
        colleg.add("Environnement, hygiène et sécurité au travail (DEC) ");
        colleg.add("Gestion d'un établissement de restauration (AEC)");
        colleg.add("Gestion d'un établissement de restauration (DEC)");
        colleg.add("Gestion et technologies d'entreprise agricole (AEC)");
        colleg.add("Gestion et technologies d'entreprise agricole (DEC)");
        colleg.add("Soins infirmiers (DEC)");
        colleg.add("Techniques d'avionique (AEC) ");
        colleg.add("Techniques d'avionique (DEC) ");
        colleg.add("Techniques d'hygiène dentaire (DEC)");
        colleg.add("Techniques d'intégration multimédia (DEC)");
        colleg.add("Techniques d'orthèses et de prothèses orthopédiques (DEC)");
        colleg.add("Techniques d'éducation spécialisée (DEC)");
        colleg.add("Techniques d'éducation à l'enfance (AEC) ");
        colleg.add("Techniques de gestion hôtelière (DEC)");
        colleg.add("Techniques de génie aérospatial (DEC) ");
        colleg.add("Techniques de génie mécanique (DEC)");
        colleg.add("Techniques de génie mécanique de marine (DEC)");
        colleg.add("Techniques de la plasturgie (DEC)");
        colleg.add("Techniques de pilotage d'aéronefs (DEC)");
        colleg.add("Techniques du meuble et d'ébénisterie (DEC)");
        colleg.add("Technologie de la mécanique du bâtiment (DEC)");
        colleg.add("Technologie de médecine nucléaire (DEC)");
        colleg.add("Technologie de radio-oncologie (DEC)");
        colleg.add("Technologie de radiodiagnostic (DEC)");
        colleg.add("Technologie de systèmes ordinés (DEC)");
        colleg.add("Technologie du génie métallurgique (DEC)");
        colleg.add("----------------------------------------");
        colleg.add("Assainissement de l'eau (AEC) ");
        colleg.add("Audioprothèse (DEC) ");
        colleg.add("Conseil en assurances et en services financiers (DEC)");
        colleg.add("Environnement, hygiène et sécurité au travail (AEC)");
        colleg.add("Gestion de projet en communications graphiques (DEC) ");
        colleg.add("Navigation (DEC)");
        colleg.add("Paysage et commercialisation en horticulture ornementale (DEC)");
        colleg.add("Techniques d'inhalothérapie (DEC) ");
        colleg.add("Techniques d'intervention en délinquance (AEC)");
        colleg.add("Techniques d'intervention en délinquance (DEC)");
        colleg.add("Techniques d'intervention en loisir (DEC) ");
        colleg.add("Techniques d'intégration multimédia (AEC) ");
        colleg.add("Techniques d'éducation spécialisée (AEC)");
        colleg.add("Techniques de bureautique (AEC)");
        colleg.add("Techniques de bureautique (DEC)");
        colleg.add("Techniques de diététique (DEC)");
        colleg.add("Techniques de gestion hôtelière (AEC) ");
        colleg.add("Techniques de génie aérospatial (AEC)");
        colleg.add("Techniques de génie chimique (DEC) ");
        colleg.add("Techniques de génie mécanique (AEC)");
        colleg.add("Techniques de l'informatique (AEC)");
        colleg.add("Techniques de maintenance d'aéronefs (AEC) ");
        colleg.add("Techniques de maintenance d'aéronefs (DEC) ");
        colleg.add("Techniques de procédés chimiques (DEC) ");
        colleg.add("Techniques de recherche sociale (DEC)");
        colleg.add("Techniques de thanatologie (AEC) ");
        colleg.add("Techniques de thanatologie (DEC)");
        colleg.add("Techniques de tourisme (DEC) ");
        colleg.add("Techniques de transformation des matériaux composites (AEC)");
        colleg.add("Techniques de transformation des matériaux composites (DEC) ");
        colleg.add("Techniques du meuble et d'ébénisterie (AEC)");
        colleg.add("Techniques juridiques (DEC) ");
        colleg.add("Techniques équines (DEC) ");
        colleg.add("Technologie de l'architecture (AEC)");
        colleg.add("Technologie de l'architecture (DEC) ");
        colleg.add("Technologie de l'électronique (AEC) ");
        colleg.add("Technologie de l'électronique (DEC) ");
        colleg.add("Technologie de l'électronique industrielle (DEC) ");
        colleg.add("Technologie de la géomatique (AEC) ");
        colleg.add("Technologie de la géomatique (DEC) ");
        colleg.add("Technologie de la production pharmaceutique (DEC) ");
        colleg.add("Technologie de maintenance industrielle (DEC)");
        colleg.add("Technologie des procédés et de la qualité des aliments (AEC) ");
        colleg.add("Technologie des procédés et de la qualité des aliments (DEC) ");
        colleg.add("Technologie des productions animales (DEC) ");
        colleg.add("Technologie du génie agromécanique (DEC)");
        colleg.add("Technologie du génie civil (DEC)");
        colleg.add("Technologie du génie industriel (AEC) ");
        colleg.add("Technologie du génie industriel (DEC)");
        colleg.add("Technologie du génie métallurgique (AEC)");
        colleg.add("Technologie du génie physique (DEC)");
        colleg.add("Technologie minérale (DEC)");

        return colleg;
    }

    public List<String> secondaire(){
        // Spinner Drop down elements
        List<String> second = new ArrayList <String>();
        second.add(" ");
        second.add("Boucherie de détail (DEP)");
        second.add("Grandes cultures (DEP) ");
        second.add("Mécanique agricole (DEP)");
        second.add("Mécanique d'ascenseur (DEP)");
        second.add("Mécanique d'engins de chantier (DEP)");
        second.add("Production acéricole (DEP) ");
        second.add("Production animale (DEP)");
        second.add("----------------------------------------");
        second.add("Assistance technique en pharmacie (DEP)");
        second.add("Assistance à la personne à domicile (DEP) ");
        second.add("Chaudronnerie (DEP)");
        second.add("Conduite de procédés de traitement de l'eau (DEP)");
        second.add("Cuisine (DEP)");
        second.add("Électromécanique de systèmes automatisés (DEP)");
        second.add("Forage et dynamitage (DEP)");
        second.add("Installation et fabrication de produits verriers (DEP)");
        second.add("Installation et réparation d'équipement de télécommunication (DEP)");
        second.add("Mécanique de machines fixes (DEP)");
        second.add("Mécanique de véhicules lourds routiers (DEP)");
        second.add("Régulation de vol (DEP)");
        second.add("Techniques d'usinage (DEP) ");
        second.add("----------------------------------------");
        second.add("Calorifugeage (DEP)");
        second.add("Carrosserie (DEP)");
        second.add("Charpenterie-menuiserie (DEP)");
        second.add("Coiffure (DEP)");
        second.add("Conduite et réglage de machines à mouler (DEP)");
        second.add("Conseil et vente de pièces d'équipement motorisé (DEP)");
        second.add("Conseil et vente de voyages (DEP)");
        second.add("Conseil technique en entretien et en réparation de véhicules (DEP)");
        second.add("Dessin de bâtiment (DEP) ");
        second.add("Dessin industriel (DEP)");
        second.add("Esthétique (DEP)");
        second.add("Fabrication de moules (ASP) ");
        second.add("Ferblanterie-tôlerie (DEP)");
        second.add("Fonderie (DEP) ");
        second.add("Installation de revêtements souples (DEP)");
        second.add("Installation et entretien de systèmes de sécurité (DEP) ");
        second.add("Matriçage (ASP) ");
        second.add("Mise en oeuvre de matériaux composites (DEP)");
        second.add("Modelage (DEP) ");
        second.add("Montage de câbles et de circuits (DEP)");
        second.add("Montage de structures en aérospatiale (DEP) ");
        second.add("Montage mécanique en aérospatiale (DEP) ");
        second.add("Mécanique automobile (DEP) ");
        second.add("Mécanique de protection contre les incendies (DEP) ");
        second.add("Mécanique de véhicules légers (DEP)");
        second.add("Mécanique industrielle de construction et d'entretien (DEP)");
        second.add("Opération d'équipements de production (DEP)");
        second.add("Outillage (ASP) ");
        second.add("Pâtisserie (DEP)");
        second.add("Peinture en bâtiment (DEP)");
        second.add("Plomberie et chauffage (DEP) ");
        second.add("Préparation et finition de béton (DEP) ");
        second.add("Réfrigération (DEP)");
        second.add("Santé, assistance et soins infirmiers (DEP)");
        second.add("Service de la restauration (DEP) ");
        second.add("Service de la restauration (DEP)");
        second.add("Soudage-montage (DEP) ");
        second.add("Soutien informatique (DEP) ");
        second.add("Vente-conseil (DEP) ");
        second.add("Ébénisterie (DEP) ");

        return second;
    }

    public List<String> enf12(){
        // Spinner Drop down elements
        List<String> nbr = new ArrayList <String>();
        nbr.add(" ");
        nbr.add("0");
        nbr.add("1");
        nbr.add("2+");

        return nbr;
    }

    public List<String> enf1318(){
        // Spinner Drop down elements
        List<String> nbr = new ArrayList <String>();
        nbr.add(" ");
        nbr.add("0");
        nbr.add("1");
        nbr.add("2");
        nbr.add("3");
        nbr.add("4");

        return nbr;
    }

    public List<String> pays(){
        // Spinner Drop down elements
        List<String> pays = new ArrayList<String>();
        //pays.add(" ");
        pays.add("Allemagne");
        pays.add("Afghanistan");
        pays.add("Afrique Du Sud");
        pays.add("Albanie");
        pays.add("Algérie");
        pays.add("Andorre");
        pays.add("Angola");
        pays.add("Anguilla");
        pays.add("Antarctique");
        pays.add("Antigua et Barbuda");
        pays.add("Antilles Néerlandaises");
        pays.add("Arabie Saoudite");
        pays.add("Argentine");
        pays.add("Arménie");
        pays.add("Aruba");
        pays.add("Australie");
        pays.add("Autriche");
        pays.add("Azerbaïdjan");
        pays.add("Bahamas");
        pays.add("Bahreïn");
        pays.add("Bangladesh");
        pays.add("Barbade");
        pays.add("Belgique");
        pays.add("Belize");
        pays.add("Bénin");
        pays.add("Bermudes");
        pays.add("Bhoutan");
        pays.add("Biélorussie");
        pays.add("Birmanie");
        pays.add("Bolivie");
        pays.add("Bosnie Herzégovine");
        pays.add("Botswana");
        pays.add("Brésil");
        pays.add("Brunéi Darussalam");
        pays.add("Bulgarie");
        pays.add("Burkina Faso");
        pays.add("Burundi");
        pays.add("Cambodge");
        pays.add("Cameroun");
        pays.add("Canada");
        pays.add("Cap Vert");
        pays.add("Centrafrique");
        pays.add("Chili");
        pays.add("Chine");
        pays.add("Chypre");
        pays.add("Colombie");
        pays.add("Comores");
        pays.add("Congo");
        pays.add("Corée du Nord");
        pays.add("Corée du Sud");
        pays.add("Costa Rica");
        pays.add("Côte d'Ivoire");
        pays.add("Croatie");
        pays.add("Cuba");
        pays.add("Danemark");
        pays.add("Djibouti");
        pays.add("Dominique");
        pays.add("Egypte");
        pays.add("El Salvador");
        pays.add("Emirats Arabes Unis");
        pays.add("Equateur");
        pays.add("Erythrée");
        pays.add("Espagne");
        pays.add("Estonie");
        pays.add("Etats Fédérés de Micronésie");
        pays.add("Etats-Unis");
        pays.add("Fidji");
        pays.add("Finlande");
        pays.add("France");
        pays.add("Gabon");
        pays.add("Gambie");
        pays.add("Géorgie");
        pays.add("Géorgie du Sud et Îles Sandwich du Sud");
        pays.add("Ghana");
        pays.add("Gibraltar");
        pays.add("Grèce");
        pays.add("Grenade");
        pays.add("Groenland");
        pays.add("Guadeloupe");
        pays.add("Guam");
        pays.add("Guatemala");
        pays.add("Guernesey");
        pays.add("Guinée");
        pays.add("Guinée Équatoriale");
        pays.add("Guinée-Bissau");
        pays.add("Guyana");
        pays.add("Guyane Française");
        pays.add("Haïti");
        pays.add("Honduras");
        pays.add("Hong-Kong");
        pays.add("Hongrie");
        pays.add("Ile Bouvet");
        pays.add("Ile Christmas");
        pays.add("Ile de Man");
        pays.add("Ile Mcdonald et Iles Heard");
        pays.add("Ile Norfolk");
        pays.add("Ile Sainte Hélène");
        pays.add("Iles Aland");
        pays.add("Iles Caïmanes");
        pays.add("Iles Cocos (keeling)");
        pays.add("Iles Cook");
        pays.add("Iles Féroé");
        pays.add("Iles Malouines");
        pays.add("Iles Mariannes du Nord");
        pays.add("Iles Marshall");
        pays.add("Iles mineures éloignées des États-Unis");
        pays.add("Iles Salomon");
        pays.add("Iles Turks et Caïques");
        pays.add("Iles Vierges Britanniques");
        pays.add("Iles Vierges des États-Unis");
        pays.add("Inde");
        pays.add("Indonésie");
        pays.add("Irak");
        pays.add("Iran");
        pays.add("Irlande");
        pays.add("Islande");
        pays.add("Israël");
        pays.add("Italie");
        pays.add("Jamaïque");
        pays.add("Japon");
        pays.add("Jersey");
        pays.add("Jordanie");
        pays.add("Kazakhstan");
        pays.add("Kenya");
        pays.add("Kirghizistan");
        pays.add("Kiribati");
        pays.add("Koweït");
        pays.add("Laos");
        pays.add("Lesotho");
        pays.add("Lettonie");
        pays.add("Liban");
        pays.add("Libéria");
        pays.add("Libye");
        pays.add("Liechtenstein");
        pays.add("Lituanie");
        pays.add("Luxembourg");
        pays.add("Macao");
        pays.add("Macédoine");
        pays.add("Madagascar");
        pays.add("Malaisie");
        pays.add("Malawi");
        pays.add("Maldives");
        pays.add("Mali");
        pays.add("Malte");
        pays.add("Maroc");
        pays.add("Martinique");
        pays.add("Maurice");
        pays.add("Mauritanie");
        pays.add("Mayotte");
        pays.add("Mexique");
        pays.add("Moldavie");
        pays.add("Monaco");
        pays.add("Mongolie");
        pays.add("Monténégro");
        pays.add("Montserrat");
        pays.add("Mozambique");
        pays.add("Namibie");
        pays.add("Nauru");
        pays.add("Népal");
        pays.add("Nicaragua");
        pays.add("Niger");
        pays.add("Nigéria");
        pays.add("Niué");
        pays.add("Norvège");
        pays.add("Nouvelle Calédonie");
        pays.add("Nouvelle Zélande");
        pays.add("Oman");
        pays.add("Ouganda");
        pays.add("Ouzbékistan");
        pays.add("Pakistan");
        pays.add("Palaos");
        pays.add("Panama");
        pays.add("Papouasie Nouvelle Guinée");
        pays.add("Paraguay");
        pays.add("Pays-Bas");
        pays.add("Pérou");
        pays.add("Philippines");
        pays.add("Pitcairn");
        pays.add("Pologne");
        pays.add("Polynésie Française");
        pays.add("Porto Rico");
        pays.add("Portugal");
        pays.add("Qatar");
        pays.add("République Démocratique du Congo (ex Zaïre)");
        pays.add("République Dominicaine");
        pays.add("République Tchèque");
        pays.add("Réunion");
        pays.add("Roumanie");
        pays.add("Royaume-Uni");
        pays.add("Russie");
        pays.add("Rwanda");
        pays.add("Sahara Occidental");
        pays.add("Saint Kitts et Nevis");
        pays.add("Saint Marin");
        pays.add("Saint Pierre et Miquelon");
        pays.add("Saint Siège");
        pays.add("Saint Vincent et les Grenadines");
        pays.add("Saint-Barthélemy");
        pays.add("Saint-Martin");
        pays.add("Sainte Lucie");
        pays.add("Samoa");
        pays.add("Samoa Américaines");
        pays.add("Sao Tomé et Principe");
        pays.add("Sénégal");
        pays.add("Serbie");
        pays.add("Seychelles");
        pays.add("Sierra Leone");
        pays.add("Singapour");
        pays.add("Slovaquie");
        pays.add("Slovénie");
        pays.add("Somalie");
        pays.add("Soudan");
        pays.add("Sri Lanka");
        pays.add("Suède");
        pays.add("Suisse");
        pays.add("Suriname");
        pays.add("Svalbard et Ile Jan Mayen");
        pays.add("Swaziland");
        pays.add("Syrie");
        pays.add("Tadjikistan");
        pays.add("Taïwan");
        pays.add("Tanzanie");
        pays.add("Tchad");
        pays.add("Terres Australes Françaises");
        pays.add("Territoires palestiniens");
        pays.add("Thaïlande");
        pays.add("Timor Oriental");
        pays.add("Togo");
        pays.add("Tokelau");
        pays.add("Tonga");
        pays.add("Trinité et Tobago");
        pays.add("Tunisie");
        pays.add("Turkménistan");
        pays.add("Turquie");
        pays.add("Tuvalu");
        pays.add("Ukraine");
        pays.add("Uruguay");
        pays.add("Vanuatu");
        pays.add("Vénézuela");
        pays.add("Viet Nam");
        pays.add("Wallis et Futuna");
        pays.add("Yémen");
        pays.add("Zambie");
        pays.add("Zimbabwe");

        return pays;
    }

}
