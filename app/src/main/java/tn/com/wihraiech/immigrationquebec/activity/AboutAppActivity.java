package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.regex.Pattern;

import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.extras.PatternEditableBuilder;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by Wissem Hraiech on 15/10/2017.
 */

public class AboutAppActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView tv_google;
    private final String googlePlayURL = Config.GOOGLEPLAY_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_aboutApp);
        setSupportActionBar(mToolbar);

        ////////////////////ActionBar///////////////
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        TextView textView = (TextView) findViewById(R.id.idtoolbar);
        textView.setText(getResources().getString(R.string.title_activity_about));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        tv_google = (TextView) findViewById(R.id.tv_googlePlay);
        // GooglePlay HyperLink
        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.green),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Uri uriUrl = Uri.parse(googlePlayURL);
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                startActivity(launchBrowser);
                            }
                        }).into(tv_google);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            startActivity( new Intent(this, LoginActivity.class));
            finish();
        }

        return true;
    }

    // [START onBackPressed]
    @Override
    public void onBackPressed() {
        startActivity( new Intent(this, AccueilActivity.class));
    }


}

