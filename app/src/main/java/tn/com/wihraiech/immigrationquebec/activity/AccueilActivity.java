package tn.com.wihraiech.immigrationquebec.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;

import com.facebook.login.LoginManager;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;
import me.leolin.shortcutbadger.ShortcutBadger;
import tn.com.wihraiech.immigrationquebec.adapter.TabsAccueilAdapter;
import tn.com.wihraiech.immigrationquebec.domain.Canada;
import tn.com.wihraiech.immigrationquebec.domain.Person;
import tn.com.wihraiech.immigrationquebec.domain.ProcImage;
import tn.com.wihraiech.immigrationquebec.domain.Video;
import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.extras.SlidingTabLayout;
import tn.com.wihraiech.immigrationquebec.extras.UtilTCM;
import tn.com.wihraiech.immigrationquebec.presenter.Presenter;
import tn.com.wihraiech.immigrationquebec.R;

import static tn.com.wihraiech.immigrationquebec.globalAds.INTERSTITIAL_EVAL_AD_UNIT_ID;

/**
 * Created by lenovo on 11/12/2016.
 */

public class AccueilActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = AccueilActivity.class.getSimpleName();
    private static final int REQUEST_INVITE = 0;
    final String EXTRA_EMAIL = "email";
    private final String appLinkUrl = Config.APP_FACEBOOK_LINK_URL;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference notif_count = database.getReference("notif_count");
    private Toolbar mToolbar;
    private Drawer.Result navigationDrawerLeft;
    private AccountHeader.Result headerNavigationLeft;
    private int mItemDrawerSelected;
    private FloatingActionMenu fab;
    private Presenter presenter;
    private int badgeCount = 1;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private String email, name;
    private Uri userPhotoUrl;
    private CoordinatorLayout activity_dashboard;
    private GoogleApiClient mGoogleApiClient;
    private List<PrimaryDrawerItem> listcategories;
    private List<Person> listProfile;
    private List<ProcImage> listImages;
    private String previewImageUrl = Config.LOGO_INVITE_URL;
    private String youTubeURL = Config.CHANNEL_TOUTUBE_URL;
    private String facebookURL = Config.FACEBOOK_URL;
    private String googlePlayURL = Config.GOOGLEPLAY_URL;
    private String mylinkedin = Config.LINKEDIN_URL;
    private FirebaseUser user;
    private MaterialDialog mMaterialDialog;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // TRANSITIONS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                /*Explode trans1 = new Explode();
                trans1.setDuration(3000);
                Fade trans2 = new Fade();
                trans2.setDuration(3000);

                getWindow().setExitTransition(trans1);
                getWindow().setReenterTransition( trans2 );*/

            TransitionInflater inflater = TransitionInflater.from(this);
            Transition transition = inflater.inflateTransition(R.transition.transitions);

            getWindow().setSharedElementExitTransition(transition);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        listImages = getSetImageList(23);

        loadInterstitialAd();

        //ShortcutBadger
        ShortcutBadger.applyCount(AccueilActivity.this, badgeCount);

        // TOOLBAR
        mToolbar = findViewById(R.id.tb_main);
        mToolbar.setTitle(getResources().getString(R.string.app_name));
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_launcher);
        setSupportActionBar(mToolbar);

        // TABS
        mViewPager = (ViewPager) findViewById(R.id.vp_tabs_accueil);
        mViewPager.setAdapter(new TabsAccueilAdapter(getSupportFragmentManager(), getApplicationContext()));

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_accueil);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorAccent));
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
//                navigationDrawerLeft.setSelection( position );
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        // FRAGMENT
       /* Fragment frag = getSupportFragmentManager().findFragmentByTag("mainFrag");
        if(frag == null) {
            frag = new AccueilFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.rl_fragment_container, frag, "mainFrag");
            ft.commit();
        }*/

        // TODO: NAVIGATION DRAWER
        // HEADER
        headerNavigationLeft = new AccountHeader()
                .withActivity(this)
                .withCompactStyle(false)
                .withSavedInstance(savedInstanceState)
                .withThreeSmallProfileImages(false)
                .withNameTypeface(Typeface.DEFAULT_BOLD)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile iProfile, boolean current) {
                        Person aux = getPersonByEmail(listProfile, (ProfileDrawerItem) iProfile);
                        if (aux != null)
                            headerNavigationLeft.setBackgroundRes(aux.getBackground());
                        return true;
                    }
                })
                .build();

        listProfile = getSetProfileList();
        if (listProfile != null && listProfile.size() > 0) {
            for (int i = 0; i < listProfile.size(); i++) {
                headerNavigationLeft.addProfile(listProfile.get(i).getProfile(), i);
            }
            headerNavigationLeft.setBackgroundRes(listProfile.get(0).getBackground());
        }

        setFloatActionButton();

    }

    public void loadInterstitialAd() {
        mInterstitialAd.load(getApplicationContext(),
                INTERSTITIAL_EVAL_AD_UNIT_ID,
                new AdRequest.Builder().build(),
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitial) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        new FullScreenContentCallback() {
                            @Override
                            public void onAdDismissedFullScreenContent() {
                                startActivity(new Intent(getApplicationContext(), EvaluateActivity.class));
                            }
                        };
                        mInterstitialAd = interstitial;
                        Log.i("TAG_InterstitialAd", "onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
                        Log.i("TAG_InterstitialAd", loadAdError.getMessage());
                        mInterstitialAd = null;
                    }
                });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {

            return super.dispatchTouchEvent(ev);
        } catch (IllegalArgumentException exception) {
            Log.d(TAG, "dispatch key event exception");
        }
        return false;
    }

    // TODO:[END on_create]

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu_accueil, menu);

        MenuItem menuItem = menu.findItem(R.id.action_notification);
        if (UtilTCM.verifyConnection(getApplicationContext()))
            menuItem.setIcon(buildCounterDrawable(badgeCount, R.mipmap.ic_action_notifications));
        else
            menuItem.setIcon(buildCounterDrawable(badgeCount, R.mipmap.ic_action_notif));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_donate) {
            ShortcutBadger.applyCount(AccueilActivity.this, 0);
            Intent i = new Intent(AccueilActivity.this, DonateActivity.class);
            startActivity( i );
        }*/
        if (id == R.id.action_notification) {
            ShortcutBadger.applyCount(AccueilActivity.this, 0);
            Intent i = new Intent(AccueilActivity.this, NotificationActivity.class);
            startActivity(i);
        }

        if (id == R.id.action_add) {
            Intent i = new Intent(AccueilActivity.this, PostActivity.class);
            startActivity(i);
        }

        if (id == R.id.action_share) {
            onShare();
        }

        /*if (id == R.id.action_help){
            //To Contact an Expert
            startActivity(new Intent(AccueilActivity.this, ContactActivity.class));
        }*/

        if (id == R.id.action_rate) {
            Intent it = new Intent(Intent.ACTION_VIEW);
            it.setData(Uri.parse(googlePlayURL));
            startActivity(it);
        }
        if (id == R.id.action_newpassword) {
            startActivity(new Intent(AccueilActivity.this, NewPasswordActivity.class));
        }

        if (id == R.id.action_quitter) {
            //finish();
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    // CATEGORIES
    private List<PrimaryDrawerItem> getSetCategoryList() {
        String[] names = new String[]{getResources().getString(R.string.home_menu_drawer), getResources().getString(R.string.about_imm_menu_drawer), getResources().getString(R.string.links_menu_drawer), getResources().getString(R.string.imm_pictures_menu_drawer), getResources().getString(R.string.imm_videos_menu_drawer), getResources().getString(R.string.canada_pictures_menu_drawer), getResources().getString(R.string.help_menu_drawer), getResources().getString(R.string.scores_menu_drawer), getResources().getString(R.string.share_menu_drawer), getResources().getString(R.string.about_app_menu_drawer)};
        int[] icons = new int[]{R.drawable.ic_calculator, R.drawable.ic_immigration, R.drawable.ic_link, R.drawable.ic_images, R.drawable.ic_video, R.drawable.ic_picture, R.drawable.ic_help, R.drawable.ic_score, R.drawable.share1, R.mipmap.ic_launcher};
        int[] iconsSelected = new int[]{R.drawable.ic_calculator_selected, R.drawable.ic_immigration_selected, R.drawable.ic_link_selected, R.drawable.ic_images_selected, R.drawable.ic_video_selected, R.drawable.ic_picture_selected, R.drawable.ic_help_selected, R.drawable.ic_score_selected, R.drawable.share_selected, R.mipmap.ic_launcher};
        List<PrimaryDrawerItem> list = new ArrayList<>();

        for (int i = 0; i < names.length; i++) {
            PrimaryDrawerItem aux = new PrimaryDrawerItem();
            aux.setName(names[i]);
            aux.setIcon(getResources().getDrawable(icons[i]));
            aux.setTextColor(getResources().getColor(R.color.colorPrimarytext));
            aux.setSelectedIcon(getResources().getDrawable(iconsSelected[i]));
            aux.setSelectedTextColor(getResources().getColor(R.color.blue_item));

            list.add(aux);
        }

        return (list);
    }

    // PERSON
    private Person getPersonByEmail(List<Person> list, ProfileDrawerItem p) {
        Person aux = null;
        try {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getProfile().getEmail().equalsIgnoreCase(p.getEmail())) {
                    aux = list.get(i);
                    break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return aux;
    }

    private List<Person> getSetProfileList() {
        String[] names = new String[]{name};
        //String[] emails = new String[]{"www.immigration-quebec.gouv.qc.ca"};
        String[] emails = new String[]{email};
        //int[] photos = new int[]{ R.drawable.ic_logo };
        int[] background = new int[]{R.drawable.quebecwp};
        List<Person> list = new ArrayList<>();

        // initialize and create the image LOADER LOGIC
        DrawerImageLoader.init(new DrawerImageLoader.IDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Picasso.with(imageView.getContext())
                        .load(uri)
                        .into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Picasso.with(imageView.getContext()).cancelRequest(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx) {
                return null;
            }
        });

        for (int i = 0; i < names.length; i++) {
            ProfileDrawerItem aux = new ProfileDrawerItem();
            aux.setName(names[i]);
            aux.setEmail(emails[i]);
            //aux.setIcon(getResources().getDrawable(photos[i]));
            try {
                aux.setIcon(user.getPhotoUrl());
                //aux.setIcon("https://firebasestorage.googleapis.com/v0/b/androidauthentification-e450d.appspot.com/o/ic_logo.png?alt=media&token=76837096-2467-4569-9f8d-30eb5b7708bd");
            } catch (Exception e) {
                aux.setIcon("https://firebasestorage.googleapis.com/v0/b/androidauthentification-e450d.appspot.com/o/ic_logo.png?alt=media&token=76837096-2467-4569-9f8d-30eb5b7708bd");
            }
            //aux.setIcon(user.getPhotoUrl());


            Person p = new Person();
            p.setProfile(aux);
            p.setBackground(background[i]);

            list.add(p);
        }
        return (list);
    }

    // CSQ IMAGE
    public List<ProcImage> getSetImageList(int qtd) {

        String[] titres = new String[]{getResources().getString(R.string.imm_cost_picture_title), getResources().getString(R.string.new_rates_picture_title), getResources().getString(R.string.imm_proc_picture_title),
                getResources().getString(R.string.grid2018_picture_title), getResources().getString(R.string.diagram1_picture_title), getResources().getString(R.string.diagram2_picture_title),
                getResources().getString(R.string.process_bef_2016_picture_title), getResources().getString(R.string.process_after_2016_picture_title), getResources().getString(R.string.fr_req_picture_title), getResources().getString(R.string.tcf_picture_title),
                getResources().getString(R.string.en_req_picture_title), getResources().getString(R.string.ielts_picture_title), getResources().getString(R.string.work_certif_picture_title), getResources().getString(R.string.reject_picture_title), getResources().getString(R.string.csq_picture_title),
                getResources().getString(R.string.prior_treat_picture_title), getResources().getString(R.string.federal_picture_title), getResources().getString(R.string.photo_format_title), getResources().getString(R.string.ar_sydney_picture_title), getResources().getString(R.string.medical_exam_title), getResources().getString(R.string.e_brune_title), getResources().getString(R.string.visa_title), ""};

        int[] photos = new int[]{R.drawable.cout_imm, R.drawable.nouveau_tarif, R.drawable.regle2018, R.drawable.grille_select_2018_1, R.drawable.proced_imm,
                R.drawable.proced_mpq, R.drawable.delais_2015, R.drawable.delais_2016, R.drawable.francais, R.drawable.tcf, R.drawable.anglais,
                R.drawable.ielts, R.drawable.experience, R.drawable.miseajour, R.drawable.csq, R.drawable.code_priori, R.drawable.federal, R.drawable.photos,
                R.drawable.sydney, R.drawable.visite_medical, R.drawable.e_brune, R.drawable.visa};

        List<ProcImage> listImg = new ArrayList<>();
        for (int i = 0; i < qtd; i++) {
            //ProcImage ph = new ProcImage( photos[i % photos.length]);
            ProcImage ph = new ProcImage(photos[i % photos.length], titres[i % photos.length]);

            listImg.add(ph);
        }
        return (listImg);
    }

    // FEDERAL IMAGE
    public List<ProcImage> getSetFederalImageList(int qtd) {

        String[] titres = new String[]{getResources().getString(R.string.federal_picture_title), getResources().getString(R.string.photo_format_title), getResources().getString(R.string.ar_sydney_title), getResources().getString(R.string.medical_exam_title), getResources().getString(R.string.e_brune_title), getResources().getString(R.string.visa_title), ""};
        int[] photos = new int[]{R.drawable.federal, R.drawable.photos, R.drawable.sydney, R.drawable.visite_medical,
                R.drawable.e_brune, R.drawable.visa, R.drawable.good_luck};
        List<ProcImage> listImg = new ArrayList<>();

        for (int i = 0; i < qtd; i++) {
            //ProcImage ph = new ProcImage( photos[i % photos.length]);
            ProcImage ph = new ProcImage(photos[i % photos.length], titres[i % photos.length]);

            listImg.add(ph);
        }
        return (listImg);
    }

    // List Image
    public List<ProcImage> getListImages() {
        return (listImages);
    }

    // CANADA PICTURES
    public List<Canada> getSetCanadaList(int qtd) {
        String[] provinces = new String[]{"Québec", "Ontario", "Nouveau-Brunswick", "Québec", "Yukon", "Nouvelle-Écosse", "Alberta", "Québec", "Ontario", "Colombie-Britannique", "Alberta", "Île-du-Prince-Édouard", "Terre Neuve et Labrador", "Manitoba", "Saskatchewan"};
        String[] city = new String[]{"Chateau de Frontenac", "Ottawa", "Rochers Hopewell Baie Fundy", "Québec", "Aurore Boreale", "Bluenose Coast", "Chateau Lake Louise", "Montréal", "Les chutes du Niagara", "Vancouver", "Edmonton", "Golf Île-du-Prince-Édouard", "Saint John's", "Ontario", "Saskatoon"};
        int[] photos = new int[]{R.drawable.frontenacquebec, R.drawable.ottawa, R.drawable.nouveau_brunswick, R.drawable.quebec, R.drawable.yukon, R.drawable.nouvelle_ecosse, R.drawable.lake_louise_alberta, R.drawable.montreal, R.drawable.niagara, R.drawable.vancouver, R.drawable.edmonton, R.drawable.golf, R.drawable.terre_neuve_labrador, R.drawable.manitoba, R.drawable.saskatoon};
        List<Canada> listAux = new ArrayList<>();

        for (int i = 0; i < qtd; i++) {
            Canada c = new Canada(provinces[i % city.length], city[i % city.length], photos[i % city.length]);
            listAux.add(c);
        }
        return (listAux);
    }

    // LIST VIDEOS
    public List<Video> getSetVideoList(int qtd) {
        String[] titles = new String[]{getResources().getString(R.string.app_demo_video_title), getResources().getString(R.string.calcul_score_video_title), getResources().getString(R.string.create_account_mpq_video_title),
                getResources().getString(R.string.fill_forms_video_title), getResources().getString(R.string.pay_fees_video_title),
                getResources().getString(R.string.string_cle_gc_link), getResources().getString(R.string.ecas_video_title)};
        int[] photos = new int[]{R.drawable.app_invite, R.drawable.calcul, R.drawable.mpq, R.drawable.formulaire, R.drawable.paiement, R.drawable.clegc, R.drawable.ecas};
        List<Video> listAux = new ArrayList<>();

        for (int i = 0; i < qtd; i++) {
            Video v = new Video(titles[i % titles.length], photos[i % titles.length]);
            listAux.add(v);
        }
        return (listAux);
    }

    // [START onBackPressed]
    @Override
    public void onBackPressed() {
        //AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        @SuppressLint("RestrictedApi") AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                new ContextThemeWrapper(this, android.R.style.Theme_Dialog));

        alertDialogBuilder.setTitle(getResources().getString(R.string.exit_app_title));
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.exit_app_message))
                //.setIcon(R.mipmap.ic_dialog)
                .setIcon(R.drawable.ic_logo)
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.exit_message),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNeutralButton(getResources().getString(R.string.evaluate_exit_message), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse(googlePlayURL));
                        startActivity(it);
                    }
                })

                .setNegativeButton(getResources().getString(R.string.cancel_message), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        dialog.cancel();

                    }

                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // [START FAB]
    public void setFloatActionButton() {
        fab = (FloatingActionMenu) findViewById(R.id.fab);

        fab.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean b) {
                if (b) {
                    fab.setBackgroundColor(getResources().getColor(R.color.colorBackgroundTint));
                } else
                    fab.setBackgroundColor(getResources().getColor(R.color.colorTransparent));

            }
        });

        com.github.clans.fab.FloatingActionButton fab1 = findViewById(R.id.fab1);
        com.github.clans.fab.FloatingActionButton fab2 = findViewById(R.id.fab2);
        com.github.clans.fab.FloatingActionButton fab3 = findViewById(R.id.fab3);
        com.github.clans.fab.FloatingActionButton fab4 = findViewById(R.id.fab4);
        com.github.clans.fab.FloatingActionButton fab5 =
                findViewById(R.id.fab5);

        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);
        fab3.setOnClickListener(this);
        fab4.setOnClickListener(this);
        fab5.setOnClickListener(this);

        fab.showMenuButton(true);
        fab.setClosedOnTouchOutside(true);
    }

    @Override
    public void onClick(View v) {
        Intent it = null;

        switch (v.getId()) {
            case R.id.fab1:
                //onFacebookInviteClicked();
                it = new Intent(Intent.ACTION_VIEW);
                it.setData(Uri.parse(facebookURL));
                startActivity(it);
                break;
            case R.id.fab2:
                //onEmailInviteClicked();
                if (mInterstitialAd != null)
                    mInterstitialAd.show(getParent());
                else
                    startActivity(new Intent(AccueilActivity.this, EvaluateActivity.class));
                break;
            case R.id.fab3:
                it = new Intent(Intent.ACTION_VIEW);
                it.setData(Uri.parse(googlePlayURL));
                startActivity(it);
                break;
            case R.id.fab4:
                it = new Intent(Intent.ACTION_VIEW);
                it.setData(Uri.parse(youTubeURL));
                startActivity(it);
                break;
            case R.id.fab5:
                it = new Intent(Intent.ACTION_VIEW);
                it.setData(Uri.parse(mylinkedin));
                startActivity(it);
                break;
        }


        //Toast.makeText(MainActivity.this, aux, Toast.LENGTH_SHORT).show();
    }

    // [START on_facebook_invite_clicked]
    public void onFacebookInviteClicked() {
        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .setPromotionDetails("Téléchargez votre application Immigration Québec", "QUEBECPROM")
                    .build();
            AppInviteDialog.show(this, content);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        showMessage(getString(R.string.google_play_services_error));
    }

    /**
     * User has clicked the 'Invite' button, launch the invitation UI with the proper
     * title, message, and deep link
     */
    // [START ON_FIREBASE_INVITE_CLICKED]
    private void onEmailInviteClicked() {
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                .setCallToActionText(getString(R.string.invitation_cta))
                .build();
        startActivityForResult(intent, REQUEST_INVITE);
    }
    // [END on_invite_clicked]

    // [START on_activity_result]
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.d(TAG, "onActivityResult: sent invitation " + id);
                }
            } else {
                // Sending failed or it was canceled, show failure message to the user
                // [START_EXCLUDE]
                showMessage(getString(R.string.send_failed));
                // [END_EXCLUDE]
            }
        }
    }
    // [END on_activity_result]

    private void showMessage(String msg) {
        ViewGroup container = (ViewGroup) findViewById(R.id.snackbar_layout);
        Snackbar.make(container, msg, Snackbar.LENGTH_SHORT).show();
    }

    // [START SHOW_APDATE_APP]
    public void showUpdateAppDialog() {

        mMaterialDialog = new MaterialDialog(this)
                .setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_message)
                .setCanceledOnTouchOutside(false)
                .setPositiveButton(R.string.dialog_positive_label, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String packageName = getPackageName();
                        Intent intent;

                        try {
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
                            startActivity(intent);
                        } catch (android.content.ActivityNotFoundException e) {
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName));
                            startActivity(intent);
                        }
                    }
                });
        mMaterialDialog.setNegativeButton(R.string.dialog_negative_label, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMaterialDialog.dismiss();
            }
        });

        mMaterialDialog.show();
    }


    public void showProgressBar(int visibilidade) {
        findViewById(R.id.pb_loading).setVisibility(visibilidade);
    }

    public void onShare() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            String sAux = "\n" + getResources().getString(R.string.hello_share_message) + " \n\n";
            sAux = sAux + "\n" + getResources().getString(R.string.evaluate_share_dialog) + "\n\n";
            sAux = sAux + "\n● " + getResources().getString(R.string.google_play_link_share_dialog) + "\n\n";
            sAux = sAux + "► https://play.google.com/store/apps/details?id=tn.com.wihraiech.immigrationquebec \n\n";
            sAux = sAux + "● " + getResources().getString(R.string.facebook_link_share_dialog) + "\n\n";
            sAux = sAux + "►https://www.facebook.com/Application.Immigration.Quebec/ \n\n";
            sAux = sAux + getResources().getString(R.string.best_share_dialog) + " \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, getResources().getString(R.string.share_with_share_dialog)));
        } catch (Exception e) {
            Log.e("SHARE_TAG", e.getMessage());
        }
    }

    private Drawable buildCounterDrawable(int count, int backgroundImageId) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.counter_menu, null);
        view.setBackgroundResource(backgroundImageId);

        if (count == 0) {
            View counterTextPanel = view.findViewById(R.id.counterValuePanel);
            counterTextPanel.setVisibility(View.GONE);
        } else {
            TextView textView = (TextView) view.findViewById(R.id.count);
            textView.setText("" + count);
        }

        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }

    //TODO  Login Methods
    private void goLoginScreen() {
        //Intent intent = new Intent(this, FacebookLoginActivity.class);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void logout() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        goLoginScreen();
    }

}
