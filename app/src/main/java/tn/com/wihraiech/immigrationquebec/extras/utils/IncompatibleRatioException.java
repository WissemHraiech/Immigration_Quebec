package tn.com.wihraiech.immigrationquebec.extras.utils;

/**
 * Created by lenovo on 19/01/2017.
 */
public class IncompatibleRatioException extends RuntimeException {

    private static final long serialVersionUID = 234608108593115395L;

    public IncompatibleRatioException() {
        super("Can't perform Ken Burns effect on rects with distinct aspect ratios!");
    }
}
