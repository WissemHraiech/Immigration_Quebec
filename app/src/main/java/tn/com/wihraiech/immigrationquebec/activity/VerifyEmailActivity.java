package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by Wissem Hraiech on 06/08/2017.
 */

public class VerifyEmailActivity extends AppCompatActivity {

    private TextView tv_email, tv_retour;
    private Button bt_confirm;
    private FirebaseUser user;

    final String EXTRA_EMAIL = "email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);

        tv_email = (TextView) findViewById(R.id.tv_emailVerif);
        tv_retour = (TextView) findViewById(R.id.tv_retour);
        bt_confirm = (Button) findViewById(R.id.bt_confirm);

        Intent intent = getIntent();
        if (intent != null) {
            tv_email.setText(intent.getStringExtra(EXTRA_EMAIL));
        }

        user = FirebaseAuth.getInstance().getCurrentUser();

        bt_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendEmailVerification();
                startActivity( new Intent(VerifyEmailActivity.this, AccueilActivity.class));

            }
        });

        tv_retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity( new Intent(VerifyEmailActivity.this, SignUpActivity.class));

            }
        });

        //tv_retour.setText(getString(R.string.email_statut, user.getEmail(), user.isEmailVerified()));

    }

    private void sendEmailVerification(){

        if (user != null) {
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(VerifyEmailActivity.this, "Accèderz à votre e-mail pour confirmation!", Toast.LENGTH_LONG).show();
                        FirebaseAuth.getInstance().signOut();
                    }
                }
            });
        }
    }

}
