package tn.com.wihraiech.immigrationquebec.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.*;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mikepenz.materialdrawer.Drawer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import tn.com.wihraiech.immigrationquebec.adapter.DBHandlerImmQueb;
import tn.com.wihraiech.immigrationquebec.domain.Score;
import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.R;

import static tn.com.wihraiech.immigrationquebec.GlobalFunctions.loadAdBanner;

/**
 * Created by lenovo on 19/04/2016.
 */
public class SecondActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        com.rey.material.widget.Spinner.OnItemSelectedListener{

    private Toolbar mToolbar;
    private Drawer.Result navigationDrawerLeft;
    private TextView tv_monScrore, tv_felicit, tv_msgEmploy;
    private com.rey.material.widget.Spinner spinner_Scolarité, spinner_univ, spinner_colleg, spinner_seconPro, spinner_Age,
            spinner_fco, spinner_fpo;
    private EditText et_scorScol, et_form, et_age, et_fco, et_fpo;
    private RadioGroup rg_form;
    private Button bt_next, bt_save;
    private String monScrore, et_scorScol1, et_form1, et_exp, et_age1, et_fco1, et_fpo1, et_fce,
            et_fpe, et_aco, et_apo, et_ace, et_ape, et_sejour, et_famille, et_emploi, scoreEnfants, et_CapFin;
    public int scoreEmp, scorEmpTot;

    private static final int PERMISSION_REQUEST_CODE = 1;
    //PDF File Parameters
    private PdfPCell cell;
    private Image bgImage;
    private String path, msgPDF;
    private File dir;
    private File file;
    //use to set background color
    private BaseColor myColor = WebColors.getRGBColor("#3140e6");
    private BaseColor myColor2 = WebColors.getRGBColor("#909090");
    private BaseColor myColor1 = WebColors.getRGBColor("#FBD2B2");

    private final String googlePlayURL = Config.GOOGLEPLAY_URL;
    final String EXTRA_SCORE = "score", EXTRA_SCOREMP = "scoremp", EXTRA_EMAIL = "email";

    private com.google.android.gms.ads.AdView mAdView;
    private com.facebook.ads.AdView adView;

    final DBHandlerImmQueb db = new DBHandlerImmQueb(this);
    private Config gestion = new Config();
    MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_second);
        //mToolbar.setTitle("Immigration Québec");
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_launcher);
        setSupportActionBar(mToolbar);

        ////////////////////ActionBar///////////////
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_second);
        TextView textView = (TextView) findViewById(R.id.idtoolbar);
        textView.setText(getResources().getString(R.string.title_activity_second));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        //ADMOB
        mAdView = (com.google.android.gms.ads.AdView) findViewById(R.id.adView_Second);
        loadAdBanner(mAdView, getApplicationContext());

        //creating new file path
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/";
        dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        mainActivity = new MainActivity();

        tv_monScrore = (TextView) findViewById(R.id.tv_ScoreCouple);
        tv_felicit = (TextView) findViewById(R.id.tv_felicit2);
        tv_msgEmploy = (TextView) findViewById(R.id.tv_msgEmploy2);
        bt_next = (Button) findViewById(R.id.bt_links);
        bt_save = (Button) findViewById(R.id.bt_save);
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // CREATE PDF
                if (Build.VERSION.SDK_INT >= 23)
                {
                    if (checkPermission())
                    {
                        try {
                            createPDF();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                        // Code for above or equal 23 API Oriented Device
                        // Your Permission granted already .Do next code
                    } else {
                        requestPermission(); // Code for permission

                    }
                }
                else
                {
                    // Code for Below 23 API Oriented Device
                    // Do next code
                    try {
                        createPDF();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }
                }

                // Contact an Expert
                //startActivity(new Intent(SecondActivity.this, ContactActivity.class));

            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            et_scorScol1 = intent.getStringExtra("et_scorScol");
            et_form1 = intent.getStringExtra("et_form");
            et_exp = intent.getStringExtra("et_exp");
            et_age1 = intent.getStringExtra("et_age");
            et_fco1 = intent.getStringExtra("et_fco");
            et_fpo1 = intent.getStringExtra("et_fpo");
            et_fce = intent.getStringExtra("et_fce");
            et_fpe = intent.getStringExtra("et_fpe");
            et_aco = intent.getStringExtra("et_aco");
            et_apo = intent.getStringExtra("et_apo");
            et_ace = intent.getStringExtra("et_ace");
            et_ape = intent.getStringExtra("et_ape");
            et_sejour = intent.getStringExtra("et_sejour");
            et_famille = intent.getStringExtra("et_famille");
            et_emploi = intent.getStringExtra("et_emploi");
            scoreEnfants = intent.getStringExtra("scoreEnfants");
            et_CapFin = intent.getStringExtra("et_CapFin");
            tv_monScrore.setText(intent.getStringExtra(EXTRA_SCORE));
            monScrore = intent.getStringExtra(EXTRA_SCORE);
            scoreEmp = intent.getIntExtra(EXTRA_SCOREMP, 0);
        }

        /*  Spinners  */
        spinner_Scolarité = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_scolarite2);
        spinner_univ = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_univ2);
        spinner_colleg = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_colleg2);
        spinner_seconPro = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_seconPro2);
        spinner_Age = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_age2);
        spinner_fco = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_cpOral2);
        spinner_fpo = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_prOrale2);

         /*  EditText  */
        et_scorScol = (EditText) findViewById(R.id.et_scoreNivScol2);
        et_form = (EditText) findViewById(R.id.et_scoreDomForm2);
        et_age = (EditText) findViewById(R.id.et_scoreAge2);
        et_fco = (EditText) findViewById(R.id.et_scoreCpOral2);
        et_fpo = (EditText) findViewById(R.id.et_scorePrOrale2);


        addItemsOnSpinner_Scolarité();
        addItemsOnSpinner_Université();
        addItemsOnSpinner_Colleg();
        addItemsOnSpinner_SecondPro();
        addItemsOnSpinner_Age();
        addItemsOnSpinner_langues();

        gestionNiveauUniversité();
        gestionNiveauCollegiale();
        gestionSecondPro();

        //Drawer finish()
        navigationDrawerLeft = new Drawer()
                .withActivity(this)
                .withActionBarDrawerToggle(false)
                .withCloseOnClick(true)
                .withActionBarDrawerToggleAnimated(true)
                .withActionBarDrawerToggle(new ActionBarDrawerToggle(this, new DrawerLayout(this), R.string.drawer_open, R.string.drawer_close) {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        super.onDrawerSlide(drawerView, slideOffset);
                        navigationDrawerLeft.closeDrawer();
                        finish();
                    }
                })

                .build();

        // TODO: Instantiate an AdView view
        // Find the Ad Container
        /* RelativeLayout adViewContainer = (RelativeLayout) findViewById(R.id.rl_ads);
       adView = new com.facebook.ads.AdView(this, getResources().getString(R.string.facebook_banner_second), AdSize.BANNER_320_50);
        adViewContainer.addView(adView);
        adView.loadAd();

        // TODO: [START FACEBOOK AdListener for AdView]
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                //Toast.makeText(SecondActivity.this, "Error: " + adError.getErrorMessage(),Toast.LENGTH_LONG).show();
                Log.i("adError:", adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });

        // Request an ad
        adView.loadAd();*/

    }

    // Add to each long-lived activity
    @Override
    public void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    // for Android, you should also log app deactivation
    @Override
    public void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    // TODO: To LinksActivity
    public void toLinks(View view){
        /*Intent intent = new Intent(this, LinkActivity.class);
        startActivity(intent);*/
        showChangeLangDialog();
    }

    // TODO: Score TOTAL
    public int calculScore(){
        int score = Integer.decode(et_scorScol.getText().toString()) + Integer.decode(et_form.getText().toString())
                + Integer.decode(et_fco.getText().toString()) + Integer.decode(et_fpo.getText().toString())
                + Integer.decode(et_age.getText().toString()) + Integer.decode(monScrore);

        //Animation text view monscore
        YoYo.with(Techniques.ZoomInUp)
                .duration(700)
                .playOn(findViewById(R.id.tv_ScoreCouple));

        scorEmpTot = scoreEmp + scoreEmp();
        Log.i("log", scorEmpTot + "");
        if(score > 58){
            if(scorEmpTot > 51){
                tv_monScrore.setTextColor(getResources().getColor(R.color.green));
                tv_felicit.setVisibility(View.VISIBLE);
                bt_next.setVisibility(View.VISIBLE);
                tv_msgEmploy.setVisibility(View.GONE);
                msgPDF = tv_felicit.getText().toString();
            }
            else{
                tv_monScrore.setTextColor(getResources().getColor(R.color.red));
                tv_felicit.setVisibility(View.GONE);
                bt_next.setVisibility(View.GONE);
                tv_msgEmploy.setVisibility(View.VISIBLE);
                msgPDF = getResources().getString(R.string.employ_thres_message);

                //Animation
                YoYo.with(Techniques.Tada)
                        .duration(700)
                        .playOn(findViewById(R.id.tv_msgEmploy2));
            }
        }
        else{
            tv_monScrore.setTextColor(getResources().getColor(R.color.red));
            tv_felicit.setVisibility(View.GONE);
            bt_next.setVisibility(View.GONE);
            tv_msgEmploy.setVisibility(View.GONE);
            msgPDF = getResources().getString(R.string.selection_crit_message);
        }

        return score;
    }

    public int scoreEmp() {
        int score = Integer.decode(et_scorScol.getText().toString()) + Integer.decode(et_form.getText().toString())
                + Integer.decode(et_fco.getText().toString()) + Integer.decode(et_fpo.getText().toString())
                + Integer.decode(et_age.getText().toString());

        return score;
    }

    //Link To ImmigrationActivity
    public void toImmigration(View view){
        Intent intent = new Intent(this, ImmigrationActivity.class);
        startActivity(intent);
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        gestionScolarité();
        gestionFormation();
        gestionAge();
        gestionLangues();
        /*gestionNbr12();
        gestionNbr1318();*/

    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {
        gestionScolarité();
        gestionFormation();
        gestionAge();
        gestionLangues();
    }

    // add items into spinner Scolarité
    public void addItemsOnSpinner_Scolarité(){

        // Spinner click listener
        spinner_Scolarité.setOnItemSelectedListener(this);

        List<String> categories = gestion.scolarité( getApplicationContext() );

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter <String>(this, R.layout.spinner_item_nbr2, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_Scolarité.setAdapter(dataAdapter);
    }

    public void gestionScolarité(){
        switch (spinner_Scolarité.getSelectedItemPosition()) {
            case 0:
                et_scorScol.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 1:
                et_scorScol.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 2:
                et_scorScol.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 3:
                et_scorScol.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 4:
                et_scorScol.setText("3");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 5:
                et_scorScol.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 6:
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 7:
                et_scorScol.setText("3");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 8:
                et_scorScol.setText("4");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 9:
                et_scorScol.setText("4");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_scorScol.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }
    }

    public void gestionFormation() {

        /* Initialize Radio Group and attach click handler */
        rg_form = (RadioGroup) findViewById(R.id.rg_formation2);
        //rg_form.clearCheck();
        /* Attach CheckedChangeListener to radio group */
        /*rg_form.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {*/

                /* Attach CheckedChangeListener to radio group */
        rg_form.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {

                    if (rb.getText().equals(getResources().getString(R.string.string_university_level))) {
                        spinner_univ.setVisibility(View.VISIBLE);
                        spinner_colleg.setVisibility(View.INVISIBLE);
                        spinner_seconPro.setVisibility(View.INVISIBLE);
                        spinner_univ.setSelection(0);
                        spinner_colleg.setSelection(0);
                        spinner_seconPro.setSelection(0);
                        Toast.makeText(SecondActivity.this, getResources().getString(R.string.zero_point_domain_message), Toast.LENGTH_LONG).show();

                    } else if (rb.getText().equals(getResources().getString(R.string.string_technical_college_level))) {
                        spinner_colleg.setVisibility(View.VISIBLE);
                        spinner_univ.setVisibility(View.INVISIBLE);
                        spinner_seconPro.setVisibility(View.INVISIBLE);
                        spinner_univ.setSelection(0);
                        spinner_colleg.setSelection(0);
                        spinner_seconPro.setSelection(0);
                        Toast.makeText(SecondActivity.this, getResources().getString(R.string.zero_point_domain_message), Toast.LENGTH_LONG).show();


                    } else if (rb.getText().equals(getResources().getString(R.string.string_secondary_professional_level))) {
                        spinner_seconPro.setVisibility(View.VISIBLE);
                        spinner_univ.setVisibility(View.INVISIBLE);
                        spinner_colleg.setVisibility(View.INVISIBLE);
                        spinner_univ.setSelection(0);
                        spinner_colleg.setSelection(0);
                        spinner_seconPro.setSelection(0);
                        Toast.makeText(SecondActivity.this, getResources().getString(R.string.zero_point_domain_message), Toast.LENGTH_LONG).show();

                    }
                }
            }
        });

        //}
        //});
    }

    public void addItemsOnSpinner_Université(){

        // Spinner click listener
        spinner_univ.setOnItemSelectedListener(this);
        List<String> univ = gestion.univ();

        // Creating adapter for spinner
        ArrayAdapter<String> univAdapter = new ArrayAdapter <String>(this, R.layout.spinner_item_nbr2, univ);

        // Drop down layout style - list view with radio button
        univAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_univ.setAdapter(univAdapter);
    }

    public void gestionNiveauUniversité() {

        spinner_univ.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
           @Override
           public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {

               switch (String.valueOf(spinner_univ.getSelectedItem())) {
                   case "Actuariat (BAC)":
                   case "Information de gestion (BAC)":
                       et_form.setText("4");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;

                   case "Administration des affaires (BAC)":
                   case "Affaires sur le plan international (BAC)":
                   case "Agriculture (BAC)":
                   case "Architecture (MAI)":
                   case "Architecture urbaine et aménagement (MAI) ":
                   case "Autres professions de la santé (DOC) ":
                   case "Bibliothéconomie et archivistique (MAI)":
                   case "Comptabilité et sciences comptables (BAC) ":
                   case "Coopération (MAI)":
                   case "Didactique (art d'enseigner) (MAI) ":
                   case "Économie rurale et agricole (BAC)":
                   case "Formation des enseignants au collégial (MAI)":
                   case "Formation des enseignants au préscolaire (BAC)":
                   case "Formation des enseignants au préscolaire et au primaire (BAC)":
                   case "Formation des enseignants de l'enseignement professionnel au secondaire et au collégial (BAC)":
                   case "Formation des enseignants spécialistes au primaire et au secondaire (BAC)":
                   case "Formation des enseignants spécialistes en adaptation scolaire (orthopédagogie) (BAC)":
                   case "Génie aérospatial, aéronautique et astronautique (BAC)":
                   case "Génie agricole et génie rural (BAC)":
                   case "Génie biologique et biomédical (BAC)":
                   case "Génie civil, de la construction et du transport (BAC)":
                   case "Génie électrique, électronique et des communications (BAC)":
                   case "Génie géologique (BAC)":
                   case "Génie industriel et administratif (BAC)":
                   case "Génie informatique et de la construction des ordinateurs (BAC)":
                   case "Génie mécanique (BAC)":
                   case "Génie métallurgique et des matériaux (BAC)":
                   case "Génie minier (BAC)":
                   case "Génie nucléaire (MAI)":
                   case "Géologie (minéralogie, etc.) (BAC)":
                   case "Ingénierie (BAC)":
                   case "Mathématiques (BAC)":
                   case "Mathématiques appliquées (BAC)":
                   case "Opérations bancaires et finance (BAC)":
                   case "Orthophonie et audiologie (BAC)":
                   case "Orthophonie et audiologie (MAI)":
                   case "Physiothérapie (MAI)":
                   case "Psychologie (DOC)":
                   case "Recherche opérationnelle (MAI)":
                   case "Relations industrielles (BAC)":
                   case "Sciences de l'informatique (BAC) ":
                   case "Sciences domestiques (BAC)":
                   case "Service social (BAC)":
                       et_form.setText("3");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;

                   case "Anglais, en général et langue maternelle (BAC)":
                   case "Administration scolaire (MAI)":
                   case "Architecture (BAC)":
                   case "Architecture paysagiste (BAC)":
                   case "Arts graphiques (communications graphiques) (BAC)":
                   case "Arts plastiques (peinture, dessin, sculpture) (BAC)":
                   case "Communications et journalisme (BAC)":
                   case "Criminologie (BAC)":
                   case "Démographie (MAI)":
                   case "Diététique et nutrition (BAC)":
                   case "Énergie (MAI)":
                   case "Environnement (qualité du milieu et pollution) (BAC)":
                   case "Ergothérapie (BAC)":
                   case "Ergothérapie (MAI)":
                   case "Ethnologie et ethnographie (MAI)":
                   case "Études urbaines (BAC)":
                   case "Formation des enseignants au secondaire (BAC)":
                   case "Français, en général et langue maternelle (BAC)":
                   case "Génétique (MAI)":
                   case "Génie chimique (BAC)":
                   case "Génie des pâtes et papiers (MAI)":
                   case "Génie physique (BAC)":
                   case "Géodésie (arpentage) (BAC)":
                   case "Géographie (BAC)":
                   case "Gestion des services de santé (MAI)":
                   case "Gestion du personnel (BAC)":
                   case "Gestion et administration des entreprises (BAC)":
                   case "Hydrologie et sciences de l'eau (MAI)":
                   case "Marketing et achats (BAC)":
                   case "Médecine vétérinaire (DOC)":
                   case "Musique (BAC)":
                   case "Optométrie (DOC)":
                   case "Pédagogie universitaire (MAI)":
                   case "Pédologie, aménagement et conservation des sols (MAI)":
                   case "Physiothérapie (BAC)":
                   case "Probabilités et statistiques (BAC)":
                   case "Psychoéducation (BAC)":
                   case "Psychoéducation (MAI)":
                   case "Psychologie (MAI)":
                   case "Récréologie (BAC)":
                   case "Ressources naturelles (BAC)":
                   case "Santé communautaire et épidémiologie (MAI)":
                   case "Sciences de la terre (BAC)":
                   case "Sciences et technologie des aliments (BAC)":
                   case "Sciences infirmières et nursing (BAC)":
                   case "Sexologie (BAC)":
                   case "Traduction (BAC)":
                   case "Urbanisme (BAC)":
                   case "Zootechnie (MAI)":
                       et_form.setText("2");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;

                   default:
                       et_form.setText("0");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;
               }
           }
       });

    }

    public void addItemsOnSpinner_Colleg(){

        // Spinner click listener
        spinner_colleg.setOnItemSelectedListener(this);


        List<String> colleg = gestion.colleg();

        // Creating adapter for spinner
        ArrayAdapter<String> collegAdapter = new ArrayAdapter <String>(this, R.layout.spinner_item_nbr2, colleg);

        // Drop down layout style - list view with radio button
        collegAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_colleg.setAdapter(collegAdapter);
    }

    public void gestionNiveauCollegiale() {

        spinner_colleg.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
             @Override
             public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {

                 switch (String.valueOf(spinner_colleg.getSelectedItem())) {
                     case "Gestion d'un établissement de restauration (DEC)":
                     case "Techniques d'éducation à l'enfance (AEC)":
                     case "Techniques d'éducation à l'enfance (DEC)":
                     case "Techniques d'orthèses visuelles (DEC)":
                         et_form.setText("4");
                         tv_monScrore.setText(String.valueOf(calculScore()));
                         break;

                     case "Assainissement de l'eau (DEC)":
                     case "Conseil en assurances et en services financiers (AEC) ":
                     case "Conseil en assurances et en services financiers (DEC) ":
                     case "Environnement, hygiène et sécurité au travail (DEC)":
                     case "Gestion d'un établissement de restauration (AEC)":
                     case "Techniques d'avionique (AEC)":
                     case "Techniques d'avionique (DEC)":
                     case "Techniques de génie aérospatial (DEC)":
                     case "Techniques de génie mécanique (DEC)":
                     case "Techniques de la plasturgie (DEC)":
                     case "Techniques de l'informatique (AEC)":
                     case "Techniques de l'informatique (DEC)":
                     case "Techniques de transformation des matériaux composites (AEC)":
                     case "Techniques de transformation des matériaux composites (DEC)":
                     case "Techniques d'éducation spécialisée (AEC)":
                     case "Techniques d'éducation spécialisée (DEC)":
                     case "Techniques d'hygiène dentaire (DEC)":
                     case "Techniques d'orthèses et de prothèses orthopédiques (DEC)":
                     case "Techniques du meuble et d'ébénisterie (AEC)":
                     case "Techniques du meuble et d'ébénisterie (DEC)":
                     case "Technologie de la géomatique (DEC)":
                     case "Technologie de la mécanique du bâtiment (DEC)":
                     case "Technologie de la production pharmaceutique (AEC) ":
                     case "Technologie de la production pharmaceutique (DEC) ":
                     case "Technologie de radiodiagnostic (DEC)":
                     case "Technologie du génie métallurgique (DEC)":
                     case "Technologie minérale (DEC)":
                         et_form.setText("3");
                         tv_monScrore.setText(String.valueOf(calculScore()));
                         break;

                     case "Acupuncture (DEC)":
                     case "Assainissement de l'eau (AEC)":
                     case "Audioprothèse (DEC)":
                     case "Environnement, hygiène et sécurité au travail (AEC)":
                     case "Gestion de projet en communications graphiques (DEC)":
                     case "Gestion et technologies d'entreprise agricole (AEC)":
                     case "Gestion et technologies d'entreprise agricole (DEC)":
                     case "Paysage et commercialisation en horticulture ornementale (DEC)":
                     case "Soins infirmiers (DEC)":
                     case "Soins préhospitaliers d'urgence (DEC)":
                     case "Techniques de bureautique (AEC)":
                     case "Techniques de bureautique (DEC)":
                     case "Techniques de diététique (DEC)":
                     case "Techniques de génie aérospatial (AEC)":
                     case "Techniques de génie mécanique (AEC)":
                     case "Techniques de gestion hôtelière (AEC)":
                     case "Techniques de gestion hôtelière (DEC)":
                     case "Techniques de laboratoire (DEC)":
                     case "Techniques de maintenance d'aéronefs (AEC)":
                     case "Techniques de maintenance d'aéronefs (DEC)":
                     case "Techniques de physiothérapie (DEC)":
                     case "Techniques de production et de postproduction télévisuelles (DEC)":
                     case "Techniques de recherche sociale (DEC)":
                     case "Techniques de santé animale (DEC)":
                     case "Techniques de sécurité incendie (AEC)":
                     case "Techniques de sécurité incendie (DEC)":
                     case "Techniques de thanatologie (AEC)":
                     case "Techniques de thanatologie (DEC)":
                     case "Techniques d'électrophysiologie médicale (DEC)":
                     case "Techniques d'inhalothérapie (DEC)":
                     case "Techniques d'intégration multimédia (AEC)":
                     case "Techniques d'intégration multimédia (DEC)":
                     case "Techniques d'intervention en délinquance (AEC)":
                     case "Techniques d'intervention en délinquance (DEC) ":
                     case "Techniques d'intervention en loisir (DEC)":
                     case "Techniques du tourisme d'aventure (AEC)":
                     case "Techniques du tourisme d'aventure (DEC)":
                     case "Techniques juridiques (AEC)":
                     case "Techniques juridiques (DEC)":
                     case "Technologie d'analyses biomédicales (AEC)":
                     case "Technologie de la géomatique (AEC)":
                     case "Technologie de la production horticole et de l'environnement (DEC)":
                     case "Technologie de la transformation des produits aquatiques (DEC)":
                     case "Technologie de l'architecture (AEC)":
                     case "Technologie de l'architecture (DEC)":
                     case "Technologie de l'architecture navale (DEC)":
                     case "Technologie de l'électronique (AEC)":
                     case "Technologie de l'électronique (DEC)":
                     case "Technologie de l'électronique industrielle (AEC)":
                     case "Technologie de l'électronique industrielle (DEC)":
                     case "Technologie de l'estimation et de l'évaluation en bâtiment (AEC)":
                     case "Technologie de maintenance industrielle (DEC)":
                     case "Technologie de médecine nucléaire (DEC)":
                     case "Technologie de radio-oncologie (DEC)":
                     case "Technologie de systèmes ordinés (DEC)":
                     case "Technologie des procédés et de la qualité des aliments (AEC)":
                     case "Technologie des procédés et de la qualité des aliments (DEC)":
                     case "Technologie des productions animales (DEC)":
                     case "Technologie du génie civil (DEC)":
                     case "Technologie du génie industriel (AEC)":
                     case "Technologie du génie industriel (DEC)":
                     case "Technologie du génie métallurgique (AEC)":
                     case "Technologie minérale (AEC)":
                         et_form.setText("2");
                         tv_monScrore.setText(String.valueOf(calculScore()));
                         break;
                     default:
                         et_form.setText("0");
                         tv_monScrore.setText(String.valueOf(calculScore()));
                         break;
                 }
             }
         });

    }

    public void addItemsOnSpinner_SecondPro(){

        // Spinner click listener
        spinner_seconPro.setOnItemSelectedListener(this);


        List<String> colleg = gestion.secondaire();

        // Creating adapter for spinner
        ArrayAdapter<String> secondAdapter = new ArrayAdapter <String>(this, R.layout.spinner_item_nbr2, colleg);

        // Drop down layout style - list view with radio button
        secondAdapter.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinner_seconPro.setAdapter(secondAdapter);
    }

    public void gestionSecondPro() {

        spinner_seconPro.setOnItemSelectedListener(new com.rey.material.widget.Spinner.OnItemSelectedListener() {
           @Override
           public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {
               switch (String.valueOf(spinner_seconPro.getSelectedItem())) {
                   case "Mécanique agricole (DEP)":
                       et_form.setText("4");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;
                   case "Boucherie de détail (DEP)":
                   case "Carrosserie (DEP)":
                   case "Charpenterie-menuiserie (DEP)":
                   case "Chaudronnerie (DEP)":
                   case "Conduite de procédés de traitement de l'eau (DEP)":
                   case "Cuisine (DEP)":
                   case "Électromécanique de systèmes automatisés (DEP)":
                   case "Fabrication de moules (ASP)":
                   case "Ferblanterie-tôlerie (DEP)":
                   case "Forage et dynamitage (DEP)":
                   case "Grandes cultures (DEP)":
                   case "Installation et entretien de systèmes de sécurité (DEP)":
                   case "Installation et réparation d'équipement de télécommunication (DEP)":
                   case "Matriçage (ASP)":
                   case "Mécanique d'ascenseur (DEP)":
                   case "Mécanique de machines fixes (DEP)":
                   case "Mécanique de protection contre les incendies (DEP)":
                   case "Mécanique de véhicules lourds routiers (DEP)":
                   case "Mécanique d'engins de chantier (DEP)":
                   case "Mécanique industrielle de construction et d'entretien (DEP)":
                   case "Outillage (ASP)":
                   case "Plomberie et chauffage (DEP)":
                   case "Production acéricole (DEP)":
                   case "Production animale (DEP)":
                   case "Réfrigération (DEP)":
                   case "Régulation de vol (DEP)":
                   case "Soutien informatique (DEP)":
                   case "Techniques d'usinage (DEP) ":
                       et_form.setText("3");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;

                   case "Arboriculture-élagage (DEP)":
                   case "Assistance à la personne à domicile (DEP)":
                   case "Assistance dentaire (DEP)":
                   case "Assistance technique en pharmacie (DEP)":
                   case "Briquetage-maçonnerie (DEP)":
                   case "Calorifugeage (DEP)":
                   case "Coiffure (DEP)":
                   case "Conduite de machines de traitement du minerai (DEP)":
                   case "Conduite d'engins de chantier (DEP)":
                   case "Conduite et réglage de machines à mouler (DEP)":
                   case "Conseil et vente de pièces d'équipement motorisé (DEP)":
                   case "Conseil et vente de voyages (DEP)":
                   case "Conseil technique en entretien et en réparation de véhicules (DEP)":
                   case "Dessin de bâtiment (DEP)":
                   case "Dessin industriel (DEP)":
                   case "Ébénisterie (DEP)":
                   case "Électricité (DEP)":
                   case "Esthétique (DEP)":
                   case "Fonderie (DEP)":
                   case "Installation de revêtements souples (DEP)":
                   case "Installation et fabrication de produits verriers (DEP)":
                   case "Mécanique automobile (DEP)":
                   case "Mécanique de véhicules légers (DEP)":
                   case "Modelage (DEP)":
                   case "Montage de câbles et de circuits (DEP)":
                   case "Montage de lignes électriques (DEP)":
                   case "Opération d'équipements de production (DEP)":
                   case "Pâtes et papiers - Opérations (DEP)":
                   case "Pâtisserie (DEP)":
                   case "Peinture en bâtiment (DEP)":
                   case "Préparation et finition de béton (DEP)":
                   case "Production horticole (DEP)":
                   case "Rembourrage artisanal (DEP)":
                   case "Santé, assistance et soins infirmiers (DEP)":
                   case "Sciage (DEP)":
                   case "Secrétariat (DEP)":
                   case "Service de la restauration (DEP)":
                   case "Soudage-montage (DEP)":
                   case "Tôlerie de précision (DEP)":
                   case "Vente-conseil (DEP)":
                       et_form.setText("2");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;
                   default:
                       et_form.setText("0");
                       tv_monScrore.setText(String.valueOf(calculScore()));
                       break;
               }
           }
       });


    }

    // add Items On Spinner Age
    public void addItemsOnSpinner_Age(){
        // Spinner click listener
        spinner_Age.setOnItemSelectedListener(this);

        List<String> age = gestion.age( getApplicationContext() );


        // Creating adapter for spinner
        ArrayAdapter<String> ageAdapter = new ArrayAdapter <String>(this, R.layout.spinner_item_nbr2, age);

        // Drop down layout style - list view with radio button
        ageAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_Age.setAdapter(ageAdapter);
    }

    public void gestionAge( ){
        switch (spinner_Age.getSelectedItemPosition()) {
            case 1:
                et_age.setText("3");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 2:
                et_age.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 3:
                et_age.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 4:
                et_age.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 5:
                et_age.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 6:
                et_age.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 7:
                et_age.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 8:
                et_age.setText("1");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case 9:
                et_age.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_age.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

    }

    public void addItemsOnSpinner_langues() {

        //TODO/***  Spinner  Français   ***/

        //TODO/*  Spinner Compréhension Orale */
        // Spinner click listener
        spinner_fco.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> francais = new ArrayList<String>();
        francais.add(" ");
        francais.add("B1");
        francais.add("B2 16/25 +");
        francais.add("C1 16/25 +");
        francais.add("C2 32/50 +");

        // Creating adapter for spinner
        ArrayAdapter<String> fcoAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, francais);

        // Drop down layout style - list view with radio button
        fcoAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_fco.setAdapter(fcoAdapter);

        //TODO/*  Spinner Production Orale   */

        // Spinner click listener
        spinner_fpo.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        ArrayAdapter<String> fpoAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_nbr2, francais);

        // Drop down layout style - list view with radio button
        fpoAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        spinner_fpo.setAdapter(fpoAdapter);

    }

    public void gestionLangues() {

        switch (String.valueOf(spinner_fco.getSelectedItem())) {
            case "B1":
                et_fco.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "B2 16/25 +":
                et_fco.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C1 16/25 +":
                et_fco.setText("3");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C2 32/50 +":
                et_fco.setText("3");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_fco.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }

        switch (String.valueOf(spinner_fpo.getSelectedItem())) {
            case "B1":
                et_fpo.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "B2 16/25 +":
                et_fpo.setText("2");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C1 16/25 +":
                et_fpo.setText("3");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            case "C2 32/50 +":
                et_fpo.setText("3");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
            default:
                et_fpo.setText("0");
                tv_monScrore.setText(String.valueOf(calculScore()));
                break;
        }
    }

    // ALERT DIALOG SAVE SCORE
    public void showChangeLangDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_score, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.et_usernameScore);
        final TextView txt = (TextView) dialogView.findViewById(R.id.tv_dialogscore);
        txt.setText(tv_monScrore.getText().toString());

        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle(getResources().getString(R.string.save_score_string));

        dialogBuilder.setPositiveButton(R.string.save_score, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Add New Score
                Log.d("Insert: ", "Inserting..");
                db.addScore( new Score(edt.getText().toString(), tv_monScrore.getText().toString()) );
                // CREATE PDF
                if (Build.VERSION.SDK_INT >= 23)
                {
                    if (checkPermission())
                    {
                        try {
                            createPDF();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                        // Code for above or equal 23 API Oriented Device
                        // Your Permission granted already .Do next code
                    } else {
                        requestPermission(); // Code for
                        try {
                            createPDF();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else
                {
                    // Code for Below 23 API Oriented Device
                    // Do next code
                    try {
                        createPDF();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }

                }
                Toast.makeText(SecondActivity.this, getResources().getString(R.string.saved_score_string),Toast.LENGTH_SHORT).show();
                // To LinksActivity
                Intent intent = new Intent(SecondActivity.this, LinkActivity.class);
                startActivity(intent);
            }
        });
        dialogBuilder.setNegativeButton(R.string.cancel_score, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // To LinksActivity
                Intent intent = new Intent(SecondActivity.this, LinkActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_rate) {
            Intent it = new Intent(Intent.ACTION_VIEW);
            it.setData(Uri.parse(googlePlayURL));
            startActivity(it);
        }

        if (id == R.id.action_pdf) {
            // CREATE PDF
            try {
                createPDF();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return true;
    }

    // CREATE PDF
    public void createPDF() throws FileNotFoundException, DocumentException {

        //create document file
        Document doc = new Document();
        try {

            Log.e("PDFCreator", "PDF Path: " + path);
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            file = new File(dir, getResources().getString(R.string.rating_sheet_pdf_name) + ".pdf");
            //file = new File(dir, "Scores PDF" + sdf.format(Calendar.getInstance().getTime()) + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfWriter writer = PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();
            //create table
            PdfPTable pt = new PdfPTable(2);
            pt.setWidthPercentage(100);
            float[] fl = new float[]{20, 80};
            pt.setWidths(fl);
            cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);

            //set drawable in cell
            Drawable myImage = SecondActivity.this.getResources().getDrawable(R.drawable.ic_logo);
            Bitmap bitmap = ((BitmapDrawable) myImage).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bitmapdata = stream.toByteArray();
            try {
                bgImage = Image.getInstance(bitmapdata);
                bgImage.setAbsolutePosition(330f, 642f);
                cell.addElement(bgImage);
                pt.addCell(cell);
                cell = new PdfPCell();
                cell.setBorder(Rectangle.NO_BORDER);
                Paragraph p1 = new Paragraph(
                        String.format(getResources().getString(R.string.app_name)),
                        new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.WHITE));
                cell.addElement(p1);

                Paragraph p2 = new Paragraph(
                        String.format(getResources().getString(R.string.body_pdf_message)),
                        new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.WHITE));
                cell.addElement(p2);
                //cell.addElement(new Paragraph(""));
                pt.addCell(cell);
                /*cell = new PdfPCell(new Paragraph("aaaaa"));
                cell.setBorder(Rectangle.NO_BORDER);
                pt.addCell(cell);*/

                PdfPTable pTable = new PdfPTable(1);
                pTable.setWidthPercentage(100);
                cell = new PdfPCell();
                cell.setColspan(1);
                cell.addElement(pt);
                pTable.addCell(cell);
                PdfPTable table = new PdfPTable(3);

                float[] columnWidth = new float[]{30, 70, 30};
                table.setWidths(columnWidth);

                cell = new PdfPCell();

                cell.setBackgroundColor(myColor);
                cell.setColspan(6);
                cell.addElement(pTable);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(" "));
                cell.setColspan(6);
                table.addCell(cell);
                cell = new PdfPCell();
                cell.setColspan(6);

                cell.setBackgroundColor(myColor2);

                cell = new PdfPCell(new Phrase(getResources().getString(R.string.message_tresh_pdf)));
                cell.setBackgroundColor(myColor2);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(getResources().getString(R.string.criteria_pdf)));
                cell.setBackgroundColor(myColor2);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(getResources().getString(R.string.points_pdf)));
                cell.setBackgroundColor(myColor2);
                table.addCell(cell);

                //table.setHeaderRows(3);
                cell = new PdfPCell();
                cell.setColspan(6);

                table.addCell(" 2 points"); table.addCell(getResources().getString(R.string.string_educational_level)); table.addCell(getCell(" " + et_scorScol1 + " / 14"));
                table.addCell(" "); table.addCell(getResources().getString(R.string.string_training_area)); table.addCell(getCell(" " + et_form1 + " / 12"));
                table.addCell(" "); table.addCell(getResources().getString(R.string.string_professionnel_exp2)); table.addCell(getCell(" " + et_exp + " / 8 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.string_age_pdf)); table.addCell(getCell(" " + et_age1 + " / 16 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.fr_oral_comprehension_pdf)); table.addCell(getCell(" " + et_fco1 + " / 7 "));
                table.addCell(" "); table.addCell("                   " + getResources().getString(R.string.string_oral_production)); table.addCell(getCell(" " + et_fpo1 + " / 7 "));
                table.addCell(" "); table.addCell("                   " + getResources().getString(R.string.string_write_comprehension)); table.addCell(getCell(" " + et_fce + " / 1 "));
                table.addCell(" "); table.addCell("                   " + getResources().getString(R.string.string_write_production)); table.addCell(getCell(" " + et_fpe + " / 1 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.en_oral_comprehension_pdf)); table.addCell(getCell(" " + et_aco + " / 2 "));
                table.addCell(" "); table.addCell("                   " + getResources().getString(R.string.string_oral_production)); table.addCell(getCell(" " + et_apo + " / 2 "));
                table.addCell(" "); table.addCell("                   " + getResources().getString(R.string.string_write_comprehension)); table.addCell(getCell(" " + et_ace + " / 1 "));
                table.addCell(" "); table.addCell("                   " + getResources().getString(R.string.string_write_production)); table.addCell(getCell(" " + et_ape + " / 1 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.string_quebec_visit)); table.addCell(getCell(" " + et_sejour + " / 5 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.family_in_quebec)); table.addCell(getCell(" " + et_famille + " / 3 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.string_validated_job_offer_pdf)); table.addCell(getCell(" " + et_emploi + " / 14 "));
                table.addCell(" " + getResources().getString(R.string.string_spouse_pdf)); table.addCell(getResources().getString(R.string.school_spouse_pdf)); table.addCell(getCell(" " + et_scorScol.getText().toString() + " / 4 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.training_spouse_pdf)); table.addCell(getCell(" " + et_form.getText().toString() + " / 4 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.age_spouse_pdf)); table.addCell(getCell(" " + et_age.getText().toString() + " / 3 "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.fr_oral_comprehension_spouse_pdf)); table.addCell(getCell(" " + et_fco.getText().toString() + " / 3 "));
                table.addCell(" "); table.addCell("                  " +getResources().getString(R.string.fr_oral_production_spouse_pdf)); table.addCell(getCell(" " + et_fpo.getText().toString() + " / 3 "));
                table.addCell(getCellColor(" " + (52) + getResources().getString(R.string.points_pdf))); table.addCell(getCellColor(getResources().getString(R.string.string_tresh_work_pdf))); table.addCell(getCellColorCenter(" " + scorEmpTot + " / " + 52 + " "));
                table.addCell(" "); table.addCell(getResources().getString(R.string.string_children_pdf)); table.addCell(getCell(" " + scoreEnfants + " / 8 "));
                table.addCell(" 1 point "); table.addCell(getResources().getString(R.string.string_financial_autonomy_pdf)); table.addCell(getCell(" " + et_CapFin + " / 1 "));


                PdfPTable ftable = new PdfPTable(2);
                ftable.setWidthPercentage(100);
                float[] columnWidthaa = new float[]{77, 23};
                ftable.setWidths(columnWidthaa);
                cell = new PdfPCell();
                cell.setColspan(6);
                cell.setBackgroundColor(myColor1);
                cell = new PdfPCell(new Phrase(getResources().getString(R.string.tresh_prem_pdf)));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                cell = new PdfPCell(new Phrase("             " + tv_monScrore.getText().toString() + " / " + (59) ));
                //cell.setBorder(Rectangle.NO_BORDER);
                cell.setBackgroundColor(myColor1);
                ftable.addCell(cell);
                //cell = new PdfPCell(new Paragraph("Vous sembez satisfaire au criteres de selection"));
                //cell.setColspan(6);
                ftable.addCell(getCell2(msgPDF));
                cell = new PdfPCell();
                cell.setColspan(6);
                cell.addElement(ftable);
                table.addCell(cell);
                doc.add(table);

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_pfd_save), Toast.LENGTH_LONG).show();

            } catch (DocumentException de) {
                Log.e("PDFCreator", "DocumentException:" + de);
            } catch (IOException e) {
                Log.e("PDFCreator", "ioException:" + e);
            } finally {
                doc.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PdfPCell getCell(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(1);
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        p.setAlignment(Element.ALIGN_CENTER);
        cell.addElement(p);
        return cell;
    }

    private PdfPCell getCellColor(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(1);
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        cell.setBackgroundColor(myColor1);
        cell.addElement(p);
        return cell;
    }

    private PdfPCell getCellColorCenter(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(3);
        cell.setUseAscender(true);
        cell.setUseDescender(true);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        p.setAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(myColor1);
        cell.addElement(p);
        return cell;
    }

    private PdfPCell getCell2(String s) {
        PdfPCell cell = new PdfPCell();
        cell.setColspan(3);
        Paragraph p = new Paragraph(
                String.format(s),
                new Font(Font.FontFamily.HELVETICA, 12));
        p.setAlignment(Element.ALIGN_CENTER);
        cell.addElement(p);
        return cell;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(SecondActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(SecondActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(SecondActivity.this, getResources().getString(R.string.write_permission_message), Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(SecondActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

}
