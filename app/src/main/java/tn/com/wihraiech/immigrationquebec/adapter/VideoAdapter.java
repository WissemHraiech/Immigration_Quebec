package tn.com.wihraiech.immigrationquebec.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import tn.com.wihraiech.immigrationquebec.domain.Video;
import tn.com.wihraiech.immigrationquebec.extras.ImageHelper;
import tn.com.wihraiech.immigrationquebec.interfaces.RecyclerViewOnClickListenerHack;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by Wissem Hraiech on 26/04/2017.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {
    private Context mContext;
    private List<Video> mList;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private float scale;
    private int width;
    private int height;

    private boolean withAnimation;
    private boolean withCardLayout;

    public VideoAdapter(Context c, List<Video> l) {
        this(c, l, true, true);
    }

    public VideoAdapter(Context c, List<Video> l, boolean wa, boolean wcl) {
        mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        withAnimation = wa;
        withCardLayout = wcl;

        scale = mContext.getResources().getDisplayMetrics().density;
        width = mContext.getResources().getDisplayMetrics().widthPixels - (int) (14 * scale + 0.5f);
        height = (width / 16) * 9;
    }

    @Override
    public VideoAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;

        if (withCardLayout) {
            v = mLayoutInflater.inflate(R.layout.item_video, viewGroup, false);
        } else {
            v = mLayoutInflater.inflate(R.layout.item_image, viewGroup, false);
        }

        VideoAdapter.MyViewHolder mvh = new VideoAdapter.MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(VideoAdapter.MyViewHolder myViewHolder, int position) {

        myViewHolder.tvTitle.setText(mList.get(position).getTitle());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //myViewHolder.ivCiy.setImageResource(mList.get(position).getPhoto());
            try {
                Picasso.with(mContext)
                        .load(mList.get(position).getPhoto())
                        .into(myViewHolder.ivVideo);
            } catch (Exception e) {
                Log.e("Picasso_TAG", e.getMessage());
            }

        } else {
            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), mList.get(position).getPhoto());
            bitmap = bitmap.createScaledBitmap(bitmap, width, height, false);

            bitmap = ImageHelper.getRoundedCornerBitmap(mContext, bitmap, 10, width, height, false, false, true, true);
            myViewHolder.ivVideo.setImageBitmap(bitmap);
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public int getImage(int pos) {
        mList.get(pos).getPhoto();
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView ivVideo;
        //public SimpleDraweeView ivCiy;
        public TextView tvTitle;
        public ImageView image_action_flag;
        public ImageView image_action_share;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivVideo = (ImageView) itemView.findViewById(R.id.iv_video);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_videoTilte);
            image_action_flag = (ImageView) itemView.findViewById(R.id.image_action_flag);
            image_action_share = (ImageView) itemView.findViewById(R.id.image_action_share);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mRecyclerViewOnClickListenerHack != null) {
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }


    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r) {
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(Video v, int position) {
        mList.add(v);
        notifyItemInserted(position);
    }

    public void removeListItem(int position) {
        mList.remove(position);
        notifyItemRemoved(position);
    }

}
