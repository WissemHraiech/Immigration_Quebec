package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import tn.com.wihraiech.immigrationquebec.R;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private EditText input_email;
    private Button btnResetPass;
    private TextView btnBack;
    private RelativeLayout activity_forgot;
    private ProgressBar progressBar;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_forgotPass);
        mToolbar.setTitle("Retrouvez votre comptre");
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_notification);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        //View
        input_email = (EditText)findViewById(R.id.forgot_email);
        btnResetPass = (Button)findViewById(R.id.forgot_btn_reset);
        btnBack = (TextView)findViewById(R.id.forgot_btn_back);
        activity_forgot = (RelativeLayout)findViewById(R.id.activity_forgot_password);
        progressBar = (ProgressBar) findViewById(R.id.progressBarFP);

        btnResetPass.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        //Init Firebase
        auth = FirebaseAuth.getInstance();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.forgot_btn_back)
        {
            startActivity(new Intent(ForgotPassword.this,LoginActivity.class));
            finish();
        }
        else  if(view.getId() == R.id.forgot_btn_reset)
        {
            String email = input_email.getText().toString();
            if(!email.isEmpty()) {
                if(email.contains("@")){
                    progressBar.setVisibility(View.VISIBLE);
                    resetPassword(input_email.getText().toString());
                }else {
                    Snackbar.make(activity_forgot, "Adresse e-mail incorrecte !", Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(activity_forgot, "Saisissez une adresse e-mail !", Snackbar.LENGTH_LONG).show();
            }

        }


    }

    private void resetPassword(final String email) {
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progressBar.setVisibility(View.GONE);
                        if(task.isSuccessful())
                        {
                            Snackbar snackBar = Snackbar.make(activity_forgot,"Lien de réinitialisation envoyé à : " + email, Snackbar.LENGTH_LONG);
                            snackBar.show();
                        }
                        else if(task.getException().getMessage().equals("An internal error has occurred. [ <<Network Error>> ]")){
                            Snackbar snackBar = Snackbar.make(activity_forgot,"Pas de connexion internet.",Snackbar.LENGTH_LONG);
                            snackBar.show();
                        }else
                        {
                            Snackbar snackBar = Snackbar.make(activity_forgot,"Aucun compte ne correspond à cette adresse e-mail ! Veuillez la vérifier et réessayer.",Snackbar.LENGTH_LONG);
                            snackBar.show();
                        }
                    }
                });
    }
}
