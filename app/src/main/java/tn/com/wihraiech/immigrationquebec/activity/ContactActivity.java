package tn.com.wihraiech.immigrationquebec.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.List;

import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.extras.UtilTCM;
import tn.com.wihraiech.immigrationquebec.extras.utils.SendMail;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by Wissem Hraiech on 20/10/2017.
 */

public class ContactActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, com.rey.material.widget.Spinner.OnItemSelectedListener {

    private Toolbar mToolbar;
    private String mEmail, emailsTo, val;
    private EditText et_nom, et_prenom, et_Email, et_tel;
    private File attachment;
    private com.rey.material.widget.Spinner sp_pays;
    private Config config = new Config();
    private static final int PERMISSION_REQUEST_CODE = 1;
    final String EXTRA_EMAIL = "email";

    //Send button
    private Button buttonSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_contact);
        setSupportActionBar(mToolbar);

        ////////////////////ActionBar///////////////
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        TextView textView = (TextView) findViewById(R.id.idtoolbar);
        textView.setText(getResources().getString(R.string.title_activity_contact));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////



        //Initializing the views
        et_nom = (EditText) findViewById(R.id.et_nom);
        et_prenom = (EditText) findViewById(R.id.et_prenom);
        et_tel = (EditText) findViewById(R.id.et_tel);
        sp_pays = (com.rey.material.widget.Spinner) findViewById(R.id.spiner_pays);

        et_Email = (EditText) findViewById(R.id.et_email);
        et_Email.setText(mEmail);


        buttonSend = (Button) findViewById(R.id.bt_send);

        addItemsOnSpinner_pays();

        //Adding click listener
        buttonSend.setOnClickListener(this);
    }

    private void sendEmail() {
        //Getting content for email
        String message = "Prénom: " + et_prenom.getText().toString().trim() + "\nNom: " + et_nom.getText().toString().trim()
                + "\nPays de résidence: " + String.valueOf(sp_pays.getSelectedItem())
                + "\nEmail: " + et_Email.getText().toString().trim() + "\nTel: " + et_tel.getText().toString().trim();


        emailsTo = "immigrationquebec.co@gmail.com" + "," + et_Email.getText().toString().trim();

        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermission())
            {
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/" + "Fiche Évaluation Immigration.pdf";
                attachment = new File(path);


                //Creating SendMail object
                SendMail sm = new SendMail(this, emailsTo, "Demande d'un expert", message, attachment);
                //Executing sendmail to send email
                sm.execute();
                // Code for above or equal 23 API Oriented Device
                // Your Permission granted already .Do next code
            } else {
                requestPermission(); // Code for
                String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/" + "Fiche Évaluation Immigration.pdf";
                attachment = new File(path);

                //Creating SendMail object
                SendMail sm = new SendMail(this, emailsTo, "Demande d'un expert", message, attachment);
                //Executing sendmail to send email
                sm.execute();

            }
        }
        else
        {
            // Code for Below 23 API Oriented Device
            // Do next code
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/" + "Fiche Évaluation Immigration.pdf";
            attachment = new File(path);

            //Creating SendMail object
            SendMail sm = new SendMail(this, emailsTo, "Demande d'un expert", message, attachment);
            //Executing sendmail to send email
            sm.execute();
        }


    }


    @Override
    public void onClick(View v) {
        if (UtilTCM.verifyConnection( getApplicationContext() )){

            attemptEnvoyer();

        }
        else {

            Snackbar.make(getCurrentFocus(), "Vérifiez SVP votre connexion Internet.", Snackbar.LENGTH_LONG)
                    .setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent it = new Intent(Settings.ACTION_WIFI_SETTINGS);
                            startActivity(it);
                        }
                    })
                    .setActionTextColor(getApplicationContext().getResources().getColor(R.color.coloLink))
                    .show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            //startActivity( new Intent(this, AccueilActivity.class));
            finish();
        }

        return true;
    }

    public void addItemsOnSpinner_pays(){
        // Spinner click listener
        sp_pays.setOnItemSelectedListener(this);

        List<String> pays = config.pays();

        // Creating adapter for spinner
        ArrayAdapter<String> paysAdapter = new ArrayAdapter <String>(this, R.layout.spinner_item_nbr2, pays);

        // Drop down layout style - list view with radio button
        paysAdapter.setDropDownViewResource(R.layout.spinner_item_nbr);

        // attaching data adapter to spinner
        sp_pays.setAdapter(paysAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemSelected(com.rey.material.widget.Spinner parent, View view, int position, long id) {

    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(ContactActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(ContactActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(ContactActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(ContactActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    public void attemptEnvoyer() {

        et_nom.setError(null);
        et_prenom.setError(null);
        et_Email.setError(null);
        et_tel.setError(null);

        // Store values at the time of the login attempt.
        String nom = et_nom.getText().toString();
        String prenom = et_prenom.getText().toString();
        String email = et_Email.getText().toString();
        String tel = et_tel.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid name.
        if (TextUtils.isEmpty(nom)) {
            et_nom.setError(getString(R.string.error_field_required));
            focusView = et_nom;
            cancel = true;
        }
        // Check for a valid prenom.
        if (TextUtils.isEmpty(prenom)) {
            et_prenom.setError(getString(R.string.error_field_required));
            focusView = et_prenom;
            cancel = true;
        }
        // Check for a valid email.
        if (TextUtils.isEmpty(email)) {
            et_Email.setError(getString(R.string.error_field_required));
            focusView = et_Email;
            cancel = true;
        } else if (!email.contains("@")) {
            et_Email.setError(getString(R.string.error_invalid_email));
            focusView = et_Email;
            cancel = true;
        }
        // Check for a valid tel.
        if (TextUtils.isEmpty(tel)) {
            et_tel.setError(getString(R.string.error_field_required));
            focusView = et_tel;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            sendEmail();

        }
    }

}
