package tn.com.wihraiech.immigrationquebec.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lenovo on 11/12/2016.
 */
public class Canada implements Parcelable {

    private long id;
    private String province;
    private String city;
    private int photo;

    public Canada() {}

    public Canada(String province, String city, int photo) {
        this.province = province;
        this.city = city;
        this.photo = photo;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.province);
        dest.writeString(this.city);
        dest.writeInt(this.photo);
    }

    protected Canada(Parcel in) {
        this.id = in.readLong();
        this.province = in.readString();
        this.city = in.readString();
        this.photo = in.readInt();
    }

    public static final Parcelable.Creator<Canada> CREATOR = new Parcelable.Creator<Canada>() {
        @Override
        public Canada createFromParcel(Parcel source) {
            return new Canada(source);
        }

        @Override
        public Canada[] newArray(int size) {
            return new Canada[size];
        }
    };
}
