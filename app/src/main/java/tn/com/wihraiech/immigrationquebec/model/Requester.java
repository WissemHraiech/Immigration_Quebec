package tn.com.wihraiech.immigrationquebec.model;

import com.loopj.android.http.AsyncHttpClient;

import tn.com.wihraiech.immigrationquebec.presenter.Presenter;

/**
 * Created by Wissem Hraiech on 28/03/2017.
 */

public class Requester {
    private AsyncHttpClient asyncHttpClient;
    private Presenter presenter;

    public Requester(Presenter presenter) {
        asyncHttpClient = new AsyncHttpClient();
        this.presenter = presenter;
    }

    public void retrieveJaguars() {
        asyncHttpClient.post(
                presenter.getContext(),
                JsonHttpRequest.URI,
                null,
                new JsonHttpRequest( presenter ) );

        new VersionRequester(presenter).execute();
    }

}
