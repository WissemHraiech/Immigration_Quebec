package tn.com.wihraiech.immigrationquebec.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.adapter.TabsLinksAdapter;
import tn.com.wihraiech.immigrationquebec.extras.SlidingTabLayout;

/**
 * Created by lenovo on 11/12/2016.
 */
public class LinksFragment extends Fragment {

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_links, container, false);

        // TABS
        mViewPager = (ViewPager) view.findViewById(R.id.vp_tabs_links);
        mViewPager.setAdapter(new TabsLinksAdapter(getChildFragmentManager(), getContext()));

        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.stl_links);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorAccent));
        mSlidingTabLayout.setViewPager(mViewPager);

        return view;
    }

}
