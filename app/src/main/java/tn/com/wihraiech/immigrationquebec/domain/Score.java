package tn.com.wihraiech.immigrationquebec.domain;

/**
 * Created by lenovo on 14/01/2017.
 */
public class Score {

    int _id;
    String _nom;
    String _score;
    String _date;


    public Score(){}

    public Score(int _id, String _nom, String _score, String _date) {
        this._id = _id;
        this._nom = _nom;
        this._score = _score;
        this._date = _date;
    }

    public Score(String _nom, String _score) {
        this._nom = _nom;
        this._score = _score;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_nom() {
        return _nom;
    }

    public void set_nom(String _nom) {
        this._nom = _nom;
    }

    public String get_score() {
        return _score;
    }

    public void set_score(String _score) {
        this._score = _score;
    }

    public String get_date() {
        return _date;
    }

    public void set_date(String _date) {
        this._date = _date;
    }
}
