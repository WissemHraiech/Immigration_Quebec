package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.facebook.ads.*;

import tn.com.wihraiech.immigrationquebec.adapter.ViewPagerAdapter;
import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.extras.CountAds;

/**
 * Created by lenovo on 11/12/2016.
 */
public class ImagePagerActivity extends AppCompatActivity {

    private int currentPos;
    private TextView tv_title;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private int[] images = {
            R.drawable.cout_imm, R.drawable.nouveau_tarif, R.drawable.regle2018, R.drawable.grille_select_2018_1, R.drawable.grille_select_2018_2, R.drawable.proced_imm,
            R.drawable.proced_mpq, R.drawable.delais_2015, R.drawable.delais_2016, R.drawable.francais, R.drawable.tcf, R.drawable.anglais,
            R.drawable.ielts, R.drawable.experience, R.drawable.miseajour, R.drawable.csq, R.drawable.code_priori, R.drawable.federal, R.drawable.photos,
            R.drawable.sydney, R.drawable.visite_medical, R.drawable.e_brune, R.drawable.visa, R.drawable.good_luck};
    final String EXTRA_POSITION = "position";


    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        Intent intent = getIntent();
        if (intent != null) {
            currentPos = intent.getIntExtra(EXTRA_POSITION, 0);
        }

        String[] titres = new String[]{getResources().getString(R.string.imm_cost_picture_title), getResources().getString(R.string.new_rates_picture_title), getResources().getString(R.string.imm_proc_picture_title),
                getResources().getString(R.string.grid2018_part1_picture_title), getResources().getString(R.string.grid2018_part2_picture_title),getResources().getString(R.string.diagram1_picture_title), getResources().getString(R.string.diagram2_picture_title),
                getResources().getString(R.string.process_bef_2016_picture_title), getResources().getString(R.string.process_after_2016_picture_title), getResources().getString(R.string.fr_req_picture_title), getResources().getString(R.string.tcf_picture_title),
                getResources().getString(R.string.en_req_picture_title), getResources().getString(R.string.ielts_picture_title), getResources().getString(R.string.work_certif_picture_title), getResources().getString(R.string.reject_picture_title), getResources().getString(R.string.csq_picture_title),
                getResources().getString(R.string.prior_treat_picture_title), getResources().getString(R.string.federal_picture_title), getResources().getString(R.string.photo_format_title), getResources().getString(R.string.ar_sydney_picture_title), getResources().getString(R.string.medical_exam_title), getResources().getString(R.string.e_brune_title), getResources().getString(R.string.visa_title), ""};
        //tv_title = (TextView) findViewById(R.id.tv_zoom_title);

        viewPager = (ViewPager) findViewById(R.id.vp_proced);
        adapter  = new ViewPagerAdapter(ImagePagerActivity.this, images, titres);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(currentPos);

        loadInterstitialAd();
    }

    private void loadInterstitialAd() {
        // Instantiate an InterstitialAd object
        //interstitialAd = new InterstitialAd(this, "1102867196483381_1151521178284649");
        interstitialAd = new InterstitialAd(this, getResources().getString(R.string.interstitial_ad_unit_id));
        // Set listeners for the Interstitial Ad
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial displayed callback

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                finish();
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Ad error callback
                Log.i("log", "Error: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Show the ad when it's done loading.
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

        });

        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        interstitialAd.loadAd();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*if (UtilTCM.verifyConnection(getApplicationContext())){
            if( interstitialAd != null
                    && CountAds.isUpToShowAd(this) ){

                interstitialAd.show();
            }else{
                //super.onBackPressed();
                finish();

            }
        }else{
            super.onBackPressed();
        }*/
    }

    @Override
    protected void onDestroy() {
        CountAds.incrementCounter(this);
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
        super.onDestroy();
    }

}