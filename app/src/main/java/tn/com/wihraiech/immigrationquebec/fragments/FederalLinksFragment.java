package tn.com.wihraiech.immigrationquebec.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.regex.Pattern;

import tn.com.wihraiech.immigrationquebec.activity.ImagePagerActivity;
import tn.com.wihraiech.immigrationquebec.activity.VideoViewActivity;
import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.extras.PatternEditableBuilder;

public class FederalLinksFragment extends Fragment {

    private TextView tv_google, tv_photo;
    private ImageView iv_paie, iv_clegc;
    private final String EXTRA_POSITION = "position", EXTRA_IDVIDEO = "idvideo";
    private final String googlePlayURL = Config.GOOGLEPLAY_URL;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_federal_links, container, false);

        iv_paie = (ImageView) view.findViewById(R.id.iv_Vdpaie);
        iv_paie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "FPw-NaMHAjc");
                startActivity(i);
            }
        });

        iv_clegc = (ImageView) view.findViewById(R.id.iv_VdClegc);
        iv_clegc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "UM5JzpuGhWU");
                startActivity(i);
            }
        });

        //LINK COÛT
        tv_photo = (TextView) view.findViewById(R.id.tv_photo);
        // COLORED LINKS
        new PatternEditableBuilder().
                addPattern(Pattern.compile("Cliquer ici"), getResources().getColor(R.color.colorAccent),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Intent i = new Intent(getContext(), ImagePagerActivity.class);
                                i.putExtra(EXTRA_POSITION, 17);
                                startActivity(i);
                            }
                        }).into(tv_photo);

        tv_google = (TextView) view.findViewById(R.id.tv_google);
        // GooglePlay HyperLink
        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.green),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Uri uriUrl = Uri.parse(googlePlayURL);
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                startActivity(launchBrowser);
                            }
                        }).into(tv_google);

        return view;
    }

}
