package tn.com.wihraiech.immigrationquebec.presenter;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import tn.com.wihraiech.immigrationquebec.activity.AccueilActivity;
import tn.com.wihraiech.immigrationquebec.model.Requester;
import tn.com.wihraiech.immigrationquebec.model.SPLocalBase;

/**
 * Created by Wissem Hraiech on 28/03/2017.
 */

public class Presenter {

    private static Presenter instance;
    private Requester model;
    private AccueilActivity activity;

    private Presenter(){
        model = new Requester( this );
    }

    public static Presenter getInstance(){
        if( instance == null ){
            instance = new Presenter();
        }
        return instance;
    }

    public void setActivity(AccueilActivity activity){
        this.activity = activity;
    }


    public Activity getContext() {
        return activity;
    }

    private PackageInfo packageInfo(){
        PackageInfo pinfo = null;
        try {
            String packageName = getContext().getPackageName();
            pinfo = getContext().getPackageManager().getPackageInfo(packageName, 0);
        }
        catch(PackageManager.NameNotFoundException e){}

        return pinfo;
    }

    public void retrieveJaguars(Bundle savedInstanceState) {

        model.retrieveJaguars();
    }

    public void showProgressBar(boolean status) {
        int visibilidade = status ? View.VISIBLE : View.GONE;
        activity.showProgressBar( visibilidade );
    }

    public void showUpdateAppDialog( String actuallyAppVersion ){
        String version = packageInfo().versionName;

        Log.i("log", version + " | " + actuallyAppVersion);
        try{
            if (actuallyAppVersion.equals(null)){
                Log.i("LOG", "App not published" );
            }else if( !actuallyAppVersion.equals(version)
                    && SPLocalBase.is24hrsDelayed(activity) ){

                //activity.showUpdateAppDialog();
            }
        }catch (Exception e){
            Log.i("LOG", "App not published" );
        }

    }

}
