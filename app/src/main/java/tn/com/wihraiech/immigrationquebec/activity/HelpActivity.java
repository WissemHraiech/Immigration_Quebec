package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.regex.Pattern;

import tn.com.wihraiech.immigrationquebec.extras.Config;
import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.extras.PatternEditableBuilder;

/**
 * Created by lenovo on 25/10/2016.
 */
public class HelpActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView tv_google, tv_links, tv_couple, link_installapp, tv_grille;
    private ImageView iv_help;
    private final String EXTRA_POSITION = "position", EXTRA_IDVIDEO = "idvideo";
    private final String googlePlayURL = Config.GOOGLEPLAY_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_help);
        setSupportActionBar(mToolbar);

        ////////////////////ActionBar///////////////
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_main);
        TextView textView = (TextView) findViewById(R.id.idtoolbar);
        textView.setText(getResources().getString(R.string.title_activity_help));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        iv_help = (ImageView) findViewById(R.id.iv_VdHelp2);
        iv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), VideoViewActivity.class);
                i.putExtra(EXTRA_IDVIDEO, "qGti_9IYsMk");
                startActivity(i);
            }
        });

        tv_google = (TextView) findViewById(R.id.tv_google);
        // GooglePlay HyperLink
        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.green),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Uri uriUrl = Uri.parse(googlePlayURL);
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                startActivity(launchBrowser);
                            }
                        }).into(tv_google);

        // COLORED LINKS
        tv_links = (TextView) findViewById(R.id.tv_linkss);
        new PatternEditableBuilder().
                addPattern( Pattern.compile("\\@(\\w+)"), getResources().getColor(R.color.colorAccent) ).
                into(tv_links);

        // COUPLE LINKS
        tv_couple = (TextView) findViewById(R.id.tv_alinkcouple);
        new PatternEditableBuilder().
                addPattern( Pattern.compile("Ajout les points de mon épous(e)"), getResources().getColor(R.color.colorAccent) ).
                into(tv_couple);

        // GRILLE DE SELECTION LINKS
        tv_grille = (TextView) findViewById(R.id.tv_agrille);
        new PatternEditableBuilder().
                addPattern(Pattern.compile("d'une grille de facteurs"), getResources().getColor(R.color.colorAccent),
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {

                                Intent i = new Intent(getApplicationContext(), ImagePagerActivity.class);
                                i.putExtra(EXTRA_POSITION, 3);
                                startActivity(i);
                            }
                        }).into(tv_grille);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return true;
    }

}
