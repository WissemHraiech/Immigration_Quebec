package tn.com.wihraiech.immigrationquebec.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import tn.com.wihraiech.immigrationquebec.R;
import tn.com.wihraiech.immigrationquebec.activity.DetailsBlogActivity;
import tn.com.wihraiech.immigrationquebec.activity.EvaluateActivity;
import tn.com.wihraiech.immigrationquebec.domain.Blog;
import tn.com.wihraiech.immigrationquebec.interfaces.RecyclerViewOnClickListenerHack;

import static tn.com.wihraiech.immigrationquebec.GlobalFunctions.loadAdBanner;
import static tn.com.wihraiech.immigrationquebec.GlobalFunctions.loadInterstitialAd;
import static tn.com.wihraiech.immigrationquebec.globalAds.INTERSTITIAL_ACC_AD_UNIT_ID;
import static tn.com.wihraiech.immigrationquebec.globalAds.INTERSTITIAL_BLOG_AD_UNIT_ID;

/**
 * Created by lenovo on 11/12/2016.
 */
public class AccueilFragment extends Fragment implements RecyclerViewOnClickListenerHack {

    private static final String BLOG_DETAIL = "BLOG_DETAIL";
    private Button bt_eval;
    private RecyclerView rv_blog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Blog mBlog;
    private FirebaseRecyclerAdapter<Blog, BlogViewHolder> mFirebaseRecyclerAdapter;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private InterstitialAd mBlogInterstitialAd;

    private DatabaseReference mDatabaseRef;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_accueil, container, false);

        if (savedInstanceState != null) {
            mBlog = savedInstanceState.getParcelable(BLOG_DETAIL);
        }

        mAdView = view.findViewById(R.id.adView_acc);
        loadAdBanner(mAdView, getContext());

        mInterstitialAd = loadInterstitialAd(getContext(), INTERSTITIAL_ACC_AD_UNIT_ID);
        mBlogInterstitialAd = loadInterstitialAd(getContext(), INTERSTITIAL_BLOG_AD_UNIT_ID);
        setEvalBtOnClickListener(view);
        setupBlogRecyclerView(view);
        populateBlogList();

        return view;
    }

    private void populateBlogList() {
        mDatabaseRef = FirebaseDatabase.getInstance().getReference().child("Blog");
        Query query = mDatabaseRef.orderByKey();
        mFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Blog, BlogViewHolder>(
                Blog.class,
                R.layout.item_blog,
                BlogViewHolder.class,
                query) {
            @Override
            protected void populateViewHolder(BlogViewHolder viewHolder, Blog model, int position) {

                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDescription());
                viewHolder.setDate(model.getDate());
                viewHolder.setLink(model.getLink());
                viewHolder.setImage(getContext(), model.getImage());
            }

            @Override
            public Blog getItem(int position) {
                return super.getItem(getItemCount() - 1 - position);
            }

        };
    }

    private void setupBlogRecyclerView(View view) {
        rv_blog = view.findViewById(R.id.rv_blog);
        rv_blog.addOnItemTouchListener(new RecycleViewTouchListener(getActivity(), rv_blog, this));
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_blog.setLayoutManager(llm);
        rv_blog.setHasFixedSize(true);
        rv_blog.setItemViewCacheSize(20);
        rv_blog.setDrawingCacheEnabled(true);
        rv_blog.setAdapter(mFirebaseRecyclerAdapter);
    }

    private void setEvalBtOnClickListener(View view) {
        bt_eval = view.findViewById(R.id.bt_eval);
        bt_eval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInterstitialAd != null)
                    mInterstitialAd.show(getActivity());
                else
                    startActivity(new Intent(getActivity(), EvaluateActivity.class));

            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BLOG_DETAIL, mBlog);
    }

    /**TODO: ADMOB **/
    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();

        mFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Blog, BlogViewHolder>(
                Blog.class,
                R.layout.item_blog,
                BlogViewHolder.class,
                mDatabaseRef) {
            @Override
            protected void populateViewHolder(BlogViewHolder viewHolder, Blog model, int position) {

                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDescription());
                viewHolder.setDate(model.getDate());
                viewHolder.setLink(model.getLink());
                viewHolder.setImage(getActivity(), model.getImage());
            }

            @Override
            public Blog getItem(int position) {
                return super.getItem(getItemCount() - 1 - position);
            }

        };

        rv_blog.setAdapter(mFirebaseRecyclerAdapter);
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();

        if (mAdView != null) {
            mAdView.resume();
        }

        mFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Blog, BlogViewHolder>(
                Blog.class,
                R.layout.item_blog,
                BlogViewHolder.class,
                mDatabaseRef) {
            @Override
            protected void populateViewHolder(BlogViewHolder viewHolder, Blog model, int position) {

                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDescription());
                viewHolder.setDate(model.getDate());
                viewHolder.setLink(model.getLink());
                viewHolder.setImage(getContext(), model.getImage());
            }

            @Override
            public Blog getItem(int position) {
                return super.getItem(getItemCount() - 1 - position);
            }

        };
        rv_blog.setAdapter(mFirebaseRecyclerAdapter);
    }

    @Override
    public void onClickListener(View view, int position) {

        FirebaseRecyclerAdapter adapter = (FirebaseRecyclerAdapter) rv_blog.getAdapter();
        mBlog = (Blog) adapter.getItem(position);

        if (mBlogInterstitialAd != null) {
            mBlogInterstitialAd.show(getActivity());
        } else {
            toBlogDetails(mBlog);
        }

    }

    private void toBlogDetails(Blog blog) {
        Intent intent = new Intent(getContext(), DetailsBlogActivity.class);
        intent.putExtra("title", blog.getTitle());
        intent.putExtra("date", blog.getDate());
        intent.putExtra("desc", blog.getDescription());
        intent.putExtra("link", blog.getLink());
        intent.putExtra("image", blog.getImage());
        startActivity(intent);
    }

    @Override
    public void onLongPressClickListener(View view, int position) {

    }

    public static class BlogViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public BlogViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setTitle(String title) {
            TextView post_title = mView.findViewById(R.id.tv_title);
            post_title.setText(title);
        }

        public void setDesc(String desc) {
            TextView post_desc = mView.findViewById(R.id.tv_desc);
            post_desc.setText(desc);
        }

        public void setDate(String date) {
            TextView post_desc = mView.findViewById(R.id.tv_date);
            post_desc.setText(date);
        }

        public void setLink(String link) {
            TextView post_desc = mView.findViewById(R.id.tv_link);
            post_desc.setText(link);
        }

        private void setImage(Context context, String image) {
            ImageView post_image = mView.findViewById(R.id.iv_post);
            Picasso.with(context)
                    .load(image)
                    .into(post_image);
        }

    }

    private static class RecycleViewTouchListener implements RecyclerView.OnItemTouchListener {
        private final Context mContext;
        private final GestureDetector mGestureDetector;
        private final RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

        public RecycleViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerHack rvoclh) {
            mContext = c;
            mRecyclerViewOnClickListenerHack = rvoclh;

            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if (cv != null && mRecyclerViewOnClickListenerHack != null) {
                        mRecyclerViewOnClickListenerHack.onLongPressClickListener(cv,
                                rv.getChildPosition(cv));
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if (cv != null && mRecyclerViewOnClickListenerHack != null) {
                        mRecyclerViewOnClickListenerHack.onClickListener(cv,
                                rv.getChildPosition(cv));
                    }

                    return (true);
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            try {
                mGestureDetector.onTouchEvent(e);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
