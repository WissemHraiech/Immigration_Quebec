package tn.com.wihraiech.immigrationquebec.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.mikepenz.materialdrawer.Drawer;

import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by Wissem Hraiech on 07/08/2017.
 */

public class EvaluateActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private Button btn_celib, btn_couple;
    private Drawer.Result navigationDrawerLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluate);

        // TOOLBAR
        mToolbar = (Toolbar) findViewById(R.id.tb_eval);
        mToolbar.setTitle(getResources().getString(R.string.title_activity_evaluate));
        //mToolbar.setSubtitle("just a Subtitle");
        //mToolbar.setLogo(R.drawable.ic_notification);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //////////////////////////////////

        btn_celib = (Button) findViewById(R.id.btn_celib);
        btn_celib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("val", 1);
                startActivity(i);
            }
        });

        btn_couple = (Button) findViewById(R.id.btn_couple);
        btn_couple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("val", 2);
                startActivity(i);
            }
        });

        //Drawer finish()
        navigationDrawerLeft = new Drawer()
                .withActivity(this)
                .withActionBarDrawerToggle(true)
                .withCloseOnClick(true)
                .withActionBarDrawerToggleAnimated(true)
                .withActionBarDrawerToggle(new ActionBarDrawerToggle(this, new DrawerLayout(this), R.string.drawer_open, R.string.drawer_close) {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        super.onDrawerSlide(drawerView, slideOffset);
                        navigationDrawerLeft.closeDrawer();
                        finish();
                    }
                })
                .build();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return true;
    }

}
