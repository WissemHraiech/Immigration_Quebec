package tn.com.wihraiech.immigrationquebec.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import tn.com.wihraiech.immigrationquebec.domain.ProcImage;
import tn.com.wihraiech.immigrationquebec.interfaces.RecyclerViewOnClickListenerHack;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by lenovo on 08/12/2016.
 */
public class ProcImageAdapter extends RecyclerView.Adapter<ProcImageAdapter.MyViewHolder> {
    private Context mContext;
    private List<ProcImage> mList;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private float scale;
    private int width;
    private int height;

    private boolean withAnimation;
    private boolean withCardLayout;

    public ProcImageAdapter(Context c, List<ProcImage> l) {
        this(c, l, true, true);
    }

    public ProcImageAdapter(Context c, List<ProcImage> l, boolean wa, boolean wcl) {
        mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        withAnimation = wa;
        withCardLayout = wcl;

        scale = mContext.getResources().getDisplayMetrics().density;
        width = mContext.getResources().getDisplayMetrics().widthPixels - (int) (14 * scale + 0.5f);
        height = (width / 16) * 9;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;

        v = mLayoutInflater.inflate(R.layout.item_proc_card, viewGroup, false);

        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {

        myViewHolder.tv_title.setText(mList.get(position).getTitle());

        try {

            Picasso.with(mContext)
                    .load(mList.get(position).getPhoto())
                    .into(myViewHolder.iv_proc);

        } catch (Exception e) {
            Log.e("Picasso_TAG", e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_title;
        public ImageView iv_proc;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_proc);
            iv_proc = itemView.findViewById(R.id.iv_proc);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mRecyclerViewOnClickListenerHack != null) {
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }


    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r) {
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(ProcImage c, int position) {
        mList.add(c);
        notifyItemInserted(position);
    }

    public void removeListItem(int position) {
        mList.remove(position);
        notifyItemRemoved(position);
    }

}

