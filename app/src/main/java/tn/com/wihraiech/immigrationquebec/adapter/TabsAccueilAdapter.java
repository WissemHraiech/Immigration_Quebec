package tn.com.wihraiech.immigrationquebec.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import tn.com.wihraiech.immigrationquebec.fragments.AccueilFragment;
import tn.com.wihraiech.immigrationquebec.fragments.AllImagesFragment;
import tn.com.wihraiech.immigrationquebec.fragments.AllLinksFragment;
import tn.com.wihraiech.immigrationquebec.fragments.CanadaFragment;
import tn.com.wihraiech.immigrationquebec.fragments.HelpFragment;
import tn.com.wihraiech.immigrationquebec.fragments.ImmigrationFragment;
import tn.com.wihraiech.immigrationquebec.fragments.ScoresFragment;
import tn.com.wihraiech.immigrationquebec.fragments.VideoFragment;
import tn.com.wihraiech.immigrationquebec.R;

/**
 * Created by lenovo on 21/12/2015.
 */
public class TabsAccueilAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] titles = new String[8];//{"Accueil", "À propos", "Liens utiles", "Images utiles", "Videos utiles", "Canada en images", "Aide", "Mes scores"};
    //private int[] icons = new int[]{R.drawable.car_1, R.drawable.car_1, R.drawable.car_2, R.drawable.car_3, R.drawable.car_4};
    private int heightIcon;

    public TabsAccueilAdapter(FragmentManager fm, Context c) {
        super(fm);

        mContext = c;
        double scale = c.getResources().getDisplayMetrics().density;
        heightIcon = (int) (24 * scale + 0.5f);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;

        if(position == 0){ // ACCUEIL
            frag = new AccueilFragment();
        }
        else if(position == 1){ // IMMIGRATION
            frag = new ImmigrationFragment();
        }
        else if(position == 2){ // LINK
            frag = new AllLinksFragment();
        }
        else if(position == 3){ // IMAGES
            frag = new AllImagesFragment();
        }
        else if(position == 4){ // VIDEO
            frag = new VideoFragment();
        }
        else if(position == 5){ // CANADA
            frag = new CanadaFragment();
        }
        else if(position == 6){ // HELP
            frag = new HelpFragment();
        }
        else if(position == 7){ // SCORES
            frag = new ScoresFragment();
        }

        Bundle b = new Bundle();
        b.putInt("position", position);

        frag.setArguments(b);

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        /*Drawable d = mContext.getResources().getDrawable(icons[position]);
        d.setBounds(0, 0, heightIcon, heightIcon);

        ImageSpan is = new ImageSpan(d);

        SpannableString sp = new SpannableString(" ");
        sp.setSpan(is, 0, sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return ( sp );*/
        String[] titles = {mContext.getResources().getString(R.string.home_menu_drawer), mContext.getResources().getString(R.string.about_imm_menu_drawer), mContext.getResources().getString(R.string.links_menu_drawer), mContext.getResources().getString(R.string.imm_pictures_menu_drawer), mContext.getResources().getString(R.string.imm_videos_menu_drawer), mContext.getResources().getString(R.string.canada_pictures_menu_drawer), mContext.getResources().getString(R.string.help_menu_drawer), mContext.getResources().getString(R.string.scores_menu_drawer)};

        return titles[position];
    }
}
